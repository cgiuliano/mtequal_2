#!/bin/sh

echo "Production database initialization..."
while read line; do
    php app/console doctrine:query:sql "$line" -q
done < data/database_init.sql
