<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 01/02/16
 */

namespace AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleTest;

class VarsExtension extends Twig_Extension
{
  public function initRuntime(Twig_Environment $environment)
  {
    // TODO: Implement initRuntime() method.
  }

  public function getGlobals()
  {
    // TODO: Implement getGlobals() method.
  }

  protected $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function getName()
  {
    return 'some.extension';
  }

  public function getFilters()
  {
//    return array(
//      'json_decode' => new \Twig_Filter_Method($this, 'jsonDecode'),
//      'isAdmin' => new \Twig_Filter_Method($this, 'isAdmin')
//    );
    //Twig_SimpleFilter
    return array(
      new Twig_SimpleFilter('json_decode', array($this, 'jsonDecode')),
      new Twig_SimpleFilter('isAdmin', array($this, 'isAdmin'))
    );
  }

  public function getTests()
  {
//    return array(
//      //new \Twig_Filter_Method('owner', 'isOwner'),
//      'owner' => new \Twig_Filter_Method($this, 'isOwner'),
//      'vendor' => new \Twig_Filter_Method($this, 'isVendor')
//    );
    return array(new Twig_SimpleTest('isOwner', array($this, 'isOwner')),
      new Twig_SimpleTest('isVendor', array($this, 'isVendor')));
  }


  public function getFunctions()
  {
//    return array(
//      'implode' => new \Twig_Function_Method($this, 'implode')
//
//    );
    return array(
      new \Twig_SimpleFunction('implode', array($this, 'implode'))
    );
  }

  public function implode($array)
  {
    return implode(', ', $array);
  }

  public function jsonDecode($str)
  {
    return json_decode($str);
  }

  public function isAdmin($array)
  {
    return in_array('admin', $array);
  }

  public function isOwner($array)
  {
    return in_array('owner', $array);
  }

  public function isVendor($array)
  {
    return in_array('vendor', $array);
  }
}

?>