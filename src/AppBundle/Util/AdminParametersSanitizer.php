<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-06
 * Time: 18:35
 */

namespace AppBundle\Util;

use HTMLPurifier;
use HTMLPurifier_Config;

/**
 * Class AdminParametersSanitizer
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class AdminParametersSanitizer extends ParametersSanitizers
{
  /**
   * Sanitizes the parameters used to build a project.
   *
   * @param array|null $params
   */
  public static function sanitize(array &$params = null)
  {
    if (!$params) {
      return;
    }

    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    //name
    parent::purify($params, 'pid', $purifier, true, true);
  }
}