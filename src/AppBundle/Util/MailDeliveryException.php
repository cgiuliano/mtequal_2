<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 01/12/17
 * Time: 08:06
 */

namespace AppBundle\Util;

class MailDeliveryException extends GenericException
{
  /**
   * @var string
   */
  private $from;

  /**
   * @var string|array
   */
  private $to;

  /**
   * @var array
   */
  private $failures;

  /**
   * ParameterException constructor.
   * @param string $from
   * @param string|array $to
   * @param array $failures
   * @param string $message
   */
  public function __construct($from, $to, $failures, $message = 'Cannot deliver the message.')
  {
    parent::__construct($message, 8);
    $this->from = $from;
    $this->to = $to;
    $this->failures = $failures;
  }

  /**
   * @return string
   */
  public function getFrom()
  {
    return $this->from;
  }

  /**
   * @return string|array
   */
  public function getTo()
  {
    return $this->to;
  }

  /**
   * @return array
   */
  public function getFailures()
  {
    return $this->failures;
  }

}