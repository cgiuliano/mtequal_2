<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 18/12/2017
 * Time: 11:59
 */

namespace AppBundle\Util;

use Exception;

/**
 * Class RepositoryException
 *
 * @author giuliano
 * @package AppBundle\Repository
 */
abstract class GenericException extends Exception
{
  /**
   * @var string
   */
  private $tip;

  /**
   * GenericException constructor.
   * @param string $message
   * @param int $code
   * @param string|null $tip
   */
  public function __construct($message, $code, $tip = null)
  {
    parent::__construct($message, $code);
    $this->tip = $tip;
  }

  public function getExceptionName()
  {
    $classNameWithNamespace = get_class($this);
    return substr($classNameWithNamespace, strrpos($classNameWithNamespace, '\\') + 1);
  }

  /**
   * @return string
   */
  public function getTip()
  {
    return $this->tip;
  }

}