<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 16/11/17
 * Time: 16:03
 */

namespace AppBundle\Util;

/**
 * Class SanitizationException
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class SanitizationException extends ParameterException
{

  /**
   * SanitizationException constructor.
   * @param string $parameter
   * @param string $message
   * @param string $tip
   */
  public function __construct($parameter, $message = 'Failed to sanitize parameter.', $tip)
  {
    parent::__construct($parameter, 3, $message, $tip);
  }

}