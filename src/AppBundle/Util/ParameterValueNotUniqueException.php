<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 17/11/17
 * Time: 16:29
 */

namespace AppBundle\Util;

/**
 * Class ParameterValueNotUniqueException
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class ParameterValueNotUniqueException extends ParameterException
{
  /**
   * ParameterNotAvailableException constructor.
   * @param string $parameter
   * @param string $message
   * @param $tip
   */
  public function __construct($parameter, $message = 'Parameter not valid.', $tip = 'This value is already in use.')
  {
    parent::__construct($parameter, 5, $message, $tip);
  }
}