<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 13:34
 */

namespace AppBundle\Util;


use HTMLPurifier;
use HTMLPurifier_Config;

/**
 * Class UserParametersSanitizer
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class UserParametersSanitizer
{
  /**
   * Sanitizes the parameters used to build a user.
   *
   * @param array|null $params
   */
  public static function sanitize(array &$params = null)
  {
    if (!$params) {
      return;
    }

    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);;

    //username
    self::purify($params, 'username', $purifier, true, true);

    //email
    self::purify($params, 'email', $purifier);

    //password
    //self::purify($params, 'password', $purifier);

    //first name
    self::purify($params, 'firstName', $purifier, true, true);

    //last name
    self::purify($params, 'lastName', $purifier, true, true);

    //affiliation
    self::purify($params, 'affiliation', $purifier, true, true);

    //notes
    self::purify($params, 'notes', $purifier, true);

    //roles
    if (array_key_exists("roles", $params) && $params['roles'] != null && strlen($params['roles']) > 0) {

      $cleanRoles = [];
      $roles = json_decode($params['roles']);
      foreach ($roles as $role) {
        array_push($cleanRoles, $purifier->purify($role));
      }

      $params['roles'] = $cleanRoles;
    }


    //return $params;
  }

  /**
   * @param $params
   * @param $param
   * @param HTMLPurifier $purifier
   * @param bool $stripTags
   * @param bool $trim
   */
  private static function purify(&$params, $param, $purifier, $stripTags = false, $trim = false)
  {
    if (array_key_exists($param, $params) && $params[$param] != null && strlen($params[$param]) > 0) {
      if ($trim) {
        $params[$param] = trim($params[$param]);
      }
      if ($param == "email") {
        $params['email'] = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
      }
      if ($stripTags) {
        $params[$param] = strip_tags($params[$param]);
      }
      $params[$param] = $purifier->purify($params[$param]);

    }
  }
}