<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 10:52
 */

namespace AppBundle\Util;

/**
 * Class UserParametersValidator
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class UserParametersValidator
{
//  const MIN_PASSWORD_LENGTH = 8;
//  const MAX_PASSWORD_LENGTH = 32;
//
//  const MAX_USERNAME_LENGTH = 32;
//  const MIN_USERNAME_LENGTH = 3;
//  const MAX_FIRST_NAME_LENGTH = 32;
//  const MAX_NOTES_LENGTH = 255;
//  const MAX_LAST_NAME_LENGTH = 32;
//  const MAX_AFFILIATION_LENGTH = 255;


  /**
   * Validates the parameters used to build a project.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
    if (array_key_exists("username", $params)) {

      GenericValidator::validateUsername($params['username']);

    }

    if (array_key_exists("email", $params)) {

      GenericValidator::validateEmail($params['email']);

    }

    if (array_key_exists("password", $params)) {

      GenericValidator::validatePassword($params['password'], $params['username']);

    }

    if (array_key_exists("password", $params) && array_key_exists("current_password", $params)) {
      if ($params['password'] == $params['current_password']) {
        throw new ParameterNotValidException("password", "Your new password must be different from your current one.");
      }
    }

    if (array_key_exists("password", $params) && array_key_exists("confirm_password", $params)) {
      if ($params['password'] != $params['confirm_password']) {
        throw new ParameterNotValidException("confirm_password", "Password does not match confirm password.");
      }
    }

    if (array_key_exists("firstName", $params) && !empty($params['firstName'])) {
      GenericValidator::validateFirstName($params['firstName']);
    }

    if (array_key_exists("lastName", $params) && !empty($params['lastName'])) {
      GenericValidator::validateLastName($params['lastName']);
    }

    if (array_key_exists("affiliation", $params) && !empty($params['affiliation'])) {
      GenericValidator::validateAffiliation($params['affiliation']);
    }

    if (array_key_exists("notes", $params) && !empty($params['notes'])) {
      GenericValidator::validateNotes($params['notes']);
    }

    return true;
  }

}