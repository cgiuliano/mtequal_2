<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/2017
 * Time: 12:03
 */

namespace AppBundle\Util;


class ImmutableParametersFilter
{
  /**
   * @param array $params
   * @param $immutableParameters
   */
  public static function remove(array &$params, $immutableParameters)
  {
    if (!$params) {
      return;
    }

    if (!$immutableParameters) {
      return;
    }

    foreach ($immutableParameters as $immutableParameter) {
      unset($params[$immutableParameter]);
    }
  }
}