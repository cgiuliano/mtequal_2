<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 30/11/17
 * Time: 15:42
 */

namespace AppBundle\Util;


class PasswordGenerator
{
  const chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_.$!/\"#%&'()*+,-.:;<=>?@[]^_`{|}~";

  /**
   * Returns a valid random password.
   *
   * @param $length
   * @return string
   */
  public static function generate($length)
  {
    $password = '-2$';
    $length = $length - strlen($password);
    for ($i = 0; $i < $length; $i++) {

      $len = strlen(self::chars) - 1;
      $password .= self::chars[mt_rand(0, $len)];
    }

    return $password;
  }
}