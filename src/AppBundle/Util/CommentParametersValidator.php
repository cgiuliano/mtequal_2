<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 18/12/2017
 * Time: 08:26
 */

namespace AppBundle\Util;

/**
 * Class CommentParametersValidator
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class CommentParametersValidator
{
  /**
   * Validates the parameters used to build a project.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
    if (array_key_exists("comment", $params)) {
      GenericValidator::validateComment($params['comment']);
    }

    if (array_key_exists("obj", $params)) {
      GenericValidator::validateStringIdentifier($params['obj']);
    }

    if (array_key_exists("refid", $params)) {
      GenericValidator::validateIntIdentifier($params['refid']);
    }

    return true;
  }
}