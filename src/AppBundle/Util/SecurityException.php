<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 18/12/2017
 * Time: 12:07
 */

namespace AppBundle\Util;

class SecurityException extends GenericException
{
  /**
   * SecurityException constructor.
   * @param string $message
   * @param int $code
   * @param string|null $tip
   */
  public function __construct($message = 'A security error has occurred.', $code = 23, $tip = null)
  {
    parent::__construct($message, $code, $tip);
  }

}