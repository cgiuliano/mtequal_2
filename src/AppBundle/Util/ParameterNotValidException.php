<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 15:06
 */

namespace AppBundle\Util;

/**
 * Class ParameterNotValidException
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class ParameterNotValidException extends ParameterException
{
  /**
   * ParameterNotValidException constructor.
   * @param string $parameter
   * @param string $message
   * @param string $tip
   */
  public function __construct($parameter, $message = 'Parameter not valid.', $tip = 'Please specify a valid value.')
  {
    parent::__construct($parameter, 1, $message, $tip);
  }
}