<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 15:22
 */

namespace AppBundle\Util;


use HTMLPurifier;
use HTMLPurifier_Config;

/**
 * Class ProjectParametersSanitizer
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class ProjectParametersSanitizer
{
  /**
   * Sanitizes the parameters used to build a project.
   *
   * @param array|null $params
   */
  public static function sanitize(array &$params = null)
  {
    if (!$params) {
      return;
    }

    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    //pid
    self::purify($params, 'pid', $purifier, true, true);

    //name
    self::purify($params, 'name', $purifier, true, true);

    //description
    self::purify($params, 'description', $purifier, true);

    //password
    //self::purify($params, 'password', $purifier);

    //instructions
    self::purify($params, 'instructions', $purifier);

    //type
    self::purify($params, 'type', $purifier, true);

    //ranges
    self::purify($params, 'ranges', $purifier, true, true);

    //requiref
    self::purify($params, 'requiref', $purifier, true, true);

    //randout
    self::purify($params, 'randout', $purifier, true, true);

    //blindrev
    self::purify($params, 'blindrev', $purifier, true, true);

    //sentlist
    self::purify($params, 'sentlist', $purifier, true, true);

    //intragree
    self::purify($params, 'intragree', $purifier, true, true);

    //maxdontknow
    self::purify($params, 'maxdontknow', $purifier, true, true);

    //pooling
    self::purify($params, 'pooling', $purifier, true, true);

    //todo purify also the remaing params
  }

  /**
   * @param $params
   * @param $param
   * @param HTMLPurifier $purifier
   * @param bool $stripTags
   * @param bool $trim
   */
  private static function purify(&$params, $param, $purifier, $stripTags = false, $trim = false)
  {
    if (array_key_exists($param, $params) && $params[$param] != null && strlen($params[$param]) > 0) {
      if ($trim) {
        $params[$param] = trim($params[$param]);
      }
      if ($stripTags) {
        $params[$param] = strip_tags($params[$param]);
      }
      $params[$param] = $purifier->purify($params[$param]);

    }
  }
}