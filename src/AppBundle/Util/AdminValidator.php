<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-07
 * Time: 10:17
 */

namespace AppBundle\Util;

/**
 * Class AdminValidator
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class AdminValidator
{
  /**
   * Validates the parameters used to build a project.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
    if (array_key_exists("pid", $params)) {
      GenericValidator::validateIntIdentifier($params['pid']);
    }

    if (array_key_exists("view", $params)) {
      self::validateView($params['view']);
    }

    return true;
  }

  /**
   * @param $view
   * @throws ParameterNotValidException
   */
  public static function validateView($view)
  {
    if ($view == null) {
      throw new ParameterNotValidException("view", "Invalid view.", "Specify a view.");
    }

    if ($view != 'overview' && $view != 'monitor' && $view != 'evaluation-launch' && $view != 'evaluation-progress' && $view != 'report' && $view != 'review' &&
    $view != 'settings.details' && $view != 'settings.delete' && $view != 'settings.clone' && $view != 'settings.evalformat' && $view != 'settings.guidelines' &&
      $view != 'settings.instructions' && $view != 'settings.import' && $view != 'settings.export' && $view != 'settings.access' && $view != 'settings.deadlines') {
      throw new ParameterNotValidException("role", "Invalid view.", "Specify a valid view, it must be 'overview', 'monitor', 'evaluation-launch', 'evaluation-progress', 'report', 'settings.details', 'settings.delete', 'settings.clone', 'settings.evalformat', 'settings.guidelines', 'settings.instructions', 'settings.import', 'settings.export', 'settings.access', or 'settings.deadlines'.");
    }
  }
}