<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 15:22
 */

namespace AppBundle\Util;

/**
 * Class ProjectParametersValidator
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class ProjectParametersValidator
{

  /**
   * Validates the parameters used to build a project.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
    if (array_key_exists("name", $params)) {
      GenericValidator::validateProjectName($params['name']);
    }

    if (array_key_exists("description", $params)) {
      GenericValidator::validateDescription($params['description']);
    }

    if (array_key_exists("instructions", $params)) {
      GenericValidator::validateInstructions($params['instructions']);
    }

    if (array_key_exists("type", $params)) {
      GenericValidator::validateType($params['type']);
    }

    if (array_key_exists("ranges", $params) && array_key_exists("type", $params) && ($params['type'] === 'scale' || $params['type'] === 'binary')) {
      GenericValidator::validateRanges($params['ranges']);
    }

    //requiref
    if (array_key_exists("requiref", $params)) {
      if (is_null(filter_var($params['requiref'], FILTER_VALIDATE_BOOLEAN))) {
        throw new ParameterNotValidException("requiref", "Include reference must be boolean.");
      }
    }

    //randout
    if (array_key_exists("randout", $params)) {
      if (is_null(filter_var($params['randout'], FILTER_VALIDATE_BOOLEAN))) {
        throw new ParameterNotValidException("requiref", "Randomize targets must be boolean.");
      }
    }

    //blindrev
    if (array_key_exists("blindrev", $params)) {
      if (is_null(filter_var($params['blindrev'], FILTER_VALIDATE_BOOLEAN))) {
        throw new ParameterNotValidException("blindrev", "Anonymize evaluators must be boolean.");
      }
    }

    //sentlist
    //todo verify if deprecated
    if (array_key_exists("sentlist", $params)) {
      if (is_null(filter_var($params['sentlist'], FILTER_VALIDATE_BOOLEAN))) {
        throw new ParameterNotValidException("sentlist", "Must be boolean.");
      }
    }

    //intragree
    if (array_key_exists("intragree", $params)) {

      if (filter_var($params['intragree'], FILTER_VALIDATE_INT, array("options" => array("min_range" => 0, "max_range" => 100))) === false) {
        throw new ParameterNotValidException("intragree", "The percentage of translation units to use to calculate the intra-annotator agreement must be an integer between 0 and 100.");
      }
    }

    //maxdontknow
    if (array_key_exists("maxdontknow", $params)) {

      if (filter_var($params['maxdontknow'], FILTER_VALIDATE_INT, array("options" => array("min_range" => 0))) === false) {
        throw new ParameterNotValidException("maxdontknow", "The number of \"don't know\" answers must be integer greater than or equal to 0.");
      }
    }

    //pooling
    if (array_key_exists("pooling", $params)) {
      GenericValidator::validatePooling($params['pooling']);
    }
    return true;
  }

}