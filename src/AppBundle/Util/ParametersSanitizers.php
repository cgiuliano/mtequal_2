<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-06
 * Time: 18:36
 */

namespace AppBundle\Util;

use HTMLPurifier;
use HTMLPurifier_Config;

abstract class ParametersSanitizers
{
  /**
   * @param $params
   * @param $param
   * @param HTMLPurifier $purifier
   * @param bool $stripTags
   * @param bool $trim
   */
  protected static function purify(&$params, $param, $purifier, $stripTags = false, $trim = false)
  {
    if (array_key_exists($param, $params) && $params[$param] != null && strlen($params[$param]) > 0) {
      if ($trim) {
        $params[$param] = trim($params[$param]);
      }
      if ($stripTags) {
        $params[$param] = strip_tags($params[$param]);
      }
      $params[$param] = $purifier->purify($params[$param]);

    }
  }
}