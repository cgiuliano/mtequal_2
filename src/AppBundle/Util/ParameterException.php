<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 15:06
 */

namespace AppBundle\Util;

/**
 * Class ParameterException
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class ParameterException extends GenericException
{
  private $parameter;

  /**
   * ParameterException constructor.
   * @param string $parameter
   * @param int $code
   * @param string $message
   * @param string $tip
   */
  public function __construct($parameter, $code, $message = 'Parameter not valid.', $tip = 'Please enter a valid value.')
  {
    $this->parameter = $parameter;
    parent::__construct($message, $code, $tip);
  }

  /**
   * @return mixed
   */
  public function getParameter()
  {
    return $this->parameter;
  }


}