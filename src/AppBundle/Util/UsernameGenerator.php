<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 07/12/17
 * Time: 07:56
 */

namespace AppBundle\Util;

use Exception;

/**
 * Class UsernameGenerator
 *
 * @author giuliano
 * @package AppBundle\Util
 * @deprecated
 */
class UsernameGenerator
{
  public static function fromEmail($email)
  {
    $pos = strpos($email, '@');
    if ($pos === false) {
      throw new Exception('Invalid email address: \'' . $email . '\'');
    }
    return substr($email, 0, $pos) . '_' . StringGenerator::getNumber(4);
  }
}