<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 17/11/17
 * Time: 12:27
 */

namespace AppBundle\Util;


/**
 * Class RequiredParametersChecker
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class RequiredParametersChecker
{
  /**
   * @param array $params
   * @param array $requiredParams
   * @return bool
   * @throws MissingRequiredParameterException
   */
  public static function check(array $params, array $requiredParams)
  {
    foreach ($requiredParams as $requiredParam) {
      if (!array_key_exists($requiredParam, $params)) {
        throw new MissingRequiredParameterException($requiredParam, 'Missing required parameter \'' . $requiredParam . '\'.');
      }
    }
    return true;
  }
}