<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 01/12/17
 * Time: 12:13
 */

namespace AppBundle\Util;


class ReplyParametersValidation
{
  /**
   * Validates the parameters used by a user to reply.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
//    if (array_key_exists("hash", $params)) {
//      GenericValidator::validateHash($params['hash']);
//    }

    if (array_key_exists("action", $params)) {
      GenericValidator::validateReplyAction($params['action']);
    }
    return true;
  }
}