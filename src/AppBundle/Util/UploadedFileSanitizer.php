<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 16/11/17
 * Time: 14:49
 */

namespace AppBundle\Util;


use Exception;
use HTMLPurifier;
use HTMLPurifier_Config;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedFileSanitizer
{

  /**
   * @param $in
   * @param $out
   */
  public static function purify($in, $out)
  {
    try {
      $config = HTMLPurifier_Config::createDefault();
      $purifier = new HTMLPurifier($config);;
      $content = file_get_contents($in);
      $cleanContent = $purifier->purify($content);

      file_put_contents($out, $cleanContent);

    } catch (Exception $e) {
      //throw $e;
    }
  }

  /**
   * @param UploadedFile $uploadedFile
   * @param bool $stripTags
   * @throws SanitizationException
   */
  public static function sanitize(UploadedFile $uploadedFile, $stripTags = false)
  {
    try {
      $config = HTMLPurifier_Config::createDefault();
      $purifier = new HTMLPurifier($config);;
      $content = file_get_contents($uploadedFile);
      $cleanContent = $purifier->purify($content);

      if ($stripTags) {
        $cleanContent = strip_tags($cleanContent);
      }

      file_put_contents($uploadedFile, $cleanContent);

    } catch (Exception $e) {
      throw new SanitizationException('file', 'Error sanitizing the uploaded file.' . 'Specify a valid file.');
    }
  }
}