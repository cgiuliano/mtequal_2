<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 18/12/2017
 * Time: 08:18
 */

namespace AppBundle\Util;


use HTMLPurifier;
use HTMLPurifier_Config;

/**
 * Class CommentParametersSanitizer
 * @package AppBundle\Util
 */
class CommentParametersSanitizer
{

  /**
   * Sanitizes the parameters used to build a user.
   *
   * @param array|null $params
   */
  public static function sanitize(array &$params = null)
  {
    if (!$params) {
      return;
    }

    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);

    self::purify($params, 'comment', $purifier, true);
  }

  /**
   * @param $params
   * @param $param
   * @param HTMLPurifier $purifier
   * @param bool $stripTags
   * @param bool $trim
   */
  private static function purify(&$params, $param, $purifier, $stripTags = false, $trim = false)
  {
    if (array_key_exists($param, $params) && $params[$param] != null && strlen($params[$param]) > 0) {
      if ($trim) {
        $params[$param] = trim($params[$param]);
      }
      if ($stripTags) {
        $params[$param] = strip_tags($params[$param]);
      }
      $params[$param] = $purifier->purify($params[$param]);

    }
  }
}