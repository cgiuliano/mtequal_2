<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 30/11/17
 * Time: 12:53
 */

namespace AppBundle\Util;


class InviteParametersValidation
{
  /**
   * Validates the parameters used to invite a user.
   *
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validate(array $params)
  {
    if (array_key_exists("role", $params)) {
      GenericValidator::validateRole($params['role']);
    }

    if (array_key_exists("email", $params)) {
      GenericValidator::validateEmail($params['email']);
    }

    if (array_key_exists("pid", $params)) {
      GenericValidator::validateIntIdentifier($params['pid']);
    }

    if (array_key_exists("uid", $params)) {
      GenericValidator::validateIntIdentifier($params['uid']);
    }
    return true;
  }
}