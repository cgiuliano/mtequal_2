<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/11/17
 * Time: 18:20
 */

namespace AppBundle\Util;

/**
 * Class MissingRequiredParameterException
 *
 * @author giuliano
 * @package AppBundle\Util
 */
class MissingRequiredParameterException extends ParameterException
{
  /**
   * MissingRequiredParameterException constructor.
   * @param string $parameter
   * @param string $message
   * @param $tip
   */
  public function __construct($parameter, $message = 'Missing required parameter.', $tip = 'Please specify the missing parameter.')
  {
    parent::__construct($parameter, 2, $message, $tip);
  }

}