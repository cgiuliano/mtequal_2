<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 29/11/17
 * Time: 15:14
 */

namespace AppBundle\Util;


class StringGenerator
{
  //todo remove dot
  const chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_.-";
  //const chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  const numbers = "1234567890";

  /**
   * Returns a random token of specified length.
   *
   * @param $length
   * @return string
   */
  public static function getAlphaNumeric($length)
  {
    //todo verify the token is unique (see api key)

    $len = strlen(self::chars) - 1;
    $id = '';

    for ($i = 0; $i < $length; $i++) {
      $id .= self::chars[mt_rand(0, $len)];
    }
    return $id;
  }

  /**
   * Returns true if the input is a valid alpha numeric string; false otherwise.
   *
   * @param $string
   * @return bool
   */
  public static function isAlphaNumeric($string)
  {
    return preg_match('/^[\w\.-]+$/', $string, $matches);
  }

  /**
   * Returns a random token of specified length.
   *
   * @param $length
   * @return string
   */
  public static function getNumber($length)
  {
    //todo verify the token is unique (see api key)

    $len = strlen(self::numbers) - 1;
    $id = '';

    for ($i = 0; $i < $length; $i++) {
      $id .= self::numbers[mt_rand(0, $len)];
    }
    return $id;
  }

  /**
   * Returns true if the input is a valid alpha numeric string; false otherwise.
   *
   * @param $number
   * @return bool
   */
  public static function isNumeric($number)
  {
    return preg_match('/^\d+$/', $number, $matches);
  }
}