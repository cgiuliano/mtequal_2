<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 30/11/17
 * Time: 12:59
 */

namespace AppBundle\Util;


use DateTime;

class GenericValidator
{
  const MIN_PASSWORD_LENGTH = 8;
  const MAX_PASSWORD_LENGTH = 32;

  const MAX_USERNAME_LENGTH = 255;
  const MIN_USERNAME_LENGTH = 3;
  const MAX_FIRST_NAME_LENGTH = 255;
  const MAX_NOTES_LENGTH = 255;
  const MAX_LAST_NAME_LENGTH = 255;
  const MAX_AFFILIATION_LENGTH = 255;

  const MAX_PROJECT_NAME_LENGTH = 255;
  const MAX_PROJECT_DESCRIPTION_LENGTH = 1000;

  /**
   * @author giuliano
   * @param $date
   * @param string $format
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validateDate($date, $format = 'Y-m-d H:i:s')
  {
    $d = DateTime::createFromFormat($format, $date);
    if ($d && $d->format($format) == $date) {
      return true;
    }
    throw new ParameterNotValidException("date", "Invalid date format.", "Specify a valid date, it must be in the following format " . $format);
  }

  /**
   * @param $username
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validateUsername($username)
  {

    if ($username == null) {
      throw new ParameterNotValidException("username", "Invalid username.", "Enter a username.");
    }

    if (strlen($username) < self::MIN_USERNAME_LENGTH) {
      throw new ParameterNotValidException("username", "Invalid username.", "Enter a longer username, it must be at least " . self::MIN_USERNAME_LENGTH . " characters.");
    }

    if (strlen($username) > self::MAX_USERNAME_LENGTH) {
      throw new ParameterNotValidException("username", "Invalid username.", "Enter a shorter username, use no more than " . self::MAX_USERNAME_LENGTH . " characters.");
    }

    if (!preg_match("/^[a-zA-Z0-9_\.\-]+$/", $username)) {
      throw new ParameterNotValidException("username", "Invalid username.", "Enter a valid username, it can contain uppercase and lowercase English letters, numbers, . (dot) and - (hyphen), and _ (underscore)");
    }

    return true;
  }

  /**
   * @param $comment
   * @throws ParameterNotValidException
   */
  public static function validateComment($comment)
  {

    if ($comment == null) {
      throw new ParameterNotValidException("id", "Invalid comment.", "Enter a comment.");
    }

    if (strlen($comment) == 0) {
      throw new ParameterNotValidException("id", "Invalid comment.", "Enter a comment, it must be non-empty.");
    }

    if (strlen($comment) > 256) {
      throw new ParameterNotValidException("id", "Invalid comment.", "Enter a valid comment, it must be shorter than 256 characters.");
    }
  }

  /**
   * @param $id
   * @throws ParameterNotValidException
   */
  public static function validateStringIdentifier($id)
  {

    if ($id == null) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter an identifier.");
    }

    if (strlen($id) == 0) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter a valid identifier, it must be non-empty.");
    }

    if (strlen($id) > 256) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter a valid identifier, it must be shorter than 256 characters.");
    }
  }

  /**
   * @param $id
   * @throws ParameterNotValidException
   */
  public static function validateIntIdentifier($id)
  {

    if ($id == null) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter an identifier.");
    }

    if (strlen($id) == 0) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter a valid identifier.");
    }

    if (!filter_var($id, FILTER_VALIDATE_INT, array('min_range' => 0, 'max_range' => PHP_INT_MAX))) {
      throw new ParameterNotValidException("id", "Invalid identifier.", "Enter a valid identifier, it must be an integer greater than or equal to 0.");
    }
  }

  /**
   * @param $email
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validateEmail($email)
  {
    if ($email == null) {
      throw new ParameterNotValidException("email", "Enter a valid email address.");
    }

    if (strlen($email) == 0) {
      throw new ParameterNotValidException("email", "Enter a valid email address.");
    }

    // Validate e-mail
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      throw new ParameterNotValidException("email", "Enter a valid email address.");
    }

    return true;
  }

  /**
   * @param $password
   * @param null $username
   * @return bool
   * @throws ParameterNotValidException
   */
  public static function validatePassword($password, $username = null)
  {

    if ($password == null) {
      throw new ParameterNotValidException("password", "Invalid password.", "Enter a password.");
    }

    if (strlen($password) == 0) {
      throw new ParameterNotValidException("password", "Invalid password.", "Enter a password.");
    }

    if (strlen($password) < self::MIN_PASSWORD_LENGTH) {
      throw new ParameterNotValidException("password", "Invalid password.", "Enter a longer password, use at least " . self::MIN_PASSWORD_LENGTH . " characters.");
    }

    if (strlen($password) > self::MAX_PASSWORD_LENGTH) {
      throw new ParameterNotValidException("password", "Invalid password.", "Enter a shorter password, use no more than " . self::MAX_PASSWORD_LENGTH . " characters.");
    }

    if ($username != null && $password == $username) {
      throw new ParameterNotValidException("password", "Invalid password.", "Username cannot be used as password.");
    }

    $sum = 0;
    if (preg_match("/[a-z]+/", $password)) {
      $sum++;
    }

    if (preg_match("/[A-Z]+/", $password)) {
      $sum++;
    }

    if (preg_match("/[0-9]+/", $password)) {
      $sum++;
    }
    //todo quote - and . ?
    if (preg_match("/[ !\/\"#$%&'()*+,-.:;<=>?@[\]^_`{|}~]+/", $password)) {
      //if (preg_match("/[ !$]+/", $password)) {
      $sum++;
    }
    if ($sum < 3) {
      throw new ParameterNotValidException("password", "Invalid password.", "Enter a stronger password, it must contain at least three of the following classes: English uppercase and lowercase characters, numbers, and \" !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~\"");
    }
    return true;
  }

  /**
   * @param $firstName
   * @throws ParameterNotValidException
   */
  public static function validateFirstName($firstName)
  {
    if (strlen($firstName) > self::MAX_FIRST_NAME_LENGTH) {
      throw new ParameterNotValidException("firstName", "Invalid first name.", "Enter a shorter first name, use no more than " . self::MAX_FIRST_NAME_LENGTH . " characters.");
    }
  }

  /**
   * @param $notes
   * @throws ParameterNotValidException
   */
  public static function validateNotes($notes)
  {
    if (strlen($notes) > self::MAX_NOTES_LENGTH) {
      throw new ParameterNotValidException("notes", "Invalid notes.", "Enter a shorter first name, use no more than " . self::MAX_NOTES_LENGTH . " characters.");
    }
  }

  /**
   * @param $lastName
   * @throws ParameterNotValidException
   */
  public static function validateLastName($lastName)
  {
    if (strlen($lastName) > self::MAX_LAST_NAME_LENGTH) {
      throw new ParameterNotValidException("lastName", "Invalid last name.", "Enter a shorter last name, use no more than " . self::MAX_LAST_NAME_LENGTH . " characters.");
    }
  }

  /**
   * @param $affiliation
   * @throws ParameterNotValidException
   */
  public static function validateAffiliation($affiliation)
  {
    if (strlen($affiliation) > self::MAX_AFFILIATION_LENGTH) {
      throw new ParameterNotValidException("affiliation", "Invalid affiliation.", "Enter a shorter first name, use no more than " . self::MAX_AFFILIATION_LENGTH . " characters.");
    }
  }

  /**
   * @param $name
   * @throws ParameterNotValidException
   */
  public static function validateProjectName($name)
  {
    if ($name == null) {
      throw new ParameterNotValidException("name", "Invalid project name.", "Enter a name.");
    }

    if (strlen($name) == 0) {
      throw new ParameterNotValidException("name", "Invalid project name.", "Enter a name.");
    }

    if (strlen($name) > self::MAX_PROJECT_NAME_LENGTH) {
      throw new ParameterNotValidException("name", "Invalid project name.", "Enter a shorter name, use no more than " . self::MAX_PROJECT_NAME_LENGTH . " characters.");
    }

  }

  /**
   * @param $description
   * @throws ParameterNotValidException
   */
  public static function validateDescription($description)
  {
    if (strlen($description) > self::MAX_PROJECT_DESCRIPTION_LENGTH) {
      throw new ParameterNotValidException("description", "Invalid project description.", "Enter a shorter description, use no more than " . self::MAX_PROJECT_DESCRIPTION_LENGTH . " characters.");
    }
  }

  /**
   * @param $instructions
   * @throws ParameterNotValidException
   */
  public static function validateInstructions($instructions)
  {
    if ($instructions == null) {
      throw new ParameterNotValidException("instructions", "Invalid project instructions.", "Enter instructions.");
    }

    if (strlen($instructions) == 0) {
      throw new ParameterNotValidException("instructions", "Invalid project instructions.", "Enter instructions.");
    }
  }

  /**
   * @param $pooling
   * @throws ParameterNotValidException
   */
  public static function validatePooling($pooling)
  {
    /*   if ($pooling == null) {
         throw new ParameterNotValidException("pooling", "Specify a pooling algorithm.");
       }

       if (strlen($pooling) == 0) {
         throw new ParameterNotValidException("pooling", "Specify a pooling algorithm.");
       }*/

    if ($pooling != '' && $pooling != 'exact' && $pooling != 'exactins' && $pooling != 'bowins') {
      throw new ParameterNotValidException("pooling", "Invalid pooling algorithm.", "Specify a valid pooling algorithm, it must be 'exact', 'exactins', or 'bowins'");
    }
  }

  /**
   * @param $type
   * @throws ParameterNotValidException
   */
  public static function validateType($type)
  {
    if ($type == null) {
      throw new ParameterNotValidException("type", "Invalid evaluation format.", "Specify an evaluation format.");
    }

    if ($type != 'ranking' && $type != 'scale' && $type != 'binary' && $type != 'tree') {
      throw new ParameterNotValidException("type", "Invalid evaluation format.", "Specify a valid evaluation format, it must be 'ranking', 'scale', 'binary', or 'tree'.");
    }
  }

  /**
   * @param $format
   * @throws ParameterNotValidException
   */
  public static function validateExportDataFormat($format)
  {
    if ($format == null) {
      throw new ParameterNotValidException("format", "Invalid export format.", "Specify an export format for the evaluation data.");
    }

    if ($format != 'json' && $format != 'csv') {
      throw new ParameterNotValidException("format", "Invalid export format.", "Specify a valid export format for the evaluation data, it must be 'json' or 'csv'.");
    }
  }

  /**
   * @param $role
   * @throws ParameterNotValidException
   */
  public static function validateRole($role)
  {
    if ($role == null) {
      throw new ParameterNotValidException("role", "Invalid role.", "Specify a role.");
    }

    if ($role != 'admin' && $role != 'owner' && $role != 'evaluator' && $role != 'reviewer' && $role != 'vendor') {
      throw new ParameterNotValidException("role", "Invalid role.", "Specify a valid role, it must be 'owner', 'admin', 'vendor', 'evaluator', or 'reviewer'.");
    }
  }

  /**
   * @param $task
   * @throws ParameterNotValidException
   */
  public static function validateTask($task)
  {
    if ($task == null) {
      throw new ParameterNotValidException("task", "Invalid task.", "Specify a task.");
    }

    if ($task != 'creation' && $task != 'evalformat' && $task != 'import_corpus' && $task != 'guidelines' && $task != 'check_corpus' && $task != 'finalize' && $task != 'evaluation' && $task != 'review') {
      throw new ParameterNotValidException("role", "Invalid task.", "Specify a valid role, it must be 'creation', 'evalformat', 'import_corpus', 'guidelines', 'check_corpus', 'review', 'evaluation', or 'finalize'.");
    }
  }

  /**
   * @param $ranges
   * @throws ParameterNotValidException
   */
  public static function validateRanges($ranges)
  {
    if ($ranges == null) {
      throw new ParameterNotValidException("ranges", "Invalid ranges.", "Specify ranges for the evaluation format.");
    }

    $array = json_decode($ranges);
//    if ($array == null) {
//      throw new ParameterNotValidException("ranges", "Invalid ranges.", "Specify valid ranges for the evaluation format.");
//    }

    /*    if (count($array) < 2) {
          throw new ParameterNotValidException("ranges", "Specify at least 2 ranges for the evaluation format.");
        }*/
    if ($array == null) {
      return;
    }
    foreach ($array as $range) {

      if ($range->color) {
        if (!preg_match("/^#[0-9abcdefABCDEF]{1,6}$/", $range->color)) {
          throw new ParameterNotValidException("ranges", "Invalid ranges.", "Specify a valid color code for the evaluation format.");
        }
      }

      if ($range->label) {
        if (!preg_match("/^[a-zA-Z0-9_\.\-][a-zA-Z0-9_\.\- ]*$/", $range->label)) {
          throw new ParameterNotValidException("ranges", "Invalid ranges.", "Specify a valid label for the evaluation format, it can contain uppercase and lowercase English letters, numbers, space, . (dot) and - (hyphen), and _ (underscore)");
        }
      }
    }
  }

  /**
   * @param $action
   * @throws ParameterNotValidException
   */
  public static function validateReplyAction($action)
  {
    if ($action == null) {
      throw new ParameterNotValidException("action", "Invalid reply action.", "Specify an action.");
    }

    if (strlen($action) == 0) {
      throw new ParameterNotValidException("action", "Invalid reply action.", "Specify an action.");
    }

    if ($action != 'accepted' && $action != 'declined') {
      throw new ParameterNotValidException("action", "Invalid reply action.", "Specify a valid action, it must be 'accepted', or 'declined'");
    }
  }
}