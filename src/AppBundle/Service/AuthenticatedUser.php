<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 30/11/17
 * Time: 12:16
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthenticatedUser
{
  /**
   * @var Session
   */
  private $session;

  /**
   * SessionValidator constructor.
   *
   * @author giuliano
   * @param Session $session
   */
  public function __construct(Session $session)
  {
    $this->session = $session;
  }

  /**
   * @author giuliano
   * @return bool
   * @throws AuthenticationException
   */
  public function isAuthenticated()
  {
    $user = $this->session->get('user');

    if (is_null($user)) {
      throw new AuthenticationException();
    }

    return true;
  }

  /**
   * @author giuliano
   * @return User
   * @throws AuthenticationException
   */
  public function get()
  {
    $user = $this->session->get('user');

    if (is_null($user)) {
      throw new AuthenticationException('Cannot authenticate the user: your session is expired.', 'Please try to login again.');
    }

    return $user;
  }

  /**
   * @author giuliano
   * @param $user
   */
  public function set($user)
  {
    $this->session->set('user', $user);
  }

//  /**
//   * @author giuliano
//   */
//  public function start()
//  {
//    $id = $this->session->getId();
//    $this->session->start();
//    $id = $this->session->getId();
//  }

  /**
   * @author giuliano
   */
  public function invalidate()
  {
    $id = $this->session->getId();
    //$this->session->clear();
    $this->session->invalidate(0);
    $id = $this->session->getId();
  }


  /**
   * @author giuliano
   */
  public function clear()
  {
    $id = $this->session->getId();
    $this->session->clear();
    $id = $this->session->getId();
  }


  /**
   * @author giuliano
   */
  public function migrate()
  {
    $id = $this->session->getId();
    //$this->session->clear();
    //$this->session->invalidate(0);
    $this->session->migrate();
    $id = $this->session->getId();
  }
}