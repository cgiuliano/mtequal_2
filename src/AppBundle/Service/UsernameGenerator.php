<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 15/01/18
 * Time: 11:53
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Exception;

class UsernameGenerator
{
  private $em;

  /**
   * AuthorizedUser constructor.
   * @param EntityManager $entityManager
   */
  public function __construct(EntityManager $entityManager)
  {
    $this->em = $entityManager;
  }

  /**
   * @author giuliano
   * @param string $username
   * @return bool
   */
  private function isTaken($username)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->em->getRepository('AppBundle:User');
    /** @var User $user */
    $user = $userRepository->findOneBy(array("username" => $username));
    return ($user != null);
  }

  /**
   * @author giuliano
   * @param $username
   * @return bool
   */
  private function isShort($username)
  {
    return strlen($username) < 3;
  }

  /**
   * Returns a user name from the email.
   *
   * @param $email
   * @return bool|string
   * @throws Exception
   */
  public function fromEmail($email)
  {
    $pos = strpos($email, '@');
    if ($pos === false) {
      throw new Exception('Invalid email address: \'' . $email . '\'');
    }
    $prefix = substr($email, 0, $pos);
    $counter = 0;
    do {
      //this is to avoid an infinite loop
      if ($counter > 25) {
        throw new Exception('Cannot create a username.');
      }

      if ($counter >= 1) {
        $username = $prefix . '_' . $counter;
      } else {
        $username = $prefix;
      }

      $counter++;
      //} while ($this->isShort($username) || $this->isTaken($username));
    } while ($this->isTaken($username));


    return $username;
  }
}