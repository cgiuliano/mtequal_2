<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 29/11/17
 * Time: 11:36
 */

namespace AppBundle\Service;


use AppBundle\Util\ParameterNotValidException;

/**
 * Class EmailDomainValidator
 * @package AppBundle\Service
 */
class EmailDomainValidator
{
  /**
   * @var array
   */
  private $emailDomains;


  /**
   * AccountValidator constructor.
   *
   * @author giuliano
   * @param array $emailDomains
   */
  public function __construct(array $emailDomains)
  {
    $this->emailDomains = $emailDomains;
  }

  /**
   * @author giuliano
   * @param array $params
   * @return bool
   * @throws ParameterNotValidException
   */
  public function validate(array $params)
  {
    if (!$params) {
      return true;
    }

    if (!$this->emailDomains) {
      return true;
    }

    if (array_key_exists("email", $params) && $params['email']) {

      foreach ($this->emailDomains as $emailDomain) {
        $regex = '/^[^@]+@' . $emailDomain . '$/';
        if (preg_match($regex, $params['email'])) {
          return true;
        }
      }
      //todo remove the admissible domains are
      throw new ParameterNotValidException('email', 'Invalid email address.', 'Enter a valid email address, admissible domains are ' . implode(', ', $this->emailDomains));
    }

    return false;
  }

}