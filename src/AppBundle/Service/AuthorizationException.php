<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 05/12/17
 * Time: 19:16
 */

namespace AppBundle\Service;

use AppBundle\Util\SecurityException;

/**
 * Class AuthorizationException
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class AuthorizationException extends SecurityException
{
  /**
   * AuthorizationException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string $tip
   */
  public function __construct($message = 'An authorization error has occurred.', $tip = 'Please login with correct role.')
  {
    parent::__construct($message, 11, $tip);
  }
}