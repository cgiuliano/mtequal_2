<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/12/2017
 * Time: 07:37
 */

namespace AppBundle\Service;


use AppBundle\Entity\Project;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\RoleRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class AuthorizedUser
 * @package AppBundle\Service
 */
class AuthorizedUser
{
  private $em;

  /**
   * AuthorizedUser constructor.
   * @param EntityManager $entityManager
   */
  public function __construct(EntityManager $entityManager)
  {
    $this->em = $entityManager;
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isAdmin($user, $project)
  {
    return $this->isAuthorized($user, $project, array('admin'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isOwnerOrAdmin($user, $project)
  {
    return $this->isAuthorized($user, $project, array('admin', 'owner'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isVendor($user, $project)
  {
    return $this->isAuthorized($user, $project, array('vendor'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isReviewer($user, $project)
  {
    return $this->isAuthorized($user, $project, array('reviewer'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isEvaluator($user, $project)
  {
    return $this->isAuthorized($user, $project, array('evaluator'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @return bool
   * @throws AuthorizationException
   */
  public function isOwner($user, $project)
  {
    return $this->isAuthorized($user, $project, array('owner'));
  }

  /**
   * @author giuliano
   * @param User $user
   * @param Project $project
   * @param array $roles
   * @return bool
   * @throws AuthorizationException
   */
  public function isAuthorized($user, $project, $roles)
  {
    /** @var RoleRepository $roleRepository */
    $roleRepository = $this->em->getRepository("AppBundle:Role");
    /** @var Role $role */

    $userRoles = $roleRepository->findBy(array('user' => $user->getId(), 'project' => $project->getId()));
    foreach ($roles as $role) {
      /** @var Role $userRole */
      foreach ($userRoles as $userRole) {
        if ($userRole->getRole() == $role) {
          return true;
        }
      }

    }

    throw  new AuthorizationException('User not authorized.', 'You need one of the following permission roles: ' . implode(',', $roles));
  }
}