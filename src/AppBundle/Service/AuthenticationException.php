<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 30/11/17
 * Time: 12:31
 */

namespace AppBundle\Service;


use AppBundle\Util\SecurityException;


class AuthenticationException extends SecurityException
{
  /**
   * SessionException constructor.
   * @param string $message
   * @param string $tip
   */
  public function __construct($message = 'An authentication error has occurred.', $tip = 'Please try to login again.')
  {
    parent::__construct($message, 7, $tip);
  }
}