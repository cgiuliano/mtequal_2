<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 22/11/17
 * Time: 11:27
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class ProgressException
 *
 * @author giuliano
 * @package AppBundle\Repository
 */
class ProgressException extends GenericException
{
  /**
   * ProgressException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 6, $tip);
  }
}