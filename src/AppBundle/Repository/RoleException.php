<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 22/12/17
 * Time: 15:39
 */

namespace AppBundle\Repository;


use AppBundle\Util\GenericException;

/**
 * Class RoleException
 *
 * @author giuliano
 * @package AppBundle\Repository
 */
class RoleException extends GenericException
{
  /**
   * RoleException constructor.
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 18, $tip);
  }
}