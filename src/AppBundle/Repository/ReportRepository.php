<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-01
 * Time: 17:10
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Project;
use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Exception;

/**
 * Class ReportRepository
 * @package AppBundle\Repository
 */
class ReportRepository extends EntityRepository
{
  /**
   * Returns a token for the specified pair project/user, the token is created if it doesn't exist yet.
   *
   * @param Project $project
   * @param User $user
   * @return Report
   * @throws ReportException
   */
  public function create($project, $user)
  {
    try {

      $report = $this->findOneBy(array("project" => $project->getId(), "user" => $user->getId()));

      if ($report == null) {
        $report = new Report($project->getId(), $user->getId());
        $this->save($report);
      }

      return $report;
    } catch (Exception $e) {
      throw new ReportException('Cannot share the report.', 'Try again.');
    }

  }

  /**
   * Returns the token with the project and user information
   *
   * @param $token
   * @return Report
   * @throws ReportException
   */
  public function getReport($token)
  {
    /** @var Report $report */
    $report = $this->findOneBy(array("token" => $token));
    if ($report){
      return $report;
    }

    throw new ReportException("Report not found.","Specify a valid token");
  }

  /**
   * @param $token
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function save($token)
  {
    $em = $this->getEntityManager();
    $em->persist($token);
    $em->flush();
  }
}