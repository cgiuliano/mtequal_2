<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 22/11/17
 * Time: 11:27
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class ProjectException
 *
 * @author giuliano
 * @package AppBundle\Repository
 */
class ProjectException extends GenericException
{
  /**
   * ProjectException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 10, $tip);
  }
}