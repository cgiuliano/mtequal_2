<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Document;
use AppBundle\Util\MissingRequiredParameterException;
use AppBundle\Util\ParameterNotValidException;
use AppBundle\Util\ParameterValueNotUniqueException;
use AppBundle\Util\RequiredParametersChecker;
use AppBundle\Util\ProjectParametersSanitizer;
use AppBundle\Util\ProjectParametersValidator;
use DateTime;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Project;
use Doctrine\ORM\Query;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Project repository class
 */
class ProjectRepository extends EntityRepository
{
  /**
   * get project object from ID
   * @param $id
   * @return null|object
   */
  public function findActiveProject($id)
  {
    return $this->findOneBy(array("id" => $id, "active" => true));
  }

  /**
   * Finds a project by id.
   *
   * @author giuliano
   * @param $params
   * @return Project
   * @throws ProjectException
   * @throws MissingRequiredParameterException
   */
  public function findActiveProjectById($params)
  {
    RequiredParametersChecker::check($params, ['pid']);

    /** @var Project $project */
    $project = $this->findOneBy(array("id" => $params['pid'], "active" => true));

    if ($project) {
      return $project;
    }

    throw new ProjectException('Project not found.', 'Specify a valid identifier.');
  }

  /**
   * Returns an array with project information.
   *
   * @param $id
   * @return array
   */
  public function getProject($id)
  {
    /** @var Project $project */
    $project = $this->findOneBy(array("id" => $id, "active" => true));
    if ($project != null && $project->getActive()) {
      return array(
        "id" => $project->getId(),
        "name" => $project->getName(),
        "type" => $project->getType(),
        "pooling" => $project->getPooling(),
        "description" => $project->getDescription(),
        "instructions" => $project->getInstructions(),
        "ranges" => $project->getRanges(),
        "requiref" => intval($project->getRequiref()),
        "randout" => intval($project->getRandout()),
        "blindrev" => intval($project->getBlindrev()),
        "sentlist" => intval($project->getSentlist()),
        "intragree" => intval($project->getIntragree()),
        "maxdontknow" => intval($project->getMaxdontknow()),
        "intratus" => intval($project->getIntratus()),
        //todo remove the following two keys
        "evalformat" => self::getDocumentName($project->getEvalformat()),
        "guidelines" => self::getDocumentName($project->getGuidelines()),
        "created" => $project->getCreated(),
        "modified" => $project->getModified()
      );
    }
    return array();
  }

  /**
   * returns an hash with information for all projects
   */
  public function getProjects()
  {
    $em = $this->getEntityManager();

    $dql = "SELECT p.id, p.name, p.type, p.ranges, p.active, p.modified
        FROM AppBundle:Project AS p
        ORDER BY p.name";

    /** @var Query $query */
    $query = $em->createQuery($dql);

    return $query->execute();
  }

  /**
   * @param array $params
   * @throws \AppBundle\Util\ParameterNotValidException
   */
  public function sanitizeAndValidate(array &$params)
  {
    ProjectParametersSanitizer::sanitize($params);
    ProjectParametersValidator::validate($params);
  }

  /**
   * @param array $params
   * @throws ParameterValueNotUniqueException
   */
  public function checkUniqueConstrain(array $params)
  {
    //todo project name could be shared
    //todo use transactions for check and save
    //this can be avoided using the unique columns on the DB
    //see http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/transactions-and-concurrency.html#approach-2-explicitly

    if (array_key_exists('name', $params) && !empty($params['name'])) {
      $anotherProject = $this->findOneBy(array('name' => $params['name'], 'active' => 1));
      if ($anotherProject) {
        throw new ParameterValueNotUniqueException('name', 'A project with this name already exists! Please, choose another name.');
      }
    }
  }

  /**
   * Creates a project entity.
   *
   * @author giuliano
   * @param array $params
   * @return Project
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws ParameterValueNotUniqueException
   * @throws ProjectException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function create(array $params)
  {
    RequiredParametersChecker::check($params, array('name'));
    $this->sanitizeAndValidate($params);
    $this->checkUniqueConstrain($params);

    $project = new Project();

    $project->setName($params['name']);
    if (array_key_exists('description', $params) && $params['description'] != null) {
      $project->setDescription($params['description']);
    } else {
      $project->setDescription('');
    }

    $now = new DateTime("now");
    $project->setModified($now);
    $project->setCreated($now);

    $this->save($project);
    return $project;
  }

  /**
   * Saves the specified project entity.
   *
   * @author giuliano
   * @param $project
   * @throws ProjectException
   * @throws \Doctrine\ORM\OptimisticLockException
   * @internal param $user
   */
  public function save($project)
  {
    try {
      $em = $this->getEntityManager();
      $em->persist($project);
      $em->flush();
    } catch (Exception $e) {
      throw new ProjectException('Error saving data.', 'Specify a valid project.');
    }
  }

  /**
   * Updates a project entity.
   *
   * @author giuliano
   * @param array $params
   * @return Project
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws ProjectException
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function update(array $params)
  {
    RequiredParametersChecker::check($params, array('id'));
    $this->sanitizeAndValidate($params);
    //todo check for other projects
    //$this->checkUniqueConstrain($params);

    /** @var Project $project */
    $project = self::findActiveProject($params['id']);

    if (array_key_exists('name', $params) && !empty($params['name'])) {
      $project->setName($params['name']);
    }

    if (array_key_exists('description', $params) && !empty($params['description'])) {
      $project->setDescription($params['description']);
    }

    //set the evaluation format
    if (array_key_exists('type', $params) && !empty($params['type'])) {
      $project->setType($params['type']);

      if ($params['type'] == 'binary' || $params['type'] == 'scale') {
        if (array_key_exists('ranges', $params) && !empty($params['ranges'])) {
          $project->setRanges($params['ranges']);
        } else {
          $project->setRanges('{}');
        }
      } else if ($params['type'] == 'tree') {
        $project->setRanges('[{"color":"#000000","label":"0","val":0},{"color":"#000000","label":"1","val":11111},{"color":"#000000","label":"2","val":11110},{"color":"#000000","label":"3","val":11113},{"color":"#000000","label":"4","val":11101},{"color":"#000000","label":"5","val":11100},{"color":"#000000","label":"6","val":11103},{"color":"#000000","label":"7","val":10111},{"color":"#000000","label":"8","val":10110},{"color":"#000000","label":"9","val":10113},{"color":"#000000","label":"10","val":10101},{"color":"#000000","label":"11","val":10100},{"color":"#000000","label":"12","val":10103},{"color":"#000000","label":"13","val":10011},{"color":"#000000","label":"14","val":10010},{"color":"#000000","label":"15","val":10013},{"color":"#000000","label":"16","val":10001},{"color":"#000000","label":"17","val":10000},{"color":"#000000","label":"18","val":10003}]');
      } else {
        $project->setRanges('{}');
      }
    }

    //set include reference
    if (array_key_exists('requiref', $params) && $params['requiref'] != null) {
      $project->setRequiref($params['requiref']);
    }

    //set the instructions
    if (array_key_exists('instructions', $params) && !empty($params['instructions'])) {
      $project->setInstructions($params['instructions']);
    }

    //set the values of guidelines specification
    if (array_key_exists('randout', $params) && $params['randout'] != null) {
      $project->setRandout($params['randout']);
    }

    if (array_key_exists('blindrev', $params) && $params['blindrev'] != null) {
      $project->setBlindrev($params['blindrev']);
    }

    if (array_key_exists('sentlist', $params) && !empty($params['sentlist'])) {
      $project->setSentlist($params['sentlist']);
    }

    if (array_key_exists('intragree', $params) && $params['intragree'] != null) {
      //todo remove setDonePooling
      $project->setDonePooling('0');
      $project->setDatasetCreated(false);
      if (intval($params['intragree']) > 0) {
        $project->setIntragree(intval($params['intragree']));
      } else {
        $project->setIntragree(0);
      }
    }

    if (array_key_exists('maxdontknow', $params) && $params['maxdontknow'] != null) {
      if (intval($params['maxdontknow']) >= 0) {
        $project->setMaxdontknow($params['maxdontknow']);
      } else {
        $project->setMaxdontknow(-1);
      }
    }

    if (array_key_exists('pooling', $params) && !empty($params['pooling'])) {
      //Claudio: this field is used to specify if the dataset has been generated
      ///todo remove setDonePooling
      $project->setDonePooling("0");
      $project->setDatasetCreated(false);
      if ($params['pooling'] != "") {
        $project->setPooling($params['pooling']);
      } else {
        $project->setPooling("");
      }
    }

    $project->setModified(new DateTime("now"));
    $this->save($project);

    return $project;
  }

  /**
   * Creates a new project or update an existing one.
   *
   * @param array $values
   * @return Project
   * @throws \Doctrine\ORM\OptimisticLockException
   * @deprecated
   */
  public function createProject(array $values)
  {
    $time = new DateTime("now");
    $em = $this->getEntityManager();
    /** @var Project $project */
    $project = self::findActiveProject($values['id']);
    if (!$project) {
      $project = new Project();
      $project->setCreated($time);
    }
    //set the project's name
    if (array_key_exists('name', $values)) {
      $name = trim($values['name']);
      if (!empty($name)) {
        $project->setName($values['name']);
      } else {
        return null;
      }
    }
    //set the evaluation format
    if (array_key_exists('type', $values)) {
      $project->setType($values['type']);

      if (array_key_exists('ranges', $values)) {
        $project->setRanges($values['ranges']);
      } else {
        $project->setRanges("{}");
      }

      //set the values with import corpus constrains
      // Claudio: fixed the bug
      if (array_key_exists('requiref', $values)) {
        if ($values['requiref']) {
          $project->setRequiref(true);
        } else {
          $project->setRequiref(false);
        }
      }

      //save evalformat document
      //todo deprecated?
      $docData = array("type" => $project->getType(),
        "ranges" => json_decode($project->getRanges(), true),
        "requiref" => $project->getRequiref());
      $docValues = array("type" => "evalformat",
        "format" => $project->getType(),
        "url" => $project->getName(),
        "data" => json_encode($docData));
      /** @var Document $doc */
      $doc = $em->getRepository("AppBundle:Document")->update($docValues);
      $project->setEvalformat($doc->getId());
    }


    //set the guidelines
    if (array_key_exists('instructions', $values)) {
      $project->setInstructions(trim(preg_replace('/<br>$/', '', $values['instructions'])));
    }

    //set the values of guidelines specification
    if (array_key_exists('randout', $values)) {
      if ($values['randout']) {
        $project->setRandout(true);
      } else {
        $project->setRandout(false);
      }
    }

    if (array_key_exists('blindrev', $values)) {
      if ($values['blindrev']) {
        $project->setBlindrev(true);
      } else {
        $project->setBlindrev(false);
      }
    }

    if (array_key_exists('sentlist', $values)) {
      if ($values['sentlist']) {
        $project->setSentlist(true);
      } else {
        $project->setSentlist(false);
      }
    }

    if (array_key_exists('intragree', $values)) {
      //todo improve comparing if the new value is different from the previous one
      $project->setDonePooling("0");
      $project->setDatasetCreated(false);
      if (intval($values['intragree']) > 0) {
        $project->setIntragree(intval($values['intragree']));
      } else {
        $project->setIntragree(0);
      }
    }

    if (array_key_exists('maxdontknow', $values)) {
      if (intval($values['maxdontknow']) >= 0) {
        $project->setMaxdontknow($values['maxdontknow']);
      } else {
        $project->setMaxdontknow(-1);
      }
    }

    if (array_key_exists('pooling', $values)) {
      //Claudio this field is used to specify if the corpus has been generated
      /*if ($project->getDonePooling() == null) {
        $project->setDonePooling($project->getPooling());
      }*/

      //todo improve comparing if the new value is different from the previous one
      $project->setDonePooling("0");
      $project->setDatasetCreated(false);
      if ($values['pooling'] != "") {
        $project->setPooling($values['pooling']);
      } else {
        $project->setPooling("");
      }
    }

    //save guidelines document
    // deprecated?
    $docData = array("randout" => $project->getRandout(),
      "blindrev" => $project->getBlindrev(),
      "sentlist" => $project->getSentlist(),
      "intragree" => $project->getIntragree(),
      "maxdontknow" => $project->getMaxdontknow(),
      "pooling" => $project->getPooling(),
      "instructions" => $project->getInstructions()
    );
    $docValues = array("type" => "guidelines",
      "format" => "",
      "url" => $project->getName(),
      "data" => json_encode($docData));

    /** @var Document $doc */
    $doc = $em->getRepository("AppBundle:Document")->update($docValues);
    $project->setGuidelines($doc->getId());


    //set the description
    if (array_key_exists('description', $values)) {
      $project->setDescription(trim($values['description']));
    }

    $project->setModified($time);
    $em->persist($project);
    $em->flush();

    return $project;
  }

  /**
   * Updates to now the modified time of the project specified by id.
   *
   * @author giuliano
   * @param $id
   * @return bool
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function updateModifiedTime($id)
  {
    /** @var Project $project */
    $project = self::findActiveProject($id);

    if ($project) {
      $time = new DateTime("now");
      $em = $this->getEntityManager();

      $project->setModified($time);
      $em->persist($project);
      $em->flush();
      return true;
    }
    return false;
  }

  /*
  * action can be: edit, delete, invite, annotate, evaluate
  */
  /**
   * @param $action
   * @param $projectid
   * @param $userid
   * @return int|mixed
   * @throws \Doctrine\ORM\NoResultException
   * @throws \Doctrine\ORM\NonUniqueResultException
   */
  public function can($action, $projectid, $userid)
  {

    $dql = "SELECT count(r.role)
        FROM AppBundle:RoleType AS t
        INNER JOIN AppBundle:Role AS r
        WHERE r.project=:projectid AND r.user=:userid AND t.name=r.role AND t.can_" . $action . "=1";

    try {
      $em = $this->getEntityManager();
      $query = $em->createQuery($dql);
      $query->setParameter('projectid', $projectid);
      $query->setParameter('userid', $userid);
      //$query->setParameter('action', $action);

      return $query->getSingleScalarResult();
    } catch (Exception $e) {
      //$logger->warn('QUERY FAILED: ' .$dql);
    }
    return 0;
  }

  /**
   * set a project as not active instead of remove it from database
   * @param $id
   * @return int
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function deleteProject($id)
  {
    $em = $this->getEntityManager();
    $project = self::findActiveProject($id);
    if ($project != null) {
      //set the flag like "disabled" to the project row instead remove the project and its dependencies: delete annotation, comment, roles, progress, sentences,...
      $project->setActive(false);
      //$em->remove($project);
      $em->persist($project);
      $em->flush();
      return 1;
    }
    return 0;
  }

  /**
   * Deactivate the specified project.
   *
   * @author giuliano
   * @param Project $project
   */
  public function deactivate($project)
  {
    $now = new DateTime("now");
    $project->setModified($now);
    $project->setActive(0);

    $this->save($project);
  }

  /**
   * Returns the list of project the specified user works for (active projects = not deleted).
   *
   * @param $userId
   * @param bool $orderByCreated
   * @return mixed|null
   */
  public function getProjectList($userId, $orderByCreated = false)
  {
    if ($userId != null) {
      $em = $this->getEntityManager();
      //todo review the query (WITH s.project=p.id)
      $dql = "SELECT p.id, p.name, p.type, p.description, p.modified, p.created, s.completed, GROUP_CONCAT(r.role SEPARATOR ', ') AS roles
        FROM AppBundle:Project AS p
        INNER JOIN AppBundle:Progress AS s WITH s.project=p.id  
        INNER JOIN AppBundle:Role AS r
        WHERE p.id=r.project AND p.active=1 AND r.user=:userid AND r.role!='' AND r.status='accepted' AND s.task='finalize'
        GROUP BY p.id, s.completed";
      if ($orderByCreated) {
        $dql .= " ORDER BY p.created DESC, p.name";
      } else {
        $dql .= " ORDER BY p.modified DESC, p.name";
      }

      /** @var Query $query */
      $query = $em->createQuery($dql);
      $query->setParameter('userid', $userId);

      return $query->execute();
    }
    return null;
  }

  /**
   * get document name
   * @deprecated
   */
  private function getDocumentName($docId)
  {
    if ($docId != 0) {
      /** @var Document $doc */
      $doc = $this->getEntityManager()->getRepository("AppBundle:Document")->find($docId);
      if ($doc) {
        return $doc->getUrl();
      }
    }
    return "";
  }
}