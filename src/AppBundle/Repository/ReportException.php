<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-01
 * Time: 17:10
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class TokenException
 * @package AppBundle\Repository
 */
class ReportException extends GenericException
{
  /**
   * ReportException constructor.
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 79, $tip);
  }
}