<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/12/2017
 * Time: 16:11
 */

namespace AppBundle\Repository;


use AppBundle\Util\GenericException;

class DatasetException extends GenericException
{
  /**
   * DatasetException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 20, $tip);
  }
}