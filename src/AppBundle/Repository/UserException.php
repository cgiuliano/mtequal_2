<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 17/11/17
 * Time: 12:06
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class UserException
 *
 * @author giuliano
 * @package AppBundle\Repository
 */
class UserException extends GenericException
{
  /**
   * UserException constructor.
   * @param string $message
   * @param string $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 4, $tip);
  }
}