<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 18/12/2017
 * Time: 09:16
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class CommentException
 * @package AppBundle\Repository
 */
class CommentException extends GenericException
{
  /**
   * CommentException constructor.
   *
   * @author giuliano
   * @param string $message
   */
  public function __construct($message)
  {
    parent::__construct($message, 16);
  }
}