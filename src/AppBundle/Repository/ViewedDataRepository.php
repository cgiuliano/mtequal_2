<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 11/12/2017
 * Time: 08:38
 */

namespace AppBundle\Repository;

use AppBundle\Entity\ViewedData;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Exception;

class ViewedDataRepository extends EntityRepository
{

  public function getViewedEvaluations($projectId, $userId)
  {
    $query = "SELECT v.num, v.modified
        FROM AppBundle:ViewedData AS v
        WHERE v.project=:project AND v.user=:user";
    /** @var Query $dql */
    $dql = $this->getEntityManager()->createQuery($query);
    $dql->setParameter("project", $projectId);
    $dql->setParameter("user", $userId);
    $data = $dql->getArrayResult();

    $hash = array();
    foreach ($data as $value) {
      $hash[$value["num"]] = $value["modified"];
    }
    return $hash;
  }

  /**
   * @param $num
   * @param $projectId
   * @param $userId
   * @throws AnnotationException
   */
  public function update($num, $projectId, $userId)
  {
    /** @var ViewedData $this */
    $viewedData = $this->findOneBy(array('num' => $num,
      'project' => $projectId,
      'user' => $userId));

    if ($viewedData) {
      $viewedData->setModified(new DateTime("now"));

    } else {
      $viewedData = new ViewedData($num, $projectId, $userId);
    }

    $this->save($viewedData);

  }

  /**
   * Saves the specified entity.
   *
   * @author giuliano
   * @param $viewedData
   * @throws AnnotationException
   */
  public function save($viewedData)
  {
    try {
      $em = $this->getEntityManager();
      $em->persist($viewedData);
      $em->flush();
    } catch (Exception $e) {
      throw new AnnotationException('Error saving viewed data.', 'Specify a valid entity.');
    }
  }

}