<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 09/12/2017
 * Time: 11:47
 */

namespace AppBundle\Repository;

use AppBundle\Util\GenericException;

/**
 * Class AnnotationException
 * @package AppBundle\Repository
 */
class AnnotationException extends GenericException
{
  /**
   * AnnotationException constructor.
   * @param string $message
   * @param int $tip
   */
  public function __construct($message, $tip)
  {
    parent::__construct($message, 24, $tip);
  }
}