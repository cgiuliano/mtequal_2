<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 25/10/15
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\RoleType;

class RoleTypeRepository extends EntityRepository
{
  /*
    * get all role types name
    */
  public function getAll()
  {
    return $this->findAll();
  }
}

?>