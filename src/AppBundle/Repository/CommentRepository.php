<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 10/04/16
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Comment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Exception;

/**
 * Class CommentRepository
 * @package AppBundle\Repository
 */
class CommentRepository extends EntityRepository
{
  const MAX_COMMENTS = 4;

  /**
   * Save a comment.
   *
   * @param $projectid
   * @param $userid
   * @param $text
   * @param $table
   * @param $refid
   * @return void
   * @throws CommentException
   * @deprecated
   */
  function saveComment($projectid, $userid, $text, $table, $refid)
  {
    /** @var Comment $comment */
    $comment = new Comment($userid, $projectid);

    $comment->setComment($text);
    $comment->setObj($table);
    $comment->setRefID(intval($refid));

    $this->save($comment);
  }

  /**
   * Save a comment.
   *
   * @author giuliano
   * @param $comment
   * @throws CommentException
   */
  public function save($comment)
  {
    try {
      $em = $this->getEntityManager();
      $em->persist($comment);
      $em->flush();
    } catch (Exception $e) {
      throw new CommentException('Cannot save the specified comment.');
    }
  }

  /**
   * Returns all comments.
   *
   * @author giuliano
   * @param $projectId
   * @param $obj
   * @param $refId
   * @return array
   */
  function getComments($projectId, $obj, $refId)
  {
    $query = "SELECT c.comment, c.userid, u.username, c.date
        FROM AppBundle:Comment AS c
        INNER JOIN AppBundle:User AS u
        WHERE c.userid=u.id AND c.project=:projectid AND c.obj=:obj AND c.refid=:refid";
    $parameters ['projectid'] = $projectId;
    $parameters ['obj'] = $obj;
    $parameters ['refid'] = $refId;

    $query .= " ORDER BY c.date ASC";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($parameters);
    return $dql->getResult();
  }

  /**
   * get the last comments
   * @param $projectid
   * @param $obj
   * @param $refid
   * @param null $userid
   * @param bool $showall
   * @return array
   */
  function lastComment($projectid, $obj, $refid, $userid = null, $showall = false)
  {
    $query = "SELECT c.comment, c.userid, u.username, c.date
        FROM AppBundle:Comment AS c
        INNER JOIN AppBundle:User AS u
        WHERE c.userid=u.id AND c.project=:projectid AND c.obj=:obj AND c.refid=:refid";
    $queryparam ['projectid'] = $projectid;
    $queryparam ['obj'] = $obj;
    $queryparam ['refid'] = $refid;

    if ($userid != null) {
      $query .= " AND c.userid=:userid";
      $queryparam ['userid'] = $userid;
    }
    $query .= " ORDER BY c.date ASC";

    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);
    if (!$showall) {
      $dql->setMaxResults(self::MAX_COMMENTS);
    }
    //
    //
    //
    return $dql->getResult();
  }

  /**
   * Returns the distribution of the comments for a project
   * todo: add the file counts
   * @param $projectId
   * @return array
   */
  public function getCommentsDistribution($projectId)
  {
    $hash = array();
    $queryparam ['project'] = $projectId;
    $query = "SELECT c.refid AS num, COUNT(c.refid) AS cnt
        FROM AppBundle:Comment AS c
        WHERE c.project=:project
        GROUP BY num";


    $dql = $this->getEntityManager()->createQuery($query)->setParameters($queryparam);

    foreach ($dql->getArrayResult() as $value) {

      $hash[$value["num"]] = $value["cnt"];
    }
    return $hash;

  }

  /**
   * get the number of comments for a specific sentence
   * @param $sentenceNum
   * @return mixed
   * @throws \Doctrine\ORM\NoResultException
   * @throws \Doctrine\ORM\NonUniqueResultException
   */
  public function getCommentCount($sentenceNum)
  {
    $query = "SELECT COUNT(c.refid) AS cnt
        FROM AppBundle:Comment AS c
        WHERE c.refid=:num";

    $dql = $this->getEntityManager()->createQuery($query)->setParameter("num", $sentenceNum);

    return $dql->getSingleScalarResult();

  }
}

?>