<?php

namespace AppBundle\Repository;


use AppBundle\Util\MissingRequiredParameterException;
use AppBundle\Util\ParameterNotValidException;
use AppBundle\Util\StringGenerator;
use AppBundle\Util\ImmutableParametersFilter;
use AppBundle\Util\ParameterValueNotUniqueException;
use AppBundle\Util\RequiredParametersChecker;
use AppBundle\Util\UserParametersSanitizer;
use AppBundle\Util\UserParametersValidator;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;
use Doctrine\ORM\Query;
use Exception;

/**
 * User repository class
 */
class UserRepository extends EntityRepository
{
  /*
  * get user information from ID
  */
  public function getUserFromID($id)
  {
    return $this->findOneBy(array("id" => $id));
  }

  /**
   * Finds and returns a user by email. null if the account doesn't exist.
   *
   * @param $email
   * @param $expiringTime
   * @return null|User
   * @throws Exception
   * @throws UserException
   */
  public function findUserByEmail($email, $expiringTime)
  {
    if (is_null($email)) {
      throw new UserException('Email not specified.', 'Specify a valid email.');
    }

    /** @var User $user */
    $user = $this->findOneBy(array('email' => $email));

    return $user;
  }

  /**
   * Finds and returns a user by email. Returns null if the account doesn't exist
   * or if the activation link (registered field) is expired.
   *
   * @param $email
   * @param $expiringTime
   * @return null|User
   * @throws Exception
   * @throws UserException
   */
  public function findNotExpiredUserByEmail($email, $expiringTime)
  {
    if (is_null($email)) {
      throw new UserException('Email not specified.', 'Specify a valid email.');
    }

    /** @var User $activeUser */
    $activeUser = $this->findOneBy(array('email' => $email, 'active' => 1));

    //if an active user exists, it is returned
    if ($activeUser) {
      return $activeUser;
    }

    //there is no an active user

    /** @var array $inactiveUsers */
    $inactiveUsers = $this->findBy(array('email' => $email, 'active' => 0),array('registered' => 'DESC'));

    //loop the inactive users to find one that can be still activated (the token is not expired)
    foreach ($inactiveUsers as $inactiveUser) {

      // the account must be still activated, if it's within the activation time the account is returned

      /** @var DateTime $expires */
      $expires = $inactiveUser->getRegistered();

      $expires->add(new DateInterval('PT' . $expiringTime . 'H'));
      $now = new DateTime("now");
      if ($now <= $expires) {
        // the token is not expired
        // the most recent registered account is returned
        return $inactiveUser;
      }
    }

    return null;
  }

  /**
   * Finds and returns a user by email. Returns null if the account doesn't exist
   * or if the activation link (registered field) is expired.
   *
   * @param $email
   * @param $expiringTime
   * @return null|User
   * @throws Exception
   * @throws UserException
   * @deprecated
   */
  public function findNotExpiredUserByEmailOld($email, $expiringTime)
  {
    if (is_null($email)) {
      throw new UserException('Email not specified.', 'Specify a valid email.');
    }

    /** @var User $user */
    $user = $this->findOneBy(array('email' => $email));

    if ($user) {

      // if the user is active can be returned
      if ($user->getActive()) {
        return $user;
      }
      // otherwise the account must be activated, if it's within the activation time the account is returned

      /** @var DateTime $expires */
      $expires = $user->getRegistered();

      $expires->add(new DateInterval('PT' . $expiringTime . 'H'));
      $now = new DateTime("now");
      if ($now > $expires) {
        // the token is expired
        return null;
      }
      return $user;
    }

    return null;
  }

  /*
  * get a user by column->value pairs
  */
  public function getUser(array $value)
  {
    return $this->findOneBy($value);
  }

  /**
   * returns an hash with information for all users
   */
  public function getUsers()
  {
    $em = $this->getEntityManager();

    $dql = "SELECT u.id, u.firstname, u.lastname, u.username, u.email, u.affiliation, u.roles, u.active, u.logged, u.registered
        FROM AppBundle:User AS u
        ORDER BY u.username";

    /** @var Query $query */
    $query = $em->createQuery($dql);

    return $query->execute();
  }

  /**
   * @param array $params
   * @throws ParameterNotValidException
   */
  public function sanitizeAndValidate(array &$params)
  {
    UserParametersSanitizer::sanitize($params);
    UserParametersValidator::validate($params);
  }

  /**
   * @param array $params
   * @throws ParameterValueNotUniqueException
   */
  public function checkUniqueConstrain(array $params)
  {
    //todo use transactions for check and save
    //this can be avoided using the unique columns on the DB
    //see http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/transactions-and-concurrency.html#approach-2-explicitly

    if (array_key_exists('username', $params) && !empty($params['username'])) {
      $anotherUser = $this->findOneBy(array('username' => $params['username'], 'active' => 1));
      if ($anotherUser) {
        throw new ParameterValueNotUniqueException('username', 'Parameter not valid.', 'This username is already in use.');
      }
    }

    if (array_key_exists('email', $params) && !empty($params['email'])) {
      $anotherUser = $this->findOneBy(array('email' => $params['email'], 'active' => 1));
      if ($anotherUser) {
        throw new ParameterValueNotUniqueException('email', 'Parameter not valid.', 'This email address is already in use.');
      }
    }
  }

  /**
   * returns an hash with information for all users
   * @param $user
   */
  public function checkUniqueConstrainAndId($user)
  {
    //todo
  }

  /**
   * Creates a users from the specified parameters.
   * Verified is used to set the activation date.
   * Registered is used to set the creation date.
   * PasswordChanged is used to check the password expiration.
   *
   * @param array $params
   * @param bool $canCreate
   * @return User
   * @throws ParameterNotValidException
   * @throws ParameterValueNotUniqueException
   * @throws UserException
   * @throws MissingRequiredParameterException
   */
  public function create(array $params, $canCreate = false)
  {
    RequiredParametersChecker::check($params, ['username', 'password', 'email']);
    $this->sanitizeAndValidate($params);
    $this->checkUniqueConstrain($params);

    $user = new User();
    $user->setUsername($params['username']);
    $user->setEmail($params['email']);

    // Caution: Using the PASSWORD_BCRYPT as the algorithm, will result in the password parameter being truncated to a maximum length of 72 characters.
    $user->setPassword(password_hash($params['password'], PASSWORD_DEFAULT));

    //todo use empty?
    if (array_key_exists('firstName', $params) && $params['firsName'] != null) {
      $user->setFirstname($params['firstName']);
    } else {
      $user->setFirstname("");
    }
    if (array_key_exists('lastName', $params) && $params['lastName'] != null) {
      $user->setLastname($params['lastName']);
    } else {
      $user->setLastname("");
    }
    if (array_key_exists('affiliation', $params) && $params['affiliation'] != null) {
      $user->setAffiliation($params['affiliation']);
    } else {
      $user->setAffiliation("");
    }
    if (array_key_exists('notes', $params) && $params['notes'] != null) {
      $user->setNotes($params['notes']);
    } else {
      $user->setNotes("");
    }
    $user->setApiKey(self::generateApiKey());
    $user->setActive(false);
    $user->setCanCreate($canCreate);

    $user->setHash(self::getHash());
    $now = new DateTime("now");
    $user->setRegistered($now);
    $user->setModified($now);
    $user->setPasswordChanged($now);

    $this->save($user);

    return $user;
  }

  /**
   * Sets the last password change to January 1st, 1970.
   * This forces the password to expire.
   *
   * @author giuliano
   * @param User $user
   * @throws UserException
   */
  public function resetPasswordChange($user)
  {
    $now = new DateTime();
    $now->setTimestamp(0);
    $user->setPasswordChanged($now);
    $this->save($user);
  }

  /**
   * Updates the user entity specified by hash.
   *
   * @param $params
   * @param null $immutableProfileParams
   * @return User
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws UserException
   */
  public function updateProfile($params, $immutableProfileParams = null)
  {
    RequiredParametersChecker::check($params, array('hash'));
    ImmutableParametersFilter::remove($params, $immutableProfileParams);

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));

    if (!$user) {
      throw new UserException('User not found.', 'Specify a valid identifier.');
    }

    if (!$params) {
      return $user;
    }

    if (!$user->getActive()) {
      throw new UserException('User not active.', 'Specify an active user.');
    }

    $this->sanitizeAndValidate($params);

    if (array_key_exists('firstName', $params) && $params['firstName'] != null) {
      $user->setFirstname($params['firstName']);
    }
    if (array_key_exists('lastName', $params) && $params['lastName'] != null) {
      $user->setLastname($params['lastName']);
    }
    if (array_key_exists('username', $params) && $params['username'] != null) {
      $user->setUsername(trim($params['username']));
    }
    //todo limit
    if (array_key_exists('email', $params) && $params['email'] != null) {
      $user->setEmail(trim($params['email']));
    }
    if (array_key_exists('affiliation', $params) && $params['affiliation'] != null) {
      $user->setAffiliation($params['affiliation']);
    }
//    if (array_key_exists('roles', $params) && $params['roles'] != null) {
//      $user->setRoles($params['roles']);
//    }
    if (array_key_exists('notes', $params) && $params['notes'] != null) {
      $user->setNotes($params['notes']);
    }
    $user->setModified(new DateTime());

    $this->save($user);
    return $user;
  }

  /**
   * Refreshes the access token (api key) of the user specified by hash and return it.
   *
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   */
  public function refreshApiKey($params)
  {
    RequiredParametersChecker::check($params, array('hash'));

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));

    if (!$user) {
      throw new UserException("User not found.", "Specify a valid identifier.");
    }

    if (!$user->getActive()) {
      throw new UserException("User not active.", "Specify an active user.");
    }

    $user->setApiKey(self::generateApiKey());
    $this->save($user);

    return $user;
  }

  /**
   * Returns a new access token (api key).
   *
   * @return string
   */
  private function generateApiKey()
  {
    $apiKey = sprintf("%010s-%s", time(), uniqid(true));
    while (self::existsUser($apiKey)) {
      $apiKey = sprintf("%010s-%s", time(), uniqid(true));
    }
    return $apiKey;
  }


  /**
   * Updates the specified user entity.
   *
   * @param User $user
   * @param $params
   * @return User
   * @throws ParameterNotValidException
   * @throws UserException
   */
  public function update($user, $params)
  {
    if (!$user) {
      throw new UserException('User not found.', 'Specify a user.');
    }

    if (!$params) {
      return $user;
    }

    if (!$user->getActive()) {
      throw new UserException('User not active.', 'Specify an active user.');
    }

    $this->sanitizeAndValidate($params);

    if (array_key_exists('firstname', $params) && $params['firstname'] != null) {
      $user->setFirstname($params['firstname']);
    }
    if (array_key_exists('lastname', $params) && $params['lastname'] != null) {
      $user->setLastname($params['lastname']);
    }
    if (array_key_exists('username', $params) && $params['username'] != null) {
      $user->setUsername(trim($params['username']));
    }
    if (array_key_exists('password', $params) && $params['password'] != null) {
      // Caution: Using the PASSWORD_BCRYPT as the algorithm, will result in the password parameter being truncated to a maximum length of 72 characters.
      $user->setPassword(password_hash((trim($params['password'])), PASSWORD_DEFAULT));
    }
    if (array_key_exists('email', $params) && $params['email'] != null) {
      $user->setEmail(trim($params['email']));
    }
    if (array_key_exists('affiliation', $params) && $params['affiliation'] != null) {
      $user->setAffiliation($params['affiliation'] && $params['firstname'] != null);
    }
    if (array_key_exists('roles', $params) && $params['roles'] != null) {
      $user->setRoles($params['roles']);
    }
    if (array_key_exists('notes', $params) && $params['notes'] != null) {
      $user->setNotes($params['notes']);
    }
    if (array_key_exists('active', $params) && $params['active'] != null) {
      $user->setActive($params['active'] && $params['firstname'] != null);
    }
    if (array_key_exists('hash', $params) && $params['hash'] != null) {
      $user->setHash($params['hash']);
    }
    if (array_key_exists('logged', $params) && $params['logged'] != null) {
      $user->setLogged($params['logged']);
    }
    if (array_key_exists('changedPassword', $params) && $params['changedPassword'] != null) {
      $user->setPasswordChanged($params['changedPassword']);
    }
    $user->setModified(new DateTime());

    if ($user->getApiKey() == "" || array_key_exists('apiKey', $params)) {
      $user->setApiKey(self::generateApiKey());
    }

    $this->save($user);

    return $user;
  }

  /**
   * Saves the specified user.
   *
   * @author giuliano
   * @param User $user
   * @throws UserException
   */
  public function save($user)
  {
    try {
      $em = $this->getEntityManager();
      $em->persist($user);
      $em->flush();
    } catch (Exception $e) {
      throw new UserException('Cannot save the specified user.', 'Specify a valid user.');
    }
  }

  /**
   * Updates user entity.
   *
   * @param $id
   * @param $values
   * @return int
   * @throws UserException
   * @deprecated
   */
  public function updateUser($id, $values)
  {
    /** @var User $user */
    $user = $this->getUserFromID($id);

    if ($user) {
      if (array_key_exists('firstname', $values)) {
        $user->setFirstname($values['firstname']);
      }
      if (array_key_exists('lastname', $values)) {
        $user->setLastname($values['lastname']);
      }
      if (array_key_exists('username', $values)) {
        $user->setUsername(trim($values['username']));
      }
      if (array_key_exists('password', $values)) {
        // Caution: Using the PASSWORD_BCRYPT as the algorithm, will result in the password parameter being truncated to a maximum length of 72 characters.
        $user->setPassword(password_hash((trim($values['password'])), PASSWORD_DEFAULT));
      }
      if (array_key_exists('email', $values)) {
        $user->setEmail(trim($values['email']));
      }
      if (array_key_exists('affiliation', $values)) {
        $user->setAffiliation($values['affiliation']);
      }
      if (array_key_exists('roles', $values)) {
        $user->setRoles($values['roles']);
      }
      if (array_key_exists('notes', $values)) {
        $user->setNotes($values['notes']);
      }
      if (array_key_exists('active', $values)) {
        $user->setActive($values['active']);
      }
      if (array_key_exists('hash', $values)) {
        $user->setHash($values['hash']);
      }
      if (array_key_exists('logged', $values)) {
        $user->setLogged($values['logged']);
      }
      if (array_key_exists('changedPassword', $values)) {
        $user->setPasswordChanged($values['changedPassword']);
      }
      $user->setModified(new DateTime());

      if ($user->getApiKey() == "" || array_key_exists('api_key', $values)) {
        $user->setApiKey(self::generateApiKey());
      }
      //todo replace with save($user)
      //$em = $this->getEntityManager();
      //$em->persist($user);
      //$em->flush();
      $this->save($user);
      return 1;
    }
    return 0;
  }

  /**
   * Finds and authenticate a user by username (or email) and password, if present, returns it.
   * Limited to username only for security.
   *
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   */
  public function authenticate($params)
  {
    RequiredParametersChecker::check($params, ['user', 'password']);

    $query = $this->createQueryBuilder('user')
      //->where('(user.username = :username OR user.email = :username)')
      ->where('(user.username = :username)')
      ->andWhere('user.verified IS NOT NULL')
      ->andWhere('user.active = 1')
      ->setParameters(array('username' => $params['user']))
      ->getQuery();

    $result = $query->getResult();
    if ($result && count($result) > 0) {

      // by construction the username and email are unique
      /** @var User $user */
      $user = $result[0];

      if (password_verify($params['password'], $user->getPassword())) {
        return $user;
      } else {
        throw new UserException('Your password is incorrect.', 'Specify a correct password.');
      }
    }

    throw new UserException('User not found.', 'Specify a user.');
  }

  /**
   * Changes the password. This method does require current password validation.
   * Used by change password and change expired password.
   *
   * @see resetPassword
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws UserException
   */
  public function changePassword($params)
  {
    RequiredParametersChecker::check($params, ['hash', 'current_password', 'password', 'confirm_password']);
    UserParametersSanitizer::sanitize($params);

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));
    if ($user) {
      if (password_verify($params['current_password'], $user->getPassword())) {

        UserParametersValidator::validate($params);

        //$user->setPassword($params['password']);
        $user->setPassword(password_hash($params['password'], PASSWORD_DEFAULT));

        $now = new DateTime("now");
        $user->setModified($now);
        $user->setPasswordChanged($now);

        $this->save($user);
        return $user;
      } else {
        throw new UserException('Current password is incorrect.', 'Specify a correct password.');
      }
    }

    throw new UserException('User not found.', 'Specify a valid user.');
  }

  /**
   * Finds the user, resets the hash, and return it.
   *
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws UserException
   */
  public function resetPasswordRequest($params)
  {
    RequiredParametersChecker::check($params, ['email']);
    UserParametersValidator::validate($params);

    /** @var User $user */
    $user = $this->findOneBy(array('email' => $params['email'], 'active' => 1));
    if ($user) {
      //regenerate the hash
      $user->setHash(self::getHash());

      //used to check if the reset password expiring period
      $user->setModified(new DateTime("now"));

      $this->save($user);
      return $user;
    } else {
      throw new UserException('User not found.', 'Specify a valid user.');
    }
  }

  /**
   * Resets the password. This method doesn't require current password validation.
   * Used by forgot/reset password.
   *
   * @see changePassword
   * @param $params
   * @param $expiringTime
   * @return User
   * @throws MissingRequiredParameterException
   * @throws ParameterNotValidException
   * @throws UserException
   */
  public function resetPassword($params, $expiringTime)
  {
    RequiredParametersChecker::check($params, ['hash', 'password', 'confirm_password']);
    UserParametersValidator::validate($params);

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));
    if ($user) {
      /** @var DateTime $expires */
      $expires = $user->getModified();
      $expires->add(new \DateInterval('PT' . $expiringTime . 'H'));
      //$expires->add(new \DateInterval('P120S'));
      $now = new DateTime("now");
      if ($now > $expires) {
        //reset failed, the link is expired
        throw new UserException('Your password reset link is expired or invalid.', 'Specify a correct link.');
      }

      $user->setPassword(password_hash($params['password'], PASSWORD_DEFAULT));
      $now = new DateTime("now");
      $user->setModified($now);
      $user->setPasswordChanged($now);

      $this->save($user);
      return $user;
    } else {
      throw new UserException('User not found.', 'Specify a valid user.');
    }
  }


  /**
   * Finds an active user by id.
   *
   * @author giuliano
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   */
  public function findActiveUserById($params)
  {
    RequiredParametersChecker::check($params, ['id']);

    /** @var User $user */
    $user = $this->findOneBy(array('id' => $params['id'], 'active' => 1));

    if ($user) {
      return $user;
    }

    throw new UserException('User not found.', 'Specify a valid identifier.');
  }

  /**
   * Finds a user by hash.
   *
   * @author giuliano
   * @param $params
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   */
  public function findUserByHash($params)
  {
    RequiredParametersChecker::check($params, ['hash']);

    /** @var User $user */
    $user = $this->findOneBy(array("hash" => $params['hash']));

    if ($user) {
      return $user;
    }

    throw new UserException('User not found.', 'Specify a valid identifier.');
  }

  /**
   * Finds a user by hash, verifies the expiring time and single use only, and returns it.
   *
   * @param $params
   * @param $expiringTime
   * @return User
   * @throws UserException
   * @throws MissingRequiredParameterException
   */
  public function findUserToResetPassword($params, $expiringTime)
  {
    RequiredParametersChecker::check($params, ['hash']);

    /** @var User $user */
    $user = $this->findOneBy(array("hash" => $params['hash']));

    if ($user) {
      /** @var DateTime $expires */
      $expires = $user->getModified();
      $expires->add(new \DateInterval('PT' . $expiringTime . 'H'));
      //$expires->add(new \DateInterval('P120S'));
      $now = new DateTime("now");
      if ($now > $expires) {
        //reset failed, the link is expired
        throw new UserException('Your password reset link is expired or invalid.', 'Specify a valid link.');
      }

      // re-generate the token/hash
      // the reset link is single use only!

      $user->setHash(self::getHash());
      $user->setActive(true);

      $this->save($user);
      return $user;
    }
    throw new UserException('Your password reset link is expired or invalid.', 'Specify a valid link.');
  }


  /**
   * Checks if the account can be activated after the user has clicked on the activation link
   * and the link is valid and not expired.
   *
   * @param $params
   * @param $expiringTime
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   * @throws Exception
   */
  public function checkActivateAccount($params, $expiringTime)
  {
    RequiredParametersChecker::check($params, array('hash'));

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));

    if ($user) {
      /** @var DateTime $expires */
      $expires = $user->getRegistered();

      $expires->add(new DateInterval('PT' . $expiringTime . 'H'));
      $now = new DateTime("now");
      if ($now > $expires) {
        //sign up failed, the token is expired
        throw new UserException('The activation link is expired.', 'Specify a valid link.');
      }

      return $user;
    }
    throw new UserException('Invalid or expired activation link.', 'Specify a valid link.');
  }

  /**
   * Activates an account if the user has clicked on the activation link
   * and the link is valid and not expired.
   *
   * @param $params
   * @param $expiringTime
   * @return User
   * @throws MissingRequiredParameterException
   * @throws UserException
   * @throws Exception
   */
  public function activateAccount($params, $expiringTime)
  {
    RequiredParametersChecker::check($params, array('hash'));

    /** @var User $user */
    $user = $this->findOneBy(array('hash' => $params['hash']));

    if ($user) {
      /** @var DateTime $expires */
      $expires = $user->getRegistered();

      $expires->add(new DateInterval('PT' . $expiringTime . 'H'));
      $now = new DateTime("now");
      if ($now > $expires) {
        //sign up failed, the token is expired
        throw new UserException('The activation link is expired.', 'Specify a valid link.');
      }

      $user->setVerified($now);
      // re-generate the token/hash
      $user->setHash(self::getHash());
      $user->setActive(true);

      $this->save($user);
      return $user;
    }
    throw new UserException('Invalid or expired activation link.', 'Specify a valid link.');
  }

  /**
   * Updates and returns a token (hash).
   *
   * @param $id
   * @return null|string
   * @throws UserException
   * @deprecated
   */
  public function updateHash($id)
  {
    /** @var User $user */
    $hash = self::getHash();

    // update the hash and modified date
    if ($this->updateUser($id, array('hash' => $hash)) == 1) {
      return $hash;
    }
    return null;
  }

  /**
   * Refreshes the token of the specified user and return it.
   *
   * @param User $user
   * @return User
   * @throws UserException
   */
  public function refreshHash($user)
  {
    $hash = self::getHash();
    $user->setHash($hash);
    $this->save($user);
    return $user;
  }

  /**
   * Deletes the user with the specified id.
   *
   * @param $id
   * @return int
   * @throws \Doctrine\ORM\OptimisticLockException
   */
  public function deleteUser($id)
  {
    $em = $this->getEntityManager();
    $user = $this->getUserFromID($id);
    if ($user != null) {
      //TODO: check dependencies: delete annotation, comment, roles, progress,...
      $em->remove($user);
      $em->flush();
      return 1;
    }
    return 0;
  }

  /**
   * Returns a random token (hash) of length 32.
   * @return string
   */
  private function getHash()
  {
    return StringGenerator::getAlphaNumeric(32);
  }

  /**
   * Returns true if a user with the specified api-key exists.
   *
   * @param $apiKey
   * @return bool
   */
  private function existsUser($apiKey)
  {
    /** @var User $user */
    $user = $this->findOneBy(array("apiKey" => $apiKey));
    if ($user) {
      return true;
    }
    return false;
  }

}