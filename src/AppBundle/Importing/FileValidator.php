<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 16/02/16
 */
namespace AppBundle\Importing;

use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileValidator
{
  /**
   * @var Project same checking depends from the project setting.
   */
  public $project;

  /**
   * @var array the error message used when a file is not uploaded correctly.
   */
  public $message;

  /**
   * @var boolean true if there was some error.
   */
  public $isWrong;

  /**
   * @var string format file.
   */
  public $file_format;

  /**
   * @var string char separator (for TSV and CSV file format).
   */
  public $char_separator;

  /**
   * @var array header fields (for TSV and CSV file format).
   */
  public $header_fields;

  /**
   * @var integer the maximum number of bytes required for the uploaded file.
   * Defaults to null, meaning no limit.
   * Note, the size limit is also affected by `upload_max_filesize` and `post_max_size` INI setting
   * and the 'MAX_FILE_SIZE' hidden field value. See [[getSizeLimit()]] for details.
   * @see http://php.net/manual/en/ini.core.php#ini.upload-max-filesize
   * @see http://php.net/post-max-size
   * @see getSizeLimit
   */
  public $sizeLimit;

  /**
   * @var string original file name.
   */
  public $origFilename;

  /**
   * @var UploadedFile file path on the server.
   */
  public $uploadFile;

  /**
   * @var array the number of valid IDs.
   */
  public $listIDs;

  /**
   * FileValidator constructor.
   *
   * @param $file
   * @param $project
   * @param bool $validateData
   */
  public function __construct($file, $project, $validateData = false)
  {
    $this->uploadFile = $file;
    $this->project = $project;
    $this->message = array();
    $this->isWrong = false;
    $this->header_fields = array();

    $this->validate($validateData);
  }

  private function validate($validateData) {
    if ($this->project == null) {
      array_push($this->message, 'ERROR: Project not found.');
      $this->isWrong = true;
      return;
    }
    if (!$this->uploadFile instanceof UploadedFile || $this->uploadFile->getError() == UPLOAD_ERR_NO_FILE) {
      array_push($this->message, 'ERROR: Please upload a valid file.');
      $this->isWrong = true;
      return;
    }
    if (!file_exists($this->uploadFile)) {
      array_push($this->message, 'ERROR: File upload failed.');
      $this->isWrong = true;
      return;
    }

    $this->origFilename = $this->uploadFile->getClientOriginalName();
    if ($this->uploadFile->getSize() == 0) {
      array_push($this->message, 'ERROR: The file is empty.');
      $this->isWrong = true;
      return;
    }

    $this->sizeLimit = $this->getSizeLimit();
    if ($this->uploadFile->getSize() > $this->sizeLimit) {
      array_push($this->message, 'ERROR: The file is too big. Its size cannot exceed '.$this->sizeLimit .'!');
      $this->isWrong = true;
      return;
    }

    $this->guessFileFormat();
    if ($this->file_format == null) {
      array_push($this->message, 'ERROR: Only CSV and TSV files are allowed. Allowed separator characters are tab, comma or semicolon.');
      $this->isWrong = true;
      return;
    } else {
      if ($this->file_format == "csv" || $this->file_format == "tsv") {
        if (count($this->header_fields) <= 1) {
          array_push($this->message, 'ERROR: The header is missing or column names are not valid.');
          $this->isWrong = true;
        }
        if (!array_key_exists("id", $this->header_fields)) {
          array_push($this->message, 'ERROR: Column "id" is mandatory.');
          $this->isWrong = true;
        }
        if (!array_key_exists("source", $this->header_fields)) {
          array_push($this->message, 'ERROR: Column "source" is mandatory.');
          $this->isWrong = true;
        }
        if (!array_key_exists("target", $this->header_fields) || count($this->header_fields["target"]) == 0) {
          array_push($this->message, 'ERROR: At least one "target" column is mandatory.');
          $this->isWrong = true;
        } else {
          //check exact number of output (for binary and ranking task ONLY)

          //Claudio: removed this control as the ranking calculate its own
          /*$countRanges = count(json_decode($this->project->getRanges()));
          if ($this->project->getType() == "ranking" &&
            count($this->header_fields["target"]) != $countRanges) {
            array_push($this->message, 'ERROR: The number of "target" outputs must be '.$countRanges.' instead of '. count($this->header_fields["target"]) .'.');
            $this->isWrong = true;
          }*/
        }

        if ($this->project->getRequiref() &&
          (!array_key_exists("reference", $this->header_fields) &&
            !array_key_exists("reference-url", $this->header_fields))
        ) {
          array_push($this->message, 'ERROR: Column "reference" or "reference-url" is required in the evaluation format.');
          $this->isWrong = true;
        }

        if ($validateData)
          $this->checkCSVData();
      }
    }
  }

  /*
   * check if there are empty values or some ID already store in the database
   */
  private function checkCSVData()
  {
    $ids = array();
    $urls = array();
    $header_column_count = 0; //count($this->header_fields, COUNT_RECURSIVE) - count($this->header_fields["target"]) - 1;
    $handle = fopen($this->uploadFile, "r");
    $linecount = 0;
    $header_found=false;
    if ($handle) {
      while (($line = fgets($handle)) !== false) {
        $linecount++;
        if (strlen(trim($line)) > 0 && !$header_found) {
          $header_column_count = count(str_getcsv($line, $this->char_separator));
          //skip header
          $header_found=true;
          continue;
        }
        if ($header_found) {
          $fields = str_getcsv($line, $this->char_separator);
          //header column count != line column count
          if ($header_column_count != count($fields)) {
            array_push($this->message, "WARNING: (line $linecount) The header has $header_column_count columns while the line has " . count($fields) .".");
            continue;
          }

          //check ID cell
          if (array_key_exists("id", $this->header_fields)) {
            $id = trim($fields[$this->header_fields["id"]]);
            if (empty($id)) {
              array_push($this->message, "WARNING: (line $linecount) The cell \"id\" is empty. An ID has been created.");
            } else {
              if (in_array($id, $ids)) {
                array_push($this->message, "ERROR: (line $linecount) ID $id is not unique in this file.");
                $this->isWrong = true;
              } else {
                array_push($ids, $id);
              }
            }
          }

          if (array_key_exists("source", $this->header_fields)) {
            $source = trim($fields[$this->header_fields["source"]]);
            if (empty($source)) {
              array_push($this->message, "ERROR: (line $linecount) The source is missing.");
              $this->isWrong = true;
            }
          }

          // check references
          if (array_key_exists("reference", $this->header_fields) ||
            array_key_exists("reference-url", $this->header_fields)) {

            //add a reference as a link
            if (array_key_exists("reference-url", $this->header_fields)) {
              $referenceUrl = trim($fields[$this->header_fields["reference-url"]]);

              //check online images
              if (!empty($referenceUrl)) {
                if (!in_array($referenceUrl, $urls)) {
                  array_push($urls, $referenceUrl);
                  $header = get_headers($referenceUrl);
                  if (stripos($header[0], "200 OK")) {
                    //store into DB
                  } else {
                    array_push($this->message, "WARNING: (line $linecount) The reference-url $referenceUrl seems not valid.");
                  }
                }
              } else {
                array_push($this->message, "WARNING: (line $linecount) The reference is missing.");
              }
            }
            //add a text reference
            if (array_key_exists("reference", $this->header_fields)) {
              $reference = trim($fields[$this->header_fields["reference"]]);
              if (empty($reference)) {
                array_push($this->message, "WARNING: (line $linecount) The reference-url is missing.");
              }
            }
          }

          // check targets
          if (array_key_exists("target", $this->header_fields)) {
            foreach ($this->header_fields["target"] as $target_name => $target_fields) {
              if (empty(trim($fields[$target_fields["target"]]))) {
                array_push($this->message, "WARNING: (line $linecount) Target \"$target_name\" is missing.");
              }
            }
          }
        }
      }
    }
    fclose($handle);

    $this->listIDs = $ids;
  }

  public function guessFileFormat() {
    $handle = fopen($this->uploadFile, "r");
    if ($handle) {
      while (($line = fgets($handle)) !== false) {
          $line = trim($line);
          if (!empty($line)) {
            //split the first not empty line
            // by tab
            $header = str_getcsv($line, "\t");
            $this->header_fields = $this->get_header_fields($header);
            if (count($this->header_fields) > 1) {
              $this->file_format = "tsv";
              $this->char_separator = "\t";
              break;
            }

            //by semicolon
            $this->message = array();
            $header = str_getcsv($line, ";");
            $this->header_fields = $this->get_header_fields($header);
            if (count($this->header_fields) > 1) {
              $this->file_format = "csv";
              $this->char_separator = ";";
              break;
            }

            //by comma
            $this->message = array();
            $header = str_getcsv($line, ",");
            $this->header_fields = $this->get_header_fields($header);
            if (count($this->header_fields) > 1) {
              $this->file_format = "csv";
              $this->char_separator = ",";
              break;
            }

            //if (preg_match('/^<\\?xml .*/', $line)) {
            //  $file_format = "xml";
            //TODO check XML format (mandatory elements mainly)
            //}
            break;
        }
      }
      fclose($handle);
    }
  }


  //returns array of the valid file columns
  private function get_header_fields($arr_csv_columns)
  {
    $hash_header = array();
    $hash_header["target"] = array();
    $HEADER = array("id", "source-language", "source", "target-language", "comment", "reference", "reference-url");
    $position = 0;
    foreach ($arr_csv_columns as $col) {
      if (in_array($col, $HEADER)) {
        $hash_header[$col] = $position;
      } else {
        $targetname = trim($col);
        $fieldname = "";
        if (preg_match('/^target-.+/',$targetname)) {
          $fieldname = "target";
          $targetname = preg_replace("/^target-/", "", $targetname);
        } else if (preg_match('/^comment-.+/',$targetname)) {
          $fieldname = "comment";
          $targetname = preg_replace("/^comment-/", "", $targetname);
        } else {
          array_push($this->message,"WARNING: Column \"$targetname\" is not expected.");
        }

        if (!empty($fieldname) && !empty($targetname)) {
          if (!array_key_exists($targetname, $hash_header["target"])) {
            $hash_header["target"][$targetname] = array();
          }
          $hash_header["target"][$targetname][$fieldname] = $position;
        }
      }
      $position++;
    }

    //elimino eventuali target senza targetname
    $delTargetList = array();
    foreach ($hash_header["target"] as $target_name => $target_fields) {
      if (!array_key_exists("target", $target_fields)) {
        if (array_key_exists("comment", $target_fields)) {
          array_push($this->message,"WARNING: Column \"comment-".$target_name."\" doesn't correspond to any target.");
        }
        array_push($delTargetList, $target_name);
      }
    }
    foreach ($delTargetList as $target_name) {
      unset($hash_header["target"][$target_name]);
    }

    return $hash_header;
  }

  /**
   * Returns the maximum size allowed for uploaded files.
   * This is determined based on four factors:
   *
   * - 'upload_max_filesize' in php.ini
   * - 'post_max_size' in php.ini
   * - 'MAX_FILE_SIZE' hidden field
   *
   * @return integer the size limit for uploaded files.
   */
  public function getSizeLimit()
  {
    // Get the lowest between post_max_size and upload_max_filesize, log a warning if the first is < than the latter
    $limit = $this->sizeToBytes(ini_get('upload_max_filesize'));
    $postLimit = $this->sizeToBytes(ini_get('post_max_size'));
    if ($postLimit > 0 && $postLimit < $limit) {
      //$this->message[] = ['PHP.ini\'s \'post_max_size\' is less than \'upload_max_filesize\'.'];
      array_push($this->message, 'WARNING: PHP.ini\'s \'post_max_size\' is less than \'upload_max_filesize\'.');
      $limit = $postLimit;
    }

    if (isset($_POST['MAX_FILE_SIZE']) && $_POST['MAX_FILE_SIZE'] > 0 && $_POST['MAX_FILE_SIZE'] < $limit) {
      $limit = (int) $_POST['MAX_FILE_SIZE'];
    }
    return $limit;
  }

  /**
   * Converts php.ini style size to bytes
   *
   * @param string $sizeStr $sizeStr
   * @return int
   */
  private function sizeToBytes($sizeStr)
  {
    switch (substr($sizeStr, -1)) {
      case 'M':
      case 'm':
        return (int) $sizeStr * 1048576;
      case 'K':
      case 'k':
        return (int) $sizeStr * 1024;
      case 'G':
      case 'g':
        return (int) $sizeStr * 1073741824;
      default:
        return (int) $sizeStr;
    }
  }
}
?>