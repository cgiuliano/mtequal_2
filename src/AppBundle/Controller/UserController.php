<?php
/**
 * User: cgirardi
 * Date: 04/10/15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\GenericException;

use AppBundle\Util\MailDeliveryException;

use DateInterval;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
  // this is a default if app.registration.expiring_hours is not specified in config.yml
  const DEFAULT_APP_REGISTRATION_EXPIRING_HOURS = 1;

  // this is a default if app.reset.expiring_hours is not specified in config.yml
  const DEFAULT_APP_RESET_EXPIRING_HOURS = 1;

  // this is a default if app.reset.expiring_hours is not specified in config.yml
  const DEFAULT_APP_PASSWORD_EXPIRING_DAYS = 90;

  /**
   * Renders the sign up page.
   *
   * @Route("/user/sign-up", name="app_user_sign_up")
   * @param Request $request
   * @return Response
   */
  public function signUpAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      return $this->redirectToRoute('app_dashboard_page');
    } catch (Exception $e) {
      return new Response($this->renderView('default/sign-up.twig'));
    }
  }

  /**
   * Renders the sign in page.
   *
   * @Route("/user/sign-in", name="app_user_sign_in")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function signInAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      return $this->redirectToRoute('app_dashboard_page');
    } catch (Exception $e) {
      return new Response($this->renderView('default/sign-in.twig'));
    }
  }

  /**
   * Renders the use profile page.
   *
   * @Route("/user/profile", name="app_user_profile_page")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function userProfile(Request $request)
  {
    try {
      $this->get('logger')->info($request);
//      if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
//        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
//      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      return new Response($this->renderView('default/user-profile.twig', $params));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Renders the use profile page.
   *
   * @Route("/user/personal", name="app_user_profile_personal_data_component")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function personalDataComponent(Request $request)
  {
    try {
      $this->get('logger')->info($request);
//      if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
//        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
//      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      return new Response($this->renderView('util/user-profile-personal-data.twig', $params));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Renders the use profile page.
   *
   * @Route("/user/password", name="app_user_profile_change_password_component")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function changePasswordComponent(Request $request)
  {
    try {
      $this->get('logger')->info($request);
//      if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
//        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
//      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      return new Response($this->renderView('util/user-profile-change-password.twig', $params));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Checks if the account can be activated after the user has clicked on the activation link
   * and if it is valid and not expired.
   * Renders the sign in pages.
   * Called from an email or address bar.
   *
   * @Route("/user/activate/check", name="app_user_activate_check_account")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function checkActivateAccount(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $expiringHours = self::DEFAULT_APP_REGISTRATION_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.registration.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.registration.expiring_hours');
      }

      $params = $request->query->all();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      /** @var User $user */
      $user = $userRepository->checkActivateAccount($params, $expiringHours);

      // Uncomment to allow to accept all roles after the account has been activated
      /** @var RoleRepository $roleRepository */
      //$roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      //$roleRepository->acceptAll($user->getId());

      $this->addFlash('success', 'The activation link is valid.');
      return new Response($this->renderView('default/sign-in.twig'));

    } catch (GenericException $e) {
      $this->addFlash('danger',
        'Account activation check failed: ' . lcfirst($e->getMessage()));
      return new Response($this->renderView('default/sign-in.twig'));
    } catch (Exception $e) {
      $this->addFlash('danger',
        'Account activation check failed.');
      return new Response($this->renderView('default/sign-in.twig'));
    }
  }

  /**
   * Activates the account if the user has clicked on the activation link
   * and if it is valid and not expired.
   * Renders the sign in pages.
   * Called from an email or address bar.
   *
   * This method is similar activateAccount in InviteController.
   *
   * @Route("/user/activate", name="app_user_activate_account")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function activateAccount(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $expiringHours = self::DEFAULT_APP_REGISTRATION_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.registration.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.registration.expiring_hours');
      }

      $params = $request->query->all();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      /** @var User $user */
      $user = $userRepository->activateAccount($params, $expiringHours);

      // Uncomment to allow to accept all roles after the account has been activated
      /** @var RoleRepository $roleRepository */
      //$roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      //$roleRepository->acceptAll($user->getId());

      $this->addFlash('success', 'Thanks for registering! You can login.');
      return new Response($this->renderView('default/sign-in.twig'));

    } catch (GenericException $e) {
      $this->addFlash('danger',
        'Account activation failed: ' . lcfirst($e->getMessage()));
      return new Response($this->renderView('default/sign-in.twig'));
    } catch (Exception $e) {
      $this->addFlash('danger',
        'Account activation failed.');
      return new Response($this->renderView('default/sign-in.twig'));
    }
  }

  /**
   * Authenticates the user by username and password (invoked by Sign in).
   * If the password is expired force the user to changed it.
   *
   * @author giuliano
   * @Route("/user/authenticate", name="app_user_authenticate")
   * @Method("POST")
   * @param Request $request
   * @return RedirectResponse|Response
   */
  public function authenticate(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->request->all();
      $a = $request->request->get("CsrfToken");

      $b = $this->isCsrfTokenValid('authenticate', $request->request->get("CsrfToken"));
      if (!$this->isCsrfTokenValid('authenticate', $request->request->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
      $user = $userRepository->authenticate($params);

      if ($this->isPasswordExpired($user)) {
        // the password is expired, it must be changed
        // generate a new token (hash)
        $user = $userRepository->refreshHash($user);
        return new Response($this->renderView('default/change-expired-password.twig', array('hash' => $user->getHash())));
      }

      //update the session
      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->migrate();
      $authenticatedUser->set($user);

      $this->addFlash(
        'success',
        'Welcome ' . $user->getUsername() . '!'
      );
      return $this->redirectToRoute('app_dashboard_page');

    } catch (GenericException $e) {
      $this->addFlash(
        'danger',
        'Cannot sign in: ' . lcfirst($e->getMessage())
      );
      return $this->redirectToRoute('app_user_sign_in');
    } catch (Exception $e) {
      $this->addFlash(
        'danger',
        'Cannot sign in.'
      );
      return $this->redirectToRoute('app_user_sign_in');
    }
  }

  /**
   * Logout.
   *
   * @author giuliano
   * @Route("/user/logout", name="app_user_logout")
   * @param Request $request
   * @Method("GET")
   * @return RedirectResponse|Response
   */
  public function logoutAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();
      $authenticatedUser->clear();

      return $this->redirectToRoute('homepage');
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Refreshes the access token (api key) of the user specified by hash.
   *
   * @author giuliano
   * @Route("/user/refreshApiKey", name="app_user_refresh_api_key", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function refreshApiKey(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      /** @var UserRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');
      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      $user = $repository->refreshApiKey($params);

      $authenticatedUser->set($user);

      return new Response(json_encode([
        'code' => -1,
        'apiKey' => $user->getApiKey(),
        'message' => 'The access token has been refreshed.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot refresh the access token: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'apiKey' => '',
        'exception' => 'Exception',
        'message' => 'Cannot refresh the access token.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Creates a new user entity. Invoked by sign up.
   *
   * @author giuliano
   * @Route("/user/create", name="app_user_create", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function create(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      //no session here, the user must activate the account and the authenticate

      $emailDomainValidator = $this->get('app.email_domain_validator');
      $canCreate = $emailDomainValidator->validate($params);

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
      $user = $userRepository->create($params, $canCreate);

      $this->sendActivationMail($user, $request->getSchemeAndHttpHost());

      return new Response(json_encode([
        'code' => -1,
        'message' => 'A confirmation link has been sent to your email address. Click on the link in the email to activate your account.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot create the account: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot create the account.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Changes the user's password. The user is identified by hash.
   * This is used to change the password when the password is expired or the user will.
   *
   * @author giuliano
   * @Route("/user/changePassword", name="app_user_change_password", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function changePassword(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      //this method uses the hash to validate the user
      //$authenticatedUser->validate();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      /** @var User $user */
      $user = $userRepository->changePassword($params);

      $authenticatedUser->set($user);
      $redirect = $this->generateUrl('app_dashboard_page');
      return new Response(json_encode([
        'code' => -1,
        'message' => 'The password has been changed.',
        'host' => $request->getSchemeAndHttpHost(),
        'redirect' => $redirect,
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot change the password: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot change the password.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Resets the user's password. The user is identified by hash.
   * Used by the reset password form.
   *
   * @author giuliano
   * @Route("/user/resetPassword", name="app_user_reset_password", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function resetPassword(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      // no authentication required

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      $expiringHours = self::DEFAULT_APP_RESET_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.reset.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.reset.expiring_hours');
      }

      /** @var User $user */
      $user = $userRepository->resetPassword($params, $expiringHours);

      //create the session so the user can be redirect to the dashboard
      //update the session
      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->set($user);

      $redirect = $this->generateUrl('app_dashboard_page');

      return new Response(json_encode([
        'code' => -1,
        'message' => 'The password has been reset.',
        'redirect' => $redirect,
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot reset the password: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot reset the password.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Returns the user profile (personal data) of the user specified by hash.
   *
   * @author giuliano
   * @Method("GET")
   * @Route("/user/getProfile", name="app_user_get_profile", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function getProfile(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      $params = $request->query->all();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      $user = $userRepository->findUserByHash($params);

      return new Response(json_encode([
        'code' => -1,
        'username' => $user->getUsername(),
        'firstName' => $user->getFirstname(),
        'lastName' => $user->getLastname(),
        'email' => $user->getEmail(),
        'affiliation' => $user->getAffiliation(),
        'accessToken' => $user->getApiKey(),
        'message' => 'User found.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'User profile not found: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'User profile not found.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Updates the user profile (personal data) of the user specified by hash.
   *
   * @author giuliano
   * @Route("/user/updateProfile", name="app_user_profile_update", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function updateProfile(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      $user = $userRepository->updateProfile($params, $this->container->getParameter('app.user.immutable_profile_params'));

      $authenticatedUser->set($user);

      return new Response(json_encode([
        'code' => -1,
        'username' => $user->getUsername(),
        'firstName' => $user->getFirstname(),
        'lastName' => $user->getLastname(),
        'email' => $user->getEmail(),
        'affiliation' => $user->getAffiliation(),
        'message' => 'The profile has been updated.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot update the profile: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot update the profile.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Returns the reset password page. This is invoked from the link (single use only) shared by email.
   *
   * @author giuliano
   * @Route("/password/reset", name="app_user_reset_password_page")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function resetPasswordPage(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      // there is no csrf token here
      $params = $request->query->all();

      // the user can change the password
      $expiringHours = self::DEFAULT_APP_RESET_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.reset.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.reset.expiring_hours');
      }

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
      $user = $userRepository->findUserToResetPassword($params, $expiringHours);

      return new Response($this->renderView('default/reset-password.twig', array('hash' => $user->getHash())));

    } catch (GenericException $e) {
      $this->addFlash(
        'danger',
        'Cannot reset the password: ' . lcfirst($e->getMessage())
      );

      return $this->redirectToRoute('app_user_sign_in');
    } catch (Exception $e) {
      $this->addFlash(
        'danger',
        'Cannot reset the password.'
      );
      return $this->redirectToRoute('app_user_sign_in');
    }
  }

  /**
   * Sends an email that contains a link to reset the password.
   *
   * @Route("/user/resetPasswordRequest", name="app_user_reset_password_request")
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public
  function resetPasswordRequest(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->request->all();
      if (!$this->isCsrfTokenValid('authenticate', $request->request->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      $user = $userRepository->resetPasswordRequest($params);

      /* send the email to recover the password */
      //$this->sendForgotPasswordMail($user->getUsername(), $user->getEmail(), $user->getHash(), $this->container->getParameter('hostname'));
      $host = $request->getSchemeAndHttpHost();
      $this->sendForgotPasswordMail($user, $host);

      $this->addFlash(
        'success',
        'The instructions to reset the password has been sent to your email address.'
      );
      return $this->redirectToRoute('homepage');

    } catch (GenericException $e) {
      $this->addFlash(
        'danger',
        'Cannot reset the password: ' . lcfirst($e->getMessage())
      );

      return $this->redirectToRoute('app_user_sign_in');
    } catch (Exception $e) {
      $this->addFlash(
        'danger',
        'Cannot reset the password.'
      );
      return $this->redirectToRoute('app_user_sign_in');
    }
  }

  /**
   * Sanitizes and validates the parameters used to create or update a user.
   * Used in sign up, change password, reset password, update user.
   *
   * @author giuliano
   * @Route("/user/validate", name="app_user_validate", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function sanitizeAndValidateUserParameters(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $emailDomainValidator = $this->get('app.email_domain_validator');
      $emailDomainValidator->validate($params);

      /** @var UserRepository $repository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
      $userRepository->sanitizeAndValidate($params);
      $userRepository->checkUniqueConstrain($params);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'User valid.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  // Services

  /**
   * Returns true if the specified user has to change the password; false otherwise.
   *
   * @author giuliano
   * @param User $user
   * @return bool
   */
  private function isPasswordExpired($user)
  {
    $changedPassword = $user->getPasswordChanged();

    $expiringDays = self::DEFAULT_APP_PASSWORD_EXPIRING_DAYS;
    if ($this->container->hasParameter('app.password.expiring_days')) {
      $expiringDays = $this->container->getParameter('app.password.expiring_days');
    }
    /** @var DateTime $changedPassword */
    $changedPassword->add(new DateInterval('P' . $expiringDays . 'D'));
    //$changedPassword->add(new \DateInterval('PT120S'));

    $now = new DateTime("now");
    if ($now > $changedPassword) {
      return true;
    }
    return false;
  }

  // emails

  /**
   * Sends the confirm registration (activate account) link by email.
   *
   * @author giuliano
   * @param User $user
   * @param $host
   * @throws Exception
   */
  private
  function sendActivationMail($user, $host)
  {
    $this->get('logger')->info('sending activation email to ' . $user->getEmail());

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $expiringHours = self::DEFAULT_APP_REGISTRATION_EXPIRING_HOURS;
    if ($this->container->hasParameter('app.registration.expiring_hours')) {
      $expiringHours = $this->container->getParameter('app.registration.expiring_hours');
    }

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $to = $user->getEmail();
    $body = $this->renderView('mail/activate-created-account.twig',
      array('user' => $user, 'host' => $host, 'expiringHours' => $expiringHours)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Account Activation')
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      $this->get('logger')->error('cannot deliver the account activation email to ' . $user->getEmail());
      throw new MailDeliveryException($from, $to, $failures, 'Cannot deliver the account activation email.');
    }
  }

  /**
   * Sends the reset password link by email
   *
   * @author giuliano
   * @param User $user
   * @param $host
   * @throws MailDeliveryException
   */
  //private function sendForgotPasswordMail($username, $email, $hash, $hostname)
  private function sendForgotPasswordMail($user, $host)
  {
    $this->get('logger')->info('sending forgot password email to ' . $user->getEmail());

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    //set the expiring period
    $expiringHours = self::DEFAULT_APP_RESET_EXPIRING_HOURS;
    if ($this->container->hasParameter('app.reset.expiring_hours')) {
      $expiringHours = $this->container->getParameter('app.reset.expiring_hours');
    }

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $to = $user->getEmail();
    $body = $this->renderView('mail/reset-password.email.twig',
      array('user' => $user, 'expiringHours' => $expiringHours, 'host' => $host)
    );
    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Password reset')
      ->setFrom($from)
      ->setTo($to)
      ->setBody(
        $body, 'text/html'
      );


    if (!$mailer->send($mail, $failures)) {
      $this->get('logger')->error('cannot deliver the forgot password email to ' . $user->getEmail());
      throw new MailDeliveryException($from, $to, $failures, 'Cannot deliver the forgot password email.');
    }
  }
}

?>