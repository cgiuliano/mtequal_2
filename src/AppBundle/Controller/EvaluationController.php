<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 07/12/17
 * Time: 08:49
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Project;
use AppBundle\Entity\Role;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationException;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\ProgressException;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\ReportRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\UserException;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;

use AppBundle\Util\GenericException;
use AppBundle\Util\GenericValidator;

use AppBundle\Util\MailDeliveryException;

use AppBundle\Util\RequiredParametersChecker;

use DateTime;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EvaluationController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class EvaluationController extends Controller
{
  /**
   * Renders the evaluation report and generates a link to a sharable report page (public).
   *
   * @Route("/evaluation/report", name="app_evaluation_report")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function report(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();


      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'reviewer', 'owner', 'vendor']);

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $reportParams = $annotationRepository->getEvalReportParams($params['pid'], $user->getId());

       /** @var ReportRepository $reportRepository */
      $reportRepository = $this->getDoctrine()->getRepository('AppBundle:Report');
      $report =$reportRepository->create($project, $user);

      $reportParams["token"] = $report->getToken();
      $reportParams["sharable"] = true;

      //$reportParams["evaluationFormat"] = $project->getType();
      $reportParams['pid'] = $params['pid'];
      //$reportParams['blindReview'] = $project->getBlindrev();

//      /** @var RoleRepository $roleRepository */
//      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
//      $userRoles = $roleRepository->getUserRoles($user->getId(), $params["pid"]);
//      $reportParams['userRoles'] = $userRoles;

      return new Response($this->renderView('listing/evalreport.twig', $reportParams));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Delete all evaluations. Only admin can delete the evaluations.
   *
   * @Route("evaluation/delete", name="app_project_delete_all_evaluations", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @author giuliano
   * @return Response
   */
  public function deleteAll(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAdmin($user, $project);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progressCheckCorpus = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "check_corpus"));


      if ($progressCheckCorpus->getCompleted()) {
        throw new ProjectException('Cannot delete the evaluations', 'The evaluation task must be stopped before.');
      }

      $annotationRepository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
      $annotationRepository->deleteAllEvaluations($project->getId());

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Evaluations deleted.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot delete the evaluations: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot delete the evaluations.',
        'valid' => false], JSON_PRETTY_PRINT));

    }
  }

  /**
   * Renders the evaluation progress.
   *
   * @author giuliano
   * @Route("/evaluation/progress", name="app_evaluation_progress")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function progress(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'owner', 'vendor']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

      $data = $progressRepository->taskReport($params['pid'], $user->getId());
      $data["isEvaluationStarted"] = $progressRepository->isCompletedTask(ProgressRepository::CHECKCORPUS_TASK, $params['pid']);

      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $data["evaluators"] = $roleRepository->getUsers($params['pid'], $user->getId());

      //get user roles
      $data["roles"] = $roleRepository->getUserRoles($user->getId(), $params["pid"]);

      $data['pid'] = $params['pid'];

      return $this->render('project/evaluation-progress.twig', $data);
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Starts the evaluation and implicitly mark as complete the task corpus_check.
   *
   * @Route("/evaluation/start", name="app_start_evaluation", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function startEvaluation(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progress = $progressRepository->findProgressByProjectIdAndTask($params);

      $host = $request->getSchemeAndHttpHost();
      $progress = $this->startAndGetEvaluation($project, $user, $progress, $host);

      $progress->setUser($user->getId());
      $progress->setModified(new DateTime('now'));
      $progressRepository->save($progress);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $progress,
        'message' => 'The evaluation has been launched.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (TaskException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot launch the evaluation: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot launch the evaluation.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * ...
   *
   * @Route("/evaluation/stop", name="app_stop_evaluation", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function stopEvaluation(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progress = $progressRepository->findProgressByProjectIdAndTask($params);

      $host = $request->getSchemeAndHttpHost();
      $progress = $this->stopAndGetEvaluation($project, $user, $progress, $host);

      $progress->setUser($user->getId());
      $progress->setModified(new DateTime('now'));
      $progressRepository->save($progress);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $progress,
        'message' => 'The evaluation has been stopped.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (TaskException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot stop the evaluation: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot stop the evaluation.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * ...
   *
   * @Route("/evaluator/start", name="app_start_evaluator", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function startEvaluator(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();
      RequiredParametersChecker::check($params, ['id']);
      GenericValidator::validateIntIdentifier($params['id']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'vendor', 'owner']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      //$progress = $progressRepository->findProgressByProjectIdAndTask($params);

      $checkCorpus = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "check_corpus"));

      if ($checkCorpus && !$checkCorpus->getCompleted()) {
        throw new TaskException('Cannot start the evaluator.', 'The evaluation has to be launched before.');
      }

      /** @var Progress $evaluator */
      $evaluator = $progressRepository->findOneBy(array('user' => $params['id'], 'project' => $project->getId(), 'task' => 'evaluation'));

      if ($evaluator->getCompleted()) {
        throw new TaskException('Cannot start the evaluator.', 'The evaluator has already completed the evaluation.');
      }

      if (!$evaluator->getDisabled()) {
        throw new TaskException('Cannot start the evaluator.', 'The evaluator has been already started.');
      }

      $evaluator->setDisabled(FALSE);
      $evaluator->setModified(new DateTime('now'));
      $progressRepository->save($evaluator);

      $host = $request->getSchemeAndHttpHost();
      $this->sendStartEvaluationEmail($project, $evaluator->getUser(), $host);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $evaluator,
        'message' => 'The evaluation has been launched.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (TaskException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot start the evaluator: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot start the evaluator.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * ...
   *
   * @Route("/evaluator/stop", name="app_stop_evaluator", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function stopEvaluator(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();
      RequiredParametersChecker::check($params, ['id']);
      GenericValidator::validateIntIdentifier($params['id']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'vendor', 'owner']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      //$progress = $progressRepository->findProgressByProjectIdAndTask($params);

      /** @var Progress $evaluator */
      $evaluator = $progressRepository->findOneBy(array('user' => $params['id'], 'project' => $project->getId(), 'task' => 'evaluation'));

      if ($evaluator->getCompleted()) {
        throw new TaskException('Cannot stop the evaluator.', 'The evaluator has already completed the evaluation.');
      }

      if ($evaluator->getDisabled()) {
        throw new TaskException('Cannot stop the evaluator.', 'The evaluator is already stopped.');
      }

      //check if the review is complete
      $review = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "review"));

      if ($review && $review->getCompleted()) {
        throw new TaskException('Cannot stop the evaluator.', 'The review has to be marked as incomplete before.');
      }

      $finalize = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "finalize"));

      if ($finalize->getCompleted()) {
        throw new TaskException('Cannot stop the evaluator.', 'The project has to be marked as incomplete before.');
      }


      $evaluator->setDisabled(TRUE);
      $evaluator->setModified(new DateTime('now'));
      $progressRepository->save($evaluator);

      $host = $request->getSchemeAndHttpHost();
      $this->sendStopEvaluationEmail($project, $evaluator->getUser(), $host);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $evaluator,
        'message' => 'The evaluator has been stopped.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (TaskException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot stop the evaluator: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot stop the evaluator.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @param $host
   * @return Progress
   * @throws MailDeliveryException
   * @throws ProgressException
   * @throws TaskException
   * @throws UserException
   */
  private function startAndGetEvaluation($project, $user, $progress, $host)
  {
    //check redundant TUs
    if ($project->getIntragree() > 0 && $project->getIntratus() == 0) {
      //todo change the tip: Reset the corpus
      throw new TaskException('Cannot launch the evaluation: the translation units for calculating the intra-annotator agreement have not been generated', 'Click on the "Recreate dataset" link. If the problem persist, contact the support to find what went wrong.');
    }

    /** @var SentenceRepository $sentenceRepository */
    $sentenceRepository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

    $redundantTUs = $sentenceRepository->countRedundantTUs($project->getId());
    if ($project->getIntragree() > 0 && $project->getIntratus() != $redundantTUs) {
      //todo change the tip: Reset the corpus
      throw new TaskException('Cannot launch the evaluation: the translation units for calculating the intra-annotator agreement have not been correctly generated (expected ' . $project->getIntratus() . ' and generated ' . $redundantTUs . ')', 'Click on the "Recreate dataset" link. If the problem persist, contact the support to find what went wrong.');
    }

    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

    $evaluationFormat = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "evalformat"));

    if (!$evaluationFormat->getCompleted()) {
      throw new TaskException('Cannot launch the evaluation.', 'The evaluation format has to be completed first.');
    }

    $guidelines = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "guidelines"));

    if (!$guidelines->getCompleted()) {
      throw new TaskException('Cannot launch the evaluation.', 'The specifications have to be completed first.');
    }

    $importCorpus = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "import_corpus"));

    if (!$importCorpus->getCompleted()) {
      throw new TaskException('Cannot launch the evaluation.', 'The import data has to be completed first.');
    }

    //todo check pooling


    //todo change this to have an explicit start/stop
    //enable (redundant) and notify by email all evaluators invited (if new evaluators are invited successively they won't be notified)
    $evaluators = $progressRepository->findBy(array("project" => $project->getId(), "task" => "evaluation"));
    $evaluatorsCount = count($evaluators);

    if ($evaluatorsCount == 0) {
      throw new TaskException('Cannot launch the evaluation: there are no evaluators:.', 'At least one evaluator must be invited and accept the invitation.');
    } else {

      $progress->setCompleted(TRUE);
      $progress->setDisabled(TRUE);

      /** @var Progress $evaluator */
      foreach ($evaluators as $evaluator) {
        //this is not necessary as they are set in this way when they accept the invitation!
        //todo if the task is restarted and an evaluator has finished yet the work is marked as not finished and cannot be restored
        //todo check if this is necessary
        $evaluator->setModified(new DateTime('now'));
        $evaluator->setDisabled(FALSE);
        $progressRepository->save($evaluator);
        $this->sendStartEvaluationEmail($project, $evaluator->getUser(), $host);
      }
    }

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @param $host
   * @return Progress
   * @throws Exception
   * @throws MailDeliveryException
   * @throws ProgressException
   * @throws TaskException
   */
  private function stopAndGetEvaluation($project, $user, $progress, $host)
  {
    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

    //check if the review is complete
    $review = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "review"));

    if ($review && $review->getCompleted()) {
      throw new TaskException('Cannot stop the evaluation.', 'The review has to be marked as incomplete before.');
    }

    $finalize = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "finalize"));

    if ($finalize->getCompleted()) {
      throw new TaskException('Cannot stop the evaluation.', 'The project has to be marked as incomplete before.');
    }

    // check if the evaluators have finished
    $evaluators = $progressRepository->findBy(array("project" => $project->getId(), "task" => "evaluation"));
    $evaluatorsCount = count($evaluators);

    if ($evaluatorsCount == 0) {
      throw new TaskException('Cannot stop the evaluation.', 'There are no evaluators.');
    }

    $activeEvaluators = array();
    /** @var Progress $evaluator */
    foreach ($evaluators as $evaluator) {
      if (!$evaluator->getCompleted()) {
        array_push($activeEvaluators, $evaluator);
      }
    }

    if (count($activeEvaluators) == 0) {
      throw new TaskException('Cannot stop the evaluation.', 'All ' . count($evaluators) . ' evaluators have already finished the evaluation.');
    }

    $progress->setCompleted(FALSE);
    $progress->setDisabled(TRUE);

    // send an email to stop the active evaluators
    foreach ($activeEvaluators as $evaluator) {

      //$evaluator->setCompleted(FALSE);
      $evaluator->setDisabled(TRUE);
      $evaluator->setModified(new DateTime('now'));
      $progressRepository->save($evaluator);

      $this->sendStopEvaluationEmail($project, $evaluator->getUser(), $host);
    }
    return $progress;
  }

  /**
   * Confirms the evaluation.
   *
   * @author giuliano
   * @Method("POST")
   * @Route("/evaluation/confirm", name="app_confirm_evaluation", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function confirm(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['id', 'reset']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isEvaluator($user, $project);

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

      if (filter_var($params['reset'], FILTER_VALIDATE_BOOLEAN)) {

        // set don't know evaluations
        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User $evaluator */
        //$evaluator = $userRepository->findOneBy(array('id' => $user->getId(), 'active' => 1));
        $evaluator = $userRepository->find($user->getId());
        $annotationRepository->dontKnow($params['id'], $project->getId(), $evaluator);
      }

      // confirm
      $annotationRepository->confirmEvaluation($params['id'], $project->getId(), $user);

      if (filter_var($params['reset'], FILTER_VALIDATE_BOOLEAN)) {

        // send don't know notifications
        $countDontKnow = $annotationRepository->countDontKnow($project->getId(), $user);

        if ($project->getMaxdontknow() > 0 && $project->getMaxdontknow() <= $countDontKnow) {
          $host = $request->getSchemeAndHttpHost();
          $this->sendDontKnowAlert($project->getId(), $user, $countDontKnow, $project->getMaxdontknow(), $host);
        }
      }

      /** @var  SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');

      $countTUs = $sentenceRepository->countTUs($project->getId());
      $countDone = $annotationRepository->countDone($project->getId(), $user);

      $last = false;
      if ($countDone == $countTUs && $countDone > 0) {
        //complete the task

        /** @var ProgressRepository $progressRepository */
        $progressRepository = $this->getDoctrine()->getRepository('AppBundle:Progress');

        /** @var Progress $evaluationProgress */
        $evaluationProgress = $progressRepository->findOneBy(array('task' => 'evaluation', 'project' => $project->getId(), 'user' => $user->getId()));
        $evaluationProgress->setCompleted(true);
        $progressRepository->save($evaluationProgress);

        //send an email to the all reviewers, admins and the vendor who invited the evaluator
        $host = $request->getSchemeAndHttpHost();
        $this->sendEvaluationComplete($project, $user, $host);

        //if there is at least a reviewer and all evaluations were done send an email to reviewers
        $incompleteEvaluations = $progressRepository->findBy(array('project' => $project->getId(), 'task' => 'evaluation', 'completed' => 0));
        if (count($incompleteEvaluations) == 0) {
          $this->sendReviewStart($project, $host);
        }
        $last = true;
      }

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Evaluation confirmed.',
        'last' => $last,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot confirm the evaluation: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot confirm the evaluation.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Saves the evaluation.
   *
   * @author giuliano
   * @Method("POST")
   * @Route("/evaluation/save", name="app_save_evaluation", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function save(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['id', 'num', 'eval']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isEvaluator($user, $project);

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
      /** @var Sentence $sentence */
      $sentence = $sentenceRepository->findOneBy(array("num" => $params["num"]));

      if (is_null($sentence)) {
        throw new AnnotationException('Cannot find the segment to evaluate.', 'Specify a valid segment.');
      }

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $annotation = $annotationRepository->findOneBy(array("num" => $params["num"], "user" => $user));


      if (is_null($annotation)) {

        //introduce to solve the problem on user returned by the session.
        //It wasn't recognized by annotator repository as an existing user.

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User $evaluator */
        //$evaluator = $userRepository->findOneBy(array('id' => $user->getId(), 'active' => 1));
        $evaluator = $userRepository->find($user->getId());

        $annotation = new Annotation();
        $annotation->setOutputId($params["id"]);
        $annotation->setNum(intval($params["num"]));
        $annotation->setUser($evaluator);
        $annotation->setProject($project);
        $annotation->setEval(trim($params["eval"]));
        $annotationRepository->save($annotation);
        $message = "A new evaluation has been saved.";
      } else {
        if ($annotation->getEval() == trim($params["eval"])) {
          $message = "The evaluation has not been changed.";
          //$em->remove($annotation);
        } else {

          $message = "An existing evaluation has been updated.";
          $annotation->setEval(trim($params["eval"]));
          $annotationRepository->save($annotation);
        }
      }

      //reset progress
      $annotationRepository->unconfirmEvaluation($params["id"], $project->getId(), $user);

      return new Response(json_encode([
        'code' => -1,
        'message' => $message,
        'eval' => $annotation->getEval(),
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot save the evaluation: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot save the evaluation.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Renders the next segment to evaluate, if it exists; a message otherwise.
   * Invoked in panel/evaluation.twig.
   *
   * @author giuliano
   * @Route("/evaluation/next", name="app_evaluation_next")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function next(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      RequiredParametersChecker::check($params, ['last']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isEvaluator($user, $project);

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository("AppBundle:Sentence");
      $targets = $sentenceRepository->targetLabels($project->getId());

      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $userRoles = $roleRepository->getUserRoles($user->getId(), $project->getId());

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository('AppBundle:Progress');

      //check if the evaluation task is enable (not disabled and not completed)
      /** @var Progress $progress */
      $progress = $progressRepository->getProgress($progressRepository::EVALUATION_TASK, $project->getId(), $user->getId());

      if (is_null($progress)) {
        throw new Exception('Cannot find the next segment to evaluate.');
      }

      if ($progress->getCompleted()) {
        return $this->render('listing/evaluation.twig', array('success' => 'The evaluation is complete, thanks for being so helpful.'));
      }

      /** @var  SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');

      /** @var  AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

      $countTUs = $sentenceRepository->countTUs($project->getId());
      $countDone = $annotationRepository->countDone($project->getId(), $user);

      if ($progress->getDisabled()) {
        if ($countDone > 0) {
          return $this->render('listing/evaluation.twig', array('warning' => 'The evaluation has been stopped. The start will be notified by email.'));
        }

        return $this->render('listing/evaluation.twig', array('warning' => 'The evaluation cannot start yet. The start will be notified by email.'));
      }

      if ($countDone == $countTUs) {
        if ($countDone > 0) {
          return $this->render('listing/evaluation.twig', array('success' => 'The evaluation is complete, thanks for being so helpful.'));
        }
        return $this->render('listing/evaluation.twig', array('warning' => 'The evaluation cannot start yet. The start will be notified by email.'));
      }

      //get the next unconfirmed TU (if it exists)
      $next = $sentenceRepository->getFirstUndoneSentences($project->getId(), $user->getId());
      if ($next) {
        $id = $next[0]['id'];

        $sentences = $sentenceRepository->getTUSentences($id, $project->getId(), $project->getRandout());
//      if ($repository_annotation->isDone($id, $project->getId(), $user)) {
//        $done = 1;
//      }
        $annotations = $annotationRepository->getUserAnnotations($id, $project->getId(), $user);

        return $this->render('listing/evaluation.twig', array(
          'pid' => $project->getId(),
          'id' => $id,
          'project' => $project,
          'targets' => $targets,
          'userRoles' => $userRoles,
          'countTUs' => $countTUs,
          'countDone' => $countDone,
          'sentences' => $sentences,
          'annotations' => $annotations,
          //'done'=>$done
        ));

      }

      return $this->render('listing/evaluation.twig', array(
        'pid' => $project->getId(),
        'project' => $project,
        'targets' => $targets,
        'userRoles' => $userRoles,
        'countTUs' => $countTUs,
        'countDone' => $countDone,
      ));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  // emails

  /**
   * @param Project $project
   * @param $evaluatorId
   * @param $host
   * @throws MailDeliveryException
   * @throws UserException
   */
  function sendStartEvaluationEmail($project, $evaluatorId, $host)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
    /** @var User $evaluator */
    $evaluator = $userRepository->findOneBy(array("id" => $evaluatorId));

    if (is_null($evaluator)) {
      throw new UserException('User not found.', 'Specify a valid identfier.');
    }

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }
    $to = $evaluator->getEmail();
    $body = $this->renderView('mail/evaluationstart.email.twig',
      array('user' => $evaluator, 'host' => $host, 'project' => $project)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] The evaluation of the project "' . $project->getName() . '" has been started')
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      throw new MailDeliveryException($from, $to, $failures);
    }
  }

  //todo rewrite
  //function sendStopEvaluationEmail($projectID, $evaluator, $host)
  /**
   * @param Project $project
   * @param $evaluatorId
   * @param $host
   * @throws Exception
   * @throws MailDeliveryException
   */
  function sendStopEvaluationEmail($project, $evaluatorId, $host)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
    /** @var User $evaluator */
    $evaluator = $userRepository->findOneBy(array("id" => $evaluatorId));

    if (is_null($evaluator)) {
      throw new UserException('User not found.', 'Specify a valid identifier.');
    }

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }
    $to = $evaluator->getEmail();
    $body = $this->renderView('mail/evaluationstop.email.twig',
      array('user' => $evaluator, 'host' => $host, 'project' => $project)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] The evaluation of the project "' . $project->getName() . '" has been stopped')
      ->setFrom($from)
      ->setTo($to)
      ->setBody(
        $body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      throw new MailDeliveryException($from, $to, $failures);
    }
  }


  //todo rewrite

  /**
   * Notifies all admins and vendors that an evaluator has overrun
   * the limit of "don't know" answers.
   *
   * @param $projectId
   * @param User $user
   * @param $countDontKnow
   * @param $maxDontKnow
   * @return int
   * @throws Exception
   */
  public function sendDontKnowAlert($projectId, User $user, $countDontKnow, $maxDontKnow, $host)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    /** @var RoleRepository $role_repository */
    $role_repository = $em->getRepository('AppBundle:Role');
    $admins = $role_repository->getUserByRole($projectId, "admin");
    $vendor = $role_repository->getInviter($projectId, $user->getId(), "evaluator");
    $users = array_merge($admins, $vendor);

    /** @var Role $admin */
    $emailAlreadySent = array();
    foreach ($users as $key => $userRow) {
      if (!in_array($userRow["id"], $emailAlreadySent)) {
        array_push($emailAlreadySent, $userRow["id"]);

        if ($this->container->hasParameter('mailer_sender_address')) {
          $from = $this->container->getParameter('mailer_sender_address');
        } else {
          $from = $this->container->getParameter('mailer_user');
        }

        $mail = \Swift_Message::newInstance()
          ->setSubject('[MT-equal] The limit of ' . $maxDontKnow . ' " don\'t know" answers has been exceeded')
          ->setFrom($from)
          ->setTo($userRow["email"])
          ->setBody(
            $this->renderView('mail/dontknowalert.email.twig',
              //blind user
              //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectId, 'projectname' => $project->getName())
              array('userTo' => $userRow["username"],
                'projectid' => $projectId,
                'projectname' => $project->getName(),
                'username' => $user->getUsername(),
                'hostname' => $host,
                'countDontKnow' => $countDontKnow,
                'maxDontKnow' => $maxDontKnow)
            ), 'text/html'
          );

        $failures = array();
        $result = $mailer->send($mail, $failures);
        if (!$result) {
          throw new \Exception($failures);
        } else {
          $countSent++;
        }
      }
    }
    return $countSent;
  }

  //todo rewrite

  /**
   * Notifies all admins, reviewers and vendors that the evaluation is complete.
   *
   * @param Project $project
   * @param User $user
   * @param $host
   * @return int
   * @throws MailDeliveryException
   */
  private function sendEvaluationComplete($project, User $user, $host)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $em = $this->getDoctrine();

    if ($project) {
      /** @var RoleRepository $roleRepository */
      $roleRepository = $em->getRepository('AppBundle:Role');
      $admins = $roleRepository->getUserByRole($project->getId(), "admin");
      $reviewers = $roleRepository->getUserByRole($project->getId(), "reviewer");
      $vendor = $roleRepository->getInviter($project->getId(), $user->getId(), "evaluator");
      $users = array_merge($admins, $reviewers, $vendor);

      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);
          $arrayParams = array('userTo' => $userRow["username"], 'hostname' => $host, 'projectid' => $project->getId(), 'projectname' => $project->getName());
          if ($userRow["role"] != "reviewer") {
            //not blind user
            $arrayParams['username'] = $user->getUsername();
          }

          //$from = $this->container->getParameter('mailer_user');
          if ($this->container->hasParameter('mailer_sender_address')) {
            $from = $this->container->getParameter('mailer_sender_address');
          } else {
            $from = $this->container->getParameter('mailer_user');
          }

          $to = $userRow["email"];
          $body = $this->renderView('mail/evaluationcomplete.email.twig',
            $arrayParams
          );

          $mail = \Swift_Message::newInstance()
            ->setSubject('[MT-equal] The evaluation is complete')
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
              $body, 'text/html'
            );

          if (!$mailer->send($mail, $failures)) {
            throw new MailDeliveryException($from, $to, $failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }

  /**
   * Notifies each reviewer involved to start the review.
   *
   * @param Project $project
   * @param $host
   * @return int
   * @throws MailDeliveryException
   */
  private function sendReviewStart($project, $host)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $em = $this->getDoctrine();

    /** @var RoleRepository $roleRepository */
    $roleRepository = $em->getRepository('AppBundle:Role');

    $reviewer = $roleRepository->getUserByRole($project->getId(), "reviewer");
    $admin = $roleRepository->getUserByRole($project->getId(), "admin");
    $users = array_merge($reviewer, $admin);
    $emailAlreadySent = array();
    foreach ($users as $key => $userRow) {
      if (!in_array($userRow["id"], $emailAlreadySent)) {
        array_push($emailAlreadySent, $userRow["id"]);

        //$from = $this->container->getParameter('mailer_user');
        if ($this->container->hasParameter('mailer_sender_address')) {
          $from = $this->container->getParameter('mailer_sender_address');
        } else {
          $from = $this->container->getParameter('mailer_user');
        }

        $to = $userRow["email"];
        $body = $this->renderView('mail/reviewstart.email.twig',
          array('hostname' => $host, 'username' => $userRow["username"], 'projectid' => $project->getId(), 'projectname' => $project->getName())
        );

        $mail = \Swift_Message::newInstance()
          ->setSubject('[MT-equal] A project is ready for review')
          ->setFrom($from)
          ->setTo($to)
          ->setBody(
            $body, 'text/html'
          );

        if (!$mailer->send($mail, $failures)) {
          throw new MailDeliveryException($from, $to, $failures);
        } else {
          $countSent++;
        }
      }
    }
    return $countSent;
  }
}