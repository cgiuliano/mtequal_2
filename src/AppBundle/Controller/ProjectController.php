<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 11/11/15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Role;
use AppBundle\Entity\User;

use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;

use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\SentenceRepository;

use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Service\AuthorizationException;
use AppBundle\Util\GenericException;
use AppBundle\Util\GenericValidator;

use AppBundle\Util\RequiredParametersChecker;

use AppBundle\Util\UploadedFileSanitizer;

use DateInterval;
use DateTime;
use Exception;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Response;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{
  /**
   * Returns the enriched project specified by id for the logged user.
   * Used by the overview.
   *
   * @author giuliano
   * @Method("GET")
   * @Route("/getProject", name="app_project_get_project", defaults={"_format" = "json"})
   * @param Request $request
   * @return JsonResponse|Response
   */
  public function getProject(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      $params = $request->query->all();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['owner', 'admin', 'reviewer', 'vendor']);

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $userRoles = $roleRepository->getUserRoles($user->getId(), $params["pid"]);
      $projectRoles = $roleRepository->statsRoles($params["pid"], "accepted");

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

      $tus = $sentenceRepository->countOutput($params["pid"]);
      $targets = $sentenceRepository->targetLabels($params["pid"]);

      /** @var CorpusRepository $corpusRepository */
      $corpusRepository = $this->getDoctrine()->getRepository("AppBundle:Corpus");
      $corpus = $corpusRepository->statsCorpus($params["pid"]);


      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'name' => $project->getName(),
        'type' => $project->getType(),
        'pooling' => $project->getPooling(),
        'description' => $project->getDescription(),
        'instructions' => $project->getInstructions(),
        'ranges' => $project->getRanges(),
        'requiref' => intval($project->getRequiref()),
        'randout' => intval($project->getRandout()),
        'blindrev' => intval($project->getBlindrev()),
        'sentlist' => intval($project->getSentlist()),
        'intragree' => intval($project->getIntragree()),
        'maxdontknow' => intval($project->getMaxdontknow()),
        'intratus' => intval($project->getIntratus()),
        'created' => $project->getCreated(),
        'modified' => $project->getModified(),
        'userRoles' => $userRoles,
        'projectRoles' => $projectRoles,
        'tus' => $tus,
        'targets' => $targets,
        'corpus' => $corpus,
        'message' => 'Project found.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Project not found: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Project not found.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Returns the list of projects for the logged user.
   *
   * @Route ("/list", name="app_project_list", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function getProjectList(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $projects = $projectRepository->getProjectList($user->getId(), true);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Projects found.',
        'projects' => $projects,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Projects not found: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Projects not found.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Renders the project monitor.
   *
   * @author giuliano
   * @Route("/monitor", name="app_project_monitor")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function monitorAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'owner', 'vendor']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

      $data = $progressRepository->taskReport($params['pid'], $user->getId());
      $data["isEvaluationStarted"] = $progressRepository->isCompletedTask(ProgressRepository::CHECKCORPUS_TASK, $params['pid']);

      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $data['userRoles'] = $roleRepository->getUserRoles($user->getId(), $params['pid']);

      $data["projectRoles"] = $roleRepository->statsRoles($params['pid'], "accepted");

      $data["evaluators"] = $roleRepository->getUsers($params['pid'], $user->getId());
      $data['pid'] = $params['pid'];
      $data['projectName'] = $project->getName();

      return $this->render('project/monitor.twig', $data);
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Renders the create new project page.
   *
   * @Route("/new", name="app_project_create_page")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function newAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      if (!$user->getCanCreate()) {
        //the user cannot crete a new project
        throw new Exception('Cannot create a new project.');
      }

      $this->get("session")->remove("project");
      return new Response($this->renderView('default/create-project.twig'));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Clones the project specified by id.
   *
   * @Route("/clone", name="app_project_clone", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function cloneAction(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      if (!$user->getCanCreate()) {
        throw new AuthorizationException('User not authorized to clone projects.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['name']);

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");

      /** @var Project $projectToClone */
      $projectToClone = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $projectToClone);

      /** @var Project $cloneProject */
      $clonedProject = $projectRepository->create($params);

      $clonedProject->setRequiref($projectToClone->getRequiref());
      $clonedProject->setRandout($projectToClone->getRandout());
      $clonedProject->setBlindrev($projectToClone->getBlindrev());
      $clonedProject->setSentlist($projectToClone->getSentlist());

      $clonedProject->setIntratus($projectToClone->getIntratus());
      $clonedProject->setIntragree($projectToClone->getIntragree());
      $clonedProject->setMaxdontknow($projectToClone->getMaxdontknow());
      $clonedProject->setType($projectToClone->getType());
      $clonedProject->setPooling($projectToClone->getPooling());
      //$clonedProject->setDonePooling($projectToClone->getDonePooling());

      $clonedProject->setDonePooling("0");
      $clonedProject->setDatasetCreated(false);

      $clonedProject->setRanges($projectToClone->getRanges());
      $clonedProject->setInstructions($projectToClone->getInstructions());

      $projectRepository->save($clonedProject);

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $roleRepository->updateRole($user->getId(), $user->getId(), $clonedProject->getId(), "owner", "accepted");

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progressRepository->initProgress($clonedProject->getId(), $user->getId());

      $redirect = $this->generateUrl('admin');

      return new Response(json_encode([
        'code' => -1,
        'id' => $clonedProject->getId(),
        //'link' => $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $clonedProject->getId(),
        'name' => $clonedProject->getName(),
        'redirect' => $redirect,
        'description' => $clonedProject->getDescription(),
        'message' => 'The project has been cloned correctly.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        //'message' => 'Cannot clone the project: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'message' => 'Cannot clone the project: ' . lcfirst($e->getMessage()),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot clone the project.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Creates a project entity.
   *
   * @Route("/create", name="app_project_create", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function create(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->request->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      if (!$user->getCanCreate()) {
        throw new AuthorizationException('User not authorized to create projects.');
      }
      //todo user single transaction for all DB save

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->create($params);

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $roleRepository->updateRole($user->getId(), $user->getId(), $project->getId(), "owner", "accepted");

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progressRepository->initProgress($project->getId(), $user->getId());

      // if the project is created correctly the user is redirect to the project overview
      // and the session is updated (in admin)

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'link' => $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $project->getId(),
        'name' => $project->getName(),
        'description' => $project->getDescription(),
        'message' => 'The project has been created correctly.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot create the project: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot create the project.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Updates a project entity.
   *
   * @Route("/update", name="app_project_update", defaults={"_format" = "json"})
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function update(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->request->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      //todo user single transaction for all DB save

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $updatedProject = $projectRepository->update($params);

      return new Response(json_encode([
        'code' => -1,
        'id' => $updatedProject->getId(),
        'name' => $updatedProject->getName(),
        'type' => $updatedProject->getType(),
        'pooling' => $updatedProject->getPooling(),
        'description' => $updatedProject->getDescription(),
        'instructions' => $updatedProject->getInstructions(),
        'ranges' => $updatedProject->getRanges(),
        'requiref' => intval($updatedProject->getRequiref()),
        'randout' => intval($updatedProject->getRandout()),
        'blindrev' => intval($updatedProject->getBlindrev()),
        'sentlist' => intval($updatedProject->getSentlist()),
        'intragree' => intval($updatedProject->getIntragree()),
        'maxdontknow' => intval($updatedProject->getMaxdontknow()),
        'intratus' => intval($updatedProject->getIntratus()),
        'created' => $updatedProject->getCreated(),
        'modified' => $updatedProject->getModified(),
        'link' => $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $updatedProject->getId(),
        'message' => 'The project has been updated correctly.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot update the project: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot update the project.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Uploads and returns the content of the uploaded file.
   * This procedure doesn't save the instructions in the database.
   *
   * @Route("/project/uploadInstructions", name="app_project_upload_instructions", defaults={"_format" = "json"})
   * @param Request $request
   * @Method("POST")
   * @return Response
   */
  public
  function uploadInstructions(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      $params = $request->query->all();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);


      /** @var  UploadedFile $uploadedFile */
      $uploadedFile = $request->files->get('upfile');
      if (!$uploadedFile->isValid()) {
        return new Response(json_encode([
          'code' => 0,
          'exception' => 'Exception',
          'message' => 'Cannot upload the file.',
          'valid' => false], JSON_PRETTY_PRINT));

      }

      $extension = $uploadedFile->guessExtension();

      if ($extension != "html" && $extension != "txt" && $extension != "xml") {
        return new Response(json_encode([
          'code' => 0,
          'exception' => 'Exception',
          'message' => 'Cannot upload the file, it must be \'html\' or \'txt\'.',
          'valid' => false], JSON_PRETTY_PRINT));
      }

      UploadedFileSanitizer::sanitize($uploadedFile);

      $content = file_get_contents($uploadedFile);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'File uploaded successfully.',
        'content' => $content,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot upload the file.',
        'valid' => false], JSON_PRETTY_PRINT));

    }
  }

  /**
   * Deletes the specified project. The project is deactivated, no data is really deleted.
   * Only the owner can delete the project.
   *
   * @Route("/deleteProject", name="app_project_delete", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @author giuliano
   * @return Response
   */
  public function deleteProject(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwner($user, $project);

      $projectRepository->deactivate($project);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Project deleted.',
        'link' => $request->getSchemeAndHttpHost() . $this->get('router')->generate('app_dashboard_page'),
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot delete the project: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot delete the project.',
        'valid' => false], JSON_PRETTY_PRINT));

    }
  }

  /**
   * Returns all roles and users for the specified project.
   *
   * @Route("/roles", name="app_project_get_roles", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @author giuliano
   * @return Response
   */
  public function getProjectRoles(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $expiringHours = InviteController::DEFAULT_APP_INVITATION_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.invitation.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.invitation.expiring_hours');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, array('owner', 'admin', 'vendor'));

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $roles = $roleRepository->getRoles($params["pid"], $user->getId());

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
      $expired = array();
      $users = array();
      foreach ($roles as $role) {
        if ($role['active']) {
          $expired[$role['id']] = false;
        } else {
          /** @var DateTime $expires */
          $expires = $role['registered'];

          $expires->add(new DateInterval('PT' . $expiringHours . 'H'));
          $now = new DateTime("now");
          if ($now > $expires) {
            $expired[$role['id']] = true;
          } else {
            $expired[$role['id']] = false;
          }
        }

        /** @var User $inviter */
        $inviter = $userRepository->getUserFromID($role["inviter"]);
        if ($inviter) {
          $users[$role["inviter"]] = $inviter->getUsername();
        }
      }

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Project roles found.',
        'roles' => $roles,
        'users' => $users,
        'expired' => $expired,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot get the project roles: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot get the project roles.',
        'valid' => false], JSON_PRETTY_PRINT));

    }
  }

  /**
   * Returns all favourite users for the specified role in the project.
   *
   * @Route("/favourites", name="app_project_get_favourites", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @author giuliano
   * @return Response
   */
  public function getFavourites(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      RequiredParametersChecker::check($params, ['role']);
      GenericValidator::validateRole($params['role']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['owner', 'admin', 'vendor']);

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $favourites = $roleRepository->getFavourites($user, $params['role']);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Favourite users found.',
        'user' => $user->getUsername(),
        'role' => $params['role'],
        'favourites' => $favourites,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot find favourite users: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot find favourite users.',
        'valid' => false], JSON_PRETTY_PRINT));

    }
  }
}
