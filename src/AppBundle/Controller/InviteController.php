<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 07/12/17
 * Time: 08:14
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;

use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\GenericException;
use AppBundle\Util\InviteParametersValidation;
use AppBundle\Util\MailDeliveryException;
use AppBundle\Util\ParameterException;
use AppBundle\Util\PasswordGenerator;
use AppBundle\Util\ReplyParametersValidation;
use AppBundle\Util\RequiredParametersChecker;

use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoleController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class InviteController extends Controller
{
  // This is a default if app.invitation.expiring_hours is not specified in config.yml
  const DEFAULT_APP_INVITATION_EXPIRING_HOURS = 48;

  /**
   * Returns all requests to work to projects.
   * Invoked by requests.twig.
   *
   * @Route("/invite/requests", name="app_user_get_requests", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function requests(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $requests = $roleRepository->getUserRequests($user->getId());

      return new Response(json_encode([
        'code' => -1,
        'requests' => $requests,
        'message' => 'User requests.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot find requests: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot find requests.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Invites a user to work on a project in a specific role.
   * If the user doesn't exist, it creates the account too.
   *
   * @Route("/invite/send", name="app_user_find_user_to_invite", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function invite(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $expiringHours = self::DEFAULT_APP_INVITATION_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.invitation.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.invitation.expiring_hours');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $inviter = $authenticatedUser->get();

      RequiredParametersChecker::check($params, ['email', 'pid', 'role']);
      InviteParametersValidation::validate($params);

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      //invitation to non-active accounts are allowed if their activation link is not expired
      $invitee = $userRepository->findNotExpiredUserByEmail($params['email'], $expiringHours);

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($inviter, $project, array('owner', 'admin', 'vendor'));

      //$inviteeNotFound = $invitee == null;

      $host = $request->getSchemeAndHttpHost();

      if ($invitee == null) {
        //invitee not found or activation link expired the account must be created

        $password = PasswordGenerator::generate(12);
        $usernameGenerator = $this->get('app.username_generator');
        $username = $usernameGenerator->fromEmail($params['email']);

        $invitee = $userRepository->create(array(
          'username' => $username,
          'email' => $params['email'],
          'password' => $password,

        ), false);

        // force the password to expire, so the user has to change the password at first login
        $userRepository->resetPasswordChange($invitee);

        /** @var RoleRepository $roleRepository */
        $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
        $role = $roleRepository->updateRole($inviter->getId(), $invitee->getId(), $params["pid"], $params["role"], null);

        if (is_null($role)) {
          throw new ProjectException('Cannot assign the specified role: \'' . $params["role"] . '\'', 'Specify a valid role.');
        }

        $this->sendInvitationAndActivationMail($inviter, $invitee, $password, $host, $project, $role);
      } else {
        //invitee found the account exists

        /** @var RoleRepository $roleRepository */
        $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
        $existingRole = $roleRepository->findOneBy(array('user' => $invitee->getId(), 'role' => $params['role'], 'project' => $project->getId()));

        if ($existingRole) {
          throw new ProjectException('The user \'' . $invitee->getUsername() . '\' has been already invited as \'' . $params["role"] . '\'.', 'Specify a different user.');
        }

        if ($inviter->getId() == $invitee->getId()) {
          $role = $roleRepository->updateRole($inviter->getId(), $invitee->getId(), $params["pid"], $params["role"], 'accepted');
        } else {

          $role = $roleRepository->updateRole($inviter->getId(), $invitee->getId(), $params["pid"], $params["role"], null);

          if (is_null($role)) {
            throw new ProjectException('Inadmissible role \'' . $params["role"] . '\'', 'Specify a valid role.');
          }

          $this->sendInvitationMail($inviter, $invitee, $project, $role, $host);
        }
      }

      return new Response(json_encode([
        'code' => -1,
        'id' => $invitee->getId(),
        'username' => $invitee->getUsername(),
        'email' => $invitee->getEmail(),
        'firstName' => $invitee->getFirstname(),
        'lastName' => $invitee->getLastname(),
        'active' => $invitee->getActive(),
        'role' => $role->getRole(),
        'message' => 'User invited as ' . '\'' . $role->getRole() . '\'',
        'valid' => true], JSON_PRETTY_PRINT));


    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot invite the user: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot invite the user.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Invoked to accept or decline to work on a project.
   * Invoked by email or requests page.
   *
   * @Route("/invite/reply", name="app_user_reply_invitation")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function reply(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $params = $request->query->all();
      //authentication not required, it uses the hash

      RequiredParametersChecker::check($params, ['hash', 'action']);
      ReplyParametersValidation::validate($params);

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

      $role = $roleRepository->findOneBy(array("hash" => $params["hash"]));
      if (is_null($role)) {
        throw new Exception('Link not valid or expired.');
      }

      if ($role->getStatus() != 'pending') {
        throw new Exception('Invitation already ' . $role->getStatus() . '.');
      }

      $roleRepository->updateRole($role->getCreatedBy(), $role->getUser(), $role->getProject(), $role->getRole(), $params["action"]);

      if ($params["action"] == 'accepted') {
        $this->addFlash(
          'success',
          'Thanks for accepting the invitation to work with us as ' . $role->getRole() . '.'
        );
      } else {
        $this->addFlash(
          'info',
          'We are sorry you declined to work with us as ' . $role->getRole() . '.'
        );
      }

    } catch (Exception $e) {
      $this->addFlash(
        'danger',
        $e->getMessage()
      );
    }

    return $this->redirectToRoute('homepage');
  }

  /**
   * Revokes the access to a project to the specified user and notifies him/her by email.
   *
   * @Route("/invite/revoke", name="app_user_access_revoked", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response* @Method("POST")
   * @author giuliano
   */
  public function revoke(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $revoker = $authenticatedUser->get();

      //todo check authorization $revoker must be admin or owner or vendor (for his invitees only)

      RequiredParametersChecker::check($params, ['uid', 'role']);
      InviteParametersValidation::validate($params);

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      //$authorizedUser->isOwnerOrAdmin($revoker, $project);
      $authorizedUser->isAuthorized($revoker, $project, array('owner', 'admin', 'vendor'));

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
      /** @var User $invitee */
      $invitee = $userRepository->findOneBy(array("id" => $params["uid"]));

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

      $roleRepository->revokeUser($params["uid"], $project->getId(), $params["role"]);

      //todo don't sent the email if the user is inactive?

      if ($revoker->getId() != $invitee->getId()) {
        $this->sendAccessRevokedInvitationMail($revoker, $invitee, $project, $params["role"]);
      }

      return new Response(json_encode([
        'code' => -1,
        'id' => $invitee->getId(),
        'username' => $invitee->getUsername(),
        'email' => $invitee->getEmail(),
        'firstName' => $invitee->getFirstname(),
        'lastName' => $invitee->getLastname(),
        'active' => $invitee->getActive(),
        'message' => 'User revoked.',
        'valid' => true], JSON_PRETTY_PRINT));


    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot revoke the user: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot revoke the user.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Activates the account if the invited user has clicked on the activation link
   * and if it is valid and not expired.
   * Renders the sign in pages.
   * Called from an email or address bar.
   *
   * This method is similar activateAccount in UserController.
   *
   * @Route("/invite/activate", name="app_invited_user_activate_account")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function activateAccount(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      $expiringHours = self::DEFAULT_APP_INVITATION_EXPIRING_HOURS;
      if ($this->container->hasParameter('app.invitation.expiring_hours')) {
        $expiringHours = $this->container->getParameter('app.invitation.expiring_hours');
      }

      $params = $request->query->all();

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      /** @var User $user */
      $user = $userRepository->activateAccount($params, $expiringHours);

      // Uncomment to allow to accept all roles after the account has been activated
      /** @var RoleRepository $roleRepository */
      //$roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      //$roleRepository->acceptAll($user->getId());

      $this->addFlash('success', 'Thanks for registering! You can login.');
      return new Response($this->renderView('default/sign-in.twig'));

    } catch (GenericException $e) {
      $this->addFlash('danger',
        'Account activation failed: ' . lcfirst($e->getMessage()));
      return new Response($this->renderView('default/sign-in.twig'));
    } catch (Exception $e) {
      $this->addFlash('danger',
        'Account activation failed.');
      return new Response($this->renderView('default/sign-in.twig'));
    }
  }


  //emails

  /**
   * Sends the confirm registration (activate account) and accept invite to work link by email.
   * This is used when an account is automatically generated by the invite user action.
   *
   * @author giuliano
   * @param User $invitee
   * @param $password
   * @param $host
   * @param User $inviter
   * @param Project $project
   * @param $role
   * @throws MailDeliveryException
   */
  public function sendInvitationAndActivationMail($inviter, $invitee, $password, $host, $project, $role)
  {
    $this->get('logger')->info('sending invitation and activation email to ' . $invitee->getEmail());

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $expiringHours = self::DEFAULT_APP_INVITATION_EXPIRING_HOURS;
    if ($this->container->hasParameter('app.invitation.expiring_hours')) {
      $expiringHours = $this->container->getParameter('app.invitation.expiring_hours');
    }

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $to = $invitee->getEmail();
    $body = $this->renderView('mail/activate-invited-account.twig',
      array('invitee' => $invitee,
        'password' => $password,
        'host' => $host,
        'inviter' => $inviter,
        'project' => $project,
        'role' => $role,
        'expiringHours' => $expiringHours)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Project invitation')
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      $this->get('logger')->error('cannot deliver the invitation and activation email to ' . $invitee->getEmail());
      throw new MailDeliveryException($from, $to, $failures);
    }
  }

  /**
   * Sends an invite to work on the specified project by email.
   *
   * @author giuliano
   * @param User $inviter
   * @param User $invitee
   * @param Project $project
   * @param Role $role
   * @param $host
   * @throws MailDeliveryException
   */
  public function sendInvitationMail($inviter, $invitee, $project, $role, $host)
  {
    $this->get('logger')->info('sending invitation email to ' . $invitee->getEmail());

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $to = $invitee->getEmail();
    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $body = $this->renderView('mail/invite-user-email.twig',
      array('inviter' => $inviter, 'invitee' => $invitee, 'project' => $project, 'role' => $role, 'host' => $host)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Project invitation')
      ->setFrom($from)
      ->setTo($to)
      ->setBody(
        $body, 'text/html'
      );


    if (!$mailer->send($mail, $failures)) {
      $this->get('logger')->error('cannot deliver the invitation email to ' . $invitee->getEmail());
      throw new MailDeliveryException($from, $to, $failures);
    }
  }

  /**
   * Sends a access revoked email.
   *
   * @param User $revoker
   * @param User $invitee
   * @param Project $project
   * @param $role
   * @throws MailDeliveryException
   */
  public function sendAccessRevokedInvitationMail($revoker, $invitee, $project, $role)
  {
    $this->get('logger')->info('sending revoke invitation email to ' . $invitee->getEmail());

    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $to = $invitee->getEmail();
    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $body = $this->renderView('mail/access-revoked-email.twig',
      array('revoker' => $revoker, 'invitee' => $invitee, 'project' => $project, 'role' => $role)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Access revoke')
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      $this->get('logger')->error('cannot deliver the access revoke email to ' . $invitee->getEmail());
      throw new MailDeliveryException($from, $to, $failures);
    }
  }
}