<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 10:17
 */

namespace AppBundle\Controller\API;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;

use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\SentenceRepository;

/**
 * Class JSONController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONController extends APIController
{
  /**
   * @Route("/api/test/ping", name="test/ping", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function test(Request $request)
  {
    $data = array();
    $data["header"]["status"] = "200";
    $data["header"]["description"] = "test method";
    $data["result"] = "It works!";

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  function getEvaluationLabels($json)
  {
    $ranges = json_decode($json, TRUE);
    $r = array();
    foreach ($ranges as $range) {
      $r[$range["val"]] = $range["label"];
    }
    return $r;
  }

  /**
   * This is used for automatic evaluation, for the decision
   * tree it returns just 5 possibles combinations of answers.
   *
   * @author giuliano
   * @param $type
   * @param $min
   * @param $max
   * @return int
   */
  function randomEvaluation($type, $min, $max)
  {

    if ($type == "tree") {

      $r = rand(0, 18);

      if ($r == 0) {
        return 0;
      } else if ($r == 1) {
        return 11111;
      } else if ($r == 2) {
        return 11110;
      } else if ($r == 3) {
        return 11113;
      } else if ($r == 4) {
        return 11101;
      } else if ($r == 5) {
        return 11100;
      } else if ($r == 6) {
        return 11103;
      } else if ($r == 7) {
        return 10111;
      } else if ($r == 8) {
        return 10110;
      } else if ($r == 9) {
        return 10113;
      } else if ($r == 10) {
        return 10101;
      } else if ($r == 11) {
        return 10100;
      } else if ($r == 12) {
        return 10103;
      } else if ($r == 13) {
        return 10011;
      } else if ($r == 14) {
        return 10010;
      } else if ($r == 15) {
        return 10013;
      } else if ($r == 16) {
        return 10001;
      } else if ($r == 17) {
        return 10000;
      } else if ($r == 18) {
        return 10003;
      }
    }

    return rand($min, $max);
  }
}