<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 18:47
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Util\ParameterException;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class JSONProjectController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONProjectController extends JSONController
{
  /**
   * @Route("/api/project/create", name="project/create", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function apiCreateProject(Request $request)
  {
    /** @var User $user */
    $user = self::findAuthorizedUser($request);
    if ($user) {
      $data = self::initJSONArray("Create a new project");
      $data["header"]["status"] = "500";

      if ($request->query->has("name")) {
        $data["result"]["name"] = $request->query->get("name");

        /** @var ProjectRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
        /** @var Project $project */
        $project = $repository->findOneBy(array("name" => $data["result"]["name"], "active" => 1));
        if ($project) {
          $data["header"]["status"] = "500";
          $data["header"]["description"] = "A project with this name already exists";
        } else {
          if ($request->query->has("description")) {
            $data["result"]["description"] = $request->query->get("description");
          } else {
            $data["result"]["description"] = "";
          }

          $project = $repository->createProject(array("name" => $data["result"]["name"], "description" => $data["result"]["description"]));
          if ($project) {
            /** @var RoleRepository $role_repository */
            $role_repository = $this->getDoctrine()->getRepository("AppBundle:Role");
            $role_repository->updateRole($user->getId(), $user->getId(), $project->getId(), "owner", "accepted");
            //$role_repository->updateRole($user->getId(), $user->getId(), $project->getId(), "admin", "accepted");

            //init the progress status
            /** @var ProgressRepository $progress_repository */
            $progress_repository = $this->getDoctrine()->getRepository("AppBundle:Progress");
            $progress_repository->initProgress($project->getId(), $user->getId());

            $data["header"]["status"] = "200";
            $data["result"]["id"] = $project->getId();
            $data["result"]["link"] = $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $project->getId();
          } else {
            $data["header"]["description"] = "An error occur during the project creation";
          }
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'name' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Creates a new project entity.
   *
   * @Route("/api/project/createNEW", name="api_project_createNEW", defaults={"_format" = "json"})
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function create(Request $request)
  {
    /** @var User $user */
    $user = self::findAuthorizedUser($request);
    $params = $request->query->all();

    $data = self::initJSONArray("Create a new project");

    if ($user) {
      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

      try {
        $project = $projectRepository->create($params);
      } catch (ParameterException $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot create the project: ' . lcfirst($e->getMessage());
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      } catch (ProjectException $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot create the project: ' . lcfirst($e->getMessage());
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      } catch (Exception $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot create the project.';
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      }

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $roleRepository->updateRole($user->getId(), $user->getId(), $project->getId(), "owner", "accepted");

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progressRepository->initProgress($project->getId(), $user->getId());

      $data["header"]["status"] = "200";
      $data["result"]["id"] = $project->getId();
      $data["result"]["link"] = $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $project->getId();
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    }

    $data["header"]["status"] = "401";
    $data["header"]["description"] = "Unauthorized";
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Creates a new project entity.
   *
   * @Route("/api/project/updateNEW", name="api_project_updateNEW", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function update(Request $request)
  {
    $data = self::initJSONArray("Update the specified project");
    $params = $request->request->all();

    $user = self::findAuthorizedUserByProjectAndRole($params['id'], "admin", $request);
    if ($user == null) {
      $user = self::findAuthorizedUserByProjectAndRole($params['id'], "owner", $request);
    }

    if ($user == null) {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    }

    try {
      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

      $project = $projectRepository->update($params);

      $data["header"]["status"] = "200";
      $data["result"]['id'] = $project->getId();
      $data["result"]['name'] = $project->getName();
      $data["result"]['description'] = $project->getDescription();
      $data["result"]['type'] = $project->getType();
      $data["result"]['ranges'] = $project->getRanges();
      $data["result"]['requiref'] = $project->getName();

      $data["result"]['requiref'] = $project->getName();
      $data["result"]['instructions'] = $project->getName();
      $data["result"]['blindrev'] = $project->getName();
      $data["result"]['requiref'] = $project->getName();
      $data["result"]['requiref'] = $project->getName();

      //todo add all properties
      $data["result"]["link"] = $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin') . "?pid=" . $project->getId();

      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    } catch (ParameterException $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot update the project: ' . lcfirst($e->getMessage());
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    } catch (ProjectException $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot update the project: ' . lcfirst($e->getMessage());
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot update the project.';
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    }

  }

  /**
   * @Route("/api/project/update", name="project/update", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public
  function apiUpdateProject(Request $request)
  {
    $data = self::initJSONArray("Update a project");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $user = self::findAuthorizedUserByProjectAndRole($request->query->get("id"), "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($request->query->get("id"), "owner", $request);
      }
      if ($user != null) {
        $paramList = array("id", "description");

        //todo investigate if we can change the name (Claudio)
        //$paramList = array("id", "name", "description");

        foreach ($paramList as $param) {
          if ($request->query->has($param)) {
            $data["result"][$param] = $request->query->get($param);
          }
        }

        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
        if ($projectRepository->createProject($data["result"])) {
          $data["header"]["status"] = "201";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/delete", name="project/delete", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function apiDeleteProject(Request $request)
  {
    $data = self::initJSONArray("Deletes an existing project.");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Delete project failed";

    if ($request->query->has("id")) {
      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $projectRepository->findActiveProject($request->query->get("id"));

      if ($project) {
        $user = self::findAuthorizedUser($request);
        if (self::isUserEnable($user, $project->getId(), "delete")) {
          if ($projectRepository->deleteProject($project->getId()) == 1) {
            $data["header"]["status"] = "200";
            unset($data["header"]["description"]);
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have the permission to delete the project! Only the owner can delete it.";
        }
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Returns the alias used to anonymize the evaluators
   *
   * @author giuliano
   * @Route("/api/project/alias", name="project/alias", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserAlias(Request $request)
  {
    $data = array();
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Alias retrieval failed";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      $result = array();
      if (self::isUserEnable($user, $projectId, "show")) {

        //$projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
        //$project = $projectRepository->getProjectFromID($projectId);

        $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
        $evaluators = $roleRepository->getUsers($projectId, $user->getId(), "evaluator");
        foreach ($evaluators as $id => $info) {
          $eval = array();

          $eval["alias"] = $info["evaluator"]["anonymous"];

          $eval["username"] = $info["evaluator"]["username"];
          $eval["id"] = $id;


          array_push($result, $eval);
        }

        $data["header"]["status"] = "200";
        $data["header"]["description"] = "Returns the alias of project evaluators";
        $data["result"] = $result;

      }
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));

  }

  /**
   * @author giuliano
   * @Route("/api/project/report", name="project/report", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function apiMyProjectReport(Request $request)
  {

    $data = self::initJSONArray("Get the project report about the done evaluations and the reviewing");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Report generation failed";


    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      //
      if (self::isUserEnable($user, $projectId, "show")) {
        $report = array();
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
        $project = $projectRepository->findActiveProject($projectId);
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $sentence_repository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
        $targetLabels = $sentence_repository->targetLabels($projectId);
        $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

        //


        $summary = array();
        $summary["id"] = intval($projectId);
        $summary["type"] = $project->getType();
        $summary["countTUs"] = intval($sentence_repository->countTUs($projectId));
        $summary["countIntraTUs"] = $project->getIntratus();

        $evaluators = $roleRepository->getUsers($projectId, $user->getId(), "evaluator");
        $summary["countEvaluators"] = count($evaluators);
        //
        $evalNum = 1;
        $evals = array();
        foreach ($evaluators as $id => $info) {
          $eval = array();
          if ($project->getBlindrev()) {
            $eval["username"] = $info["evaluator"]["anonymous"];
          } else {
            $eval["username"] = $info["evaluator"]["username"];
            $eval["id"] = $id;
          }
          $evalNum++;
          array_push($evals, $eval);
        }
        //
        $summary["evaluators"] = $evals;
        $summary["countTargets"] = count($targetLabels);
        $summary["targetLabels"] = $targetLabels;


        //this has been introduced as ranking has no ranges
        if ($project->getType() == "ranking") {
          $ranges = array();
          $rangesCount = count($targetLabels);
        } else {
          $ranges = json_decode($project->getRanges(), TRUE);
          $rangesCount = count($ranges);
        }

        $summary["countRanges"] = count($ranges);
        $summary["ranges"] = $ranges;
        $report["summary"] = $summary;
        //
        if ($annotation_repository->countEvaluation($projectId) > 0) {
          $before = array();

          $before["results"] = $annotation_repository->evaluationSummary($project, $targetLabels, $ranges, $rangesCount, false);
          $before["quality"]["kStat"] = $annotation_repository->calculateKappaQuality($projectId, true);
          $trustability = array();

          $evalNum = 1;
          //
          foreach ($evaluators as $id => $info) {
            $inter = array();

            //Claudio: taken kappa=1
            $Pnorm = $annotation_repository->getObservedIntraAgreement($projectId, $id, true);
            $Pe = $annotation_repository->getExpectedIntraAgreement($projectId, $id, true);
            $kappa = 1;

            if ((1 - $Pe) != 0) {
              $kappa = round((($Pnorm - $Pe) / (1 - $Pe)), 3);
            }

            if ($project->getBlindrev()) {
              $inter["username"] = $info["evaluator"]["anonymous"];
              $evalNum++;
            } else {
              $inter["username"] = $info["evaluator"]["username"];
              $inter["id"] = $id;
            }
            $inter["Pnorm"] = $Pnorm;
            $inter["Pe"] = $Pe;
            $inter["kStat"] = $kappa;
            array_push($trustability, $inter);
          }

          $before["trustability"] = $trustability;
          $report["before"] = $before;
        }
        //
        if ($annotation_repository->countRevAnnotation($projectId) > 0) {
          $after = array();
          $after["results"] = $annotation_repository->evaluationSummary($project, $targetLabels, $ranges, $rangesCount, true);
          $after["quality"]["kStat"] = $annotation_repository->calculateKappaQuality($projectId, false);
          //reviewer evaluation divided by user + all

          //Claudio username or anonymous label must be returned
          //
          $after["evaluationsRev"] = $annotation_repository->reviewResults($projectId);
          $after["quality"]["totEvaluationsRev"] = 0;
          foreach ($after["evaluationsRev"] as $revInfo) {
            $after["quality"]["totEvaluationsRev"] += $revInfo["numrev"];
          }
          $report["after"] = $after;
          //
        }
        //
        $data["result"]["report"] = $report;
        $data["header"]["status"] = "200";
        $data["header"]["description"] = "Returns the evaluation report of the specified project.";

      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "You don't have permission to access to the project.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/export  ", name="project/export", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function apiProjectExport(Request $request)
  {
    $data = self::initJSONArray("Export the evaluations of the project");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Export data failed";

    $user = self::findAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("id")) {
        $projectId = $request->query->get("id");
        $user = self::findAuthorizedUser($request);
        if (self::isUserEnable($user, $projectId, "show")) {
          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          /** @var Project $project */
          $project = $projectRepository->findActiveProject($projectId);

          $format = "csv";
          if ($request->query->has("format")) {
            $format = $request->query->get("format");
          }
          if ($format == "csv") {
            /** @var AnnotationRepository $annotation_repository */
            $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
            return $annotation_repository->exportEvaluation($project->getId(), $project->getType(), $user->getId(), $format);
          } elseif ($format == "json") {
            /** @var AnnotationRepository $annotation_repository */
            $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
            //$data["result"] =
            //todo add the header
            return $annotation_repository->exportEvaluation($project->getId(), $project->getType(), $user->getId(), $format);
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'format' is not valid. The available formats are 'csv' and 'json'.";
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to the project.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter project 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/list", name="project/list", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiProjects(Request $request)
  {
    //todo check the the access to this method
    $data = self::initJSONArray("Get a list of the available project");
    $user = self::findAuthorizedUser($request);
    if ($user) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $projectList = $repository->getProjectList($user->getId());

      /** @var Project $project */
      if (count($projectList) > 0) {
        $data["result"]["size"] = count($projectList);
        $p = array();

        foreach ($projectList as $projectInfo) {
          $project = $repository->find($projectInfo["id"]);
          if ($project) {
            $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
            array_push($p,
              array("id" => $project->getId(),
                "name" => $project->getName(),
                "description" => $project->getDescription(),
                "myroles" => explode(", ", $projectInfo["roles"]),
                "created" => $created)
            );
          }
        }

        $data["header"]["status"] = "200";
        $data["result"]["projects"] = $p;
      } else {
        $data["header"]["status"] = "204";
      }

      /* $projects = $repository->findBy(array("active" => 1));
      if (count($projects) > 0) {
        $data["result"]["size"] = count($projects);
        $p = array();

        foreach ($projects as $project) {
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          array_push($p,
              array("id" => $project->getId(),
                  "name" => $project->getName(),
                  "description" => $project->getDescription(),
                  "created" => $created
              ));

        }
        $data["header"]["status"] = "200";
        $data["result"]["projects"] = $p;
      } else {
        $data["header"]["status"] = "204";
      }*/


    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/show", name="project/show", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiShowProject(Request $request)
  {
    // Implemented by Claudio
    //\AppBundle\Util\Log::write("apiProjects: " .$request);
    \AppBundle\Util\Log::write("apiProjects");

    $data = self::initJSONArray("Get all information about a project");

    if ($request->query->has("id")) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $repository->findActiveProject($request->query->get("id"));

      if ($project) {
        //\AppBundle\Util\Log::write("apiProjects: " .$project->getId());
        $data['message']['p'] = $project->getId();
        $user = self::findAuthorizedUser($request);
        $data['message']['u'] = $request->headers->get("X-Api-Key");
        \AppBundle\Util\Log::write("user1: " . $user->getId());
        if (self::isUserEnable($user, $project->getId(), "show")) {
          $p = array();
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          $p["settings"] =
            array("id" => $project->getId(),
              "name" => $project->getName(),
              "description" => $project->getDescription(),
              "evalformat"
              => array("type" => $project->getType(),
                "ranges" => json_decode($project->getRanges(), true),
                "requiref" => $project->getRequiref()
              ),
              "guidelines"
              => array(
                "specification"
                => array("randout" => $project->getRandout(),
                  "blindrev" => $project->getBlindrev(),
                  "maxdontknow" => $project->getMaxdontknow(),
                  "intragree" => $project->getIntragree() / 100,
                  "pooling" => $project->getPooling()
                ),
                "instructions" => $project->getInstructions()),
              "created" => $created);

          $user = self::findAuthorizedUserByProjectAndRole($project->getId(), "admin", $request);
          if ($user == null) {
            $user = self::findAuthorizedUserByProjectAndRole($project->getId(), "owner", $request);
          }
          if ($user != null) {
            /** @var RoleRepository $roleRepository */
            $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
            $p["users"] =
              $roleRepository->getRoles($project->getId(), $user->getId());

            $dql = "SELECT p.id, p.task, p.completed, p.disabled, p.modified, p.duedate, p.user, u.username
                    FROM AppBundle:Progress AS p
                    INNER JOIN AppBundle:User AS u
                    WHERE p.user=u.id and p.project=:pid
                    ORDER BY p.num, p.id";
            /** @var Query $query */
            $query = $this->getDoctrine()->getManager()->createQuery($dql);
            $query->setParameter('pid', $project->getId());

            $p["tasks"] =
              $query->getResult();

            $p["progress"] =
              $this->getDoctrine()->getRepository('AppBundle:Annotation')->getAnnotationProgress($project->getId());
          }
          $data["result"]["project"] = $p;
          $data["header"]["status"] = "200";
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to this project.";
        }
      } else {
        $data["header"]["status"] = "500";
        $data["header"]["description"] = "Project not found.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/evalformat", name="project/evalformat", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public
  function apiSetEvaluationFormat(Request $request)
  {
    $data = self::initJSONArray("Set the evaluation format");
    $data["header"]["status"] = "500";
    $listOfFormats = ["binary", "scale", "ranking", "tree"];
    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        if ($request->query->has("type") &&
          in_array($request->query->has("type"), $listOfFormats)
        ) {
          $data["result"]["type"] = $request->query->get("type");

          // Claudio: bug fixed: check for targets: also the ranking uses the json from the content

          // Claudio: now ranking doesn't need the json anymore
          // todo: update if needed

          //if ($data["result"]["type"] == "ranking") {
          //  if ($request->query->has("targets")) {
          //    $data["result"]["targets"] = $request->query->get("targets");
          //  } else {
          //    $data["header"]["status"] = "400";
          //    $data["header"]["description"] = "The parameter 'targets' is missing. This integer tells the number of targets to be ranked.";
          //  }
          //} else {
          if (strlen($request->getContent()) > 0) {
            $error = self::json_validate($request->getContent());
            if ($error == "") {
              $data["header"]["status"] = "201";
              $data["result"]["ranges"] = $request->getContent();
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = $error;
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The content of the POST request doesn't contain any JSON.";
          }
          //}
          if ($request->query->has("requiref")) {
            $data["result"]["requiref"] = $request->query->get("requiref");
          }

          if ($data["header"]["status"] == "201") {
            /** @var ProjectRepository $projectRepository */
            $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
            if ($projectRepository->createProject($data["result"])) {

              // Claudio: bug fixed: removed duplicated [ ]
              $data["result"]["ranges"] = json_decode($request->getContent());
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The setting of the evaluation format failed.";
            }
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter evaluation 'type' is missing or not valid (use one of the following: " . join(", ", $listOfFormats) . ")";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/specifications", name="project/specifications", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public
  function apiSetSpecifications(Request $request)
  {
    $data = self::initJSONArray("Set the specifications");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        $foundParams = 0;
        $paramList = array("randout", "blindrev", "maxdontknow", "intragree", "pooling");
        foreach ($paramList as $param) {
          if ($request->query->has($param)) {
            $data["result"][$param] = $request->query->get($param);
            $foundParams++;
          }
        }

        if ($foundParams > 0) {
          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          /** @var Project $project */
          $project = $projectRepository->findActiveProject($data["result"]["id"]);
          if ($project) {
            if ($projectRepository->createProject($data["result"])) {
              $data["header"]["status"] = "201";
            }
            unset($data["result"]["instructions"]);
          } else {
            $data["header"]["description"] = "Project not found.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "No valid parameters found. You can set one of these: " . join(", ", $paramList);
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @author giuliano
   * @Route("/api/project/import", name="project/import", defaults={"_format" = "json"})
   * @Method({"GET"})
   * @param Request $request
   * @return Response
   */
  public
  function apiProjectImport(Request $request)
  {

    $data = self::initJSONArray("Imports the evaluation format and the guidelines from another project");
    $data["header"]["status"] = "500";
    $data["header"]["id"] = $request->query->get("id");
    $data["header"]["basedOn"] = $request->query->get("basedOn");

    if ($request->query->has("id") && $request->query->has("basedOn")) {
      $data["result"]["id"] = $request->query->get("id");
      $data["result"]["basedOn"] = $request->query->get("basedOn");

      //if ($request->getContent()) {
      $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

        $project = $projectRepository->findActiveProject($request->query->get("basedOn"));
        $pp = $projectRepository->getProject($request->query->get("basedOn"));
        if ($project) {
          $data["header"]["status"] = "200";
          $data["header"]["pp"] = $pp;
          $a = array();
          $a["id"] = $request->query->get("id");

          if ($request->query->has("evalformat") && $request->query->get("evalformat") == 1) {
            $a["type"] = $project->getType();
            $a["ranges"] = $project->getRanges();
            //if ($project->getRequiref()) {
            $a["requiref"] = $project->getRequiref();
            //}
            //$a["evalformat"] = $project->getEvalformat();
          }

          if ($request->query->has("guidelines") && $request->query->get("guidelines") == 1) {
            $a["pooling"] = $project->getPooling();
            $a["instructions"] = $project->getInstructions();
            $a["randout"] = $project->getRandout();
            $a["intragree"] = $project->getIntragree();
            $a["maxdontknow"] = $project->getMaxdontknow();
            //if ($project->getBlindrev()) {
            $a["blindrev"] = $project->getBlindrev();
            //}
            $a["guidelines"] = $project->getGuidelines();
          }

          if ($projectRepository->createProject($a)) {
            $data["header"]["status"] = "201";
          }
          $data["header"]["result"] = $a;

        } else {
          $data["result"]["error"] = "project errore";
        }

      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
      //} else {
      //  $data["header"]["status"] = "400";
      //  $data["header"]["description"] = "The project parameters are missing.";
      //}
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "Missing required parameters";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/project/instructions", name="project/instructions", defaults={"_format" = "json"})
   * @Method({"POST"})
   * @param Request $request
   * @return Response
   */
  public
  function apiSetInstructions(Request $request)
  {
    $data = self::initJSONArray("Set the instructions for evaluators");
    $data["header"]["status"] = "500";

    if ($request->query->has("id")) {
      $data["result"]["id"] = $request->query->get("id");
      if ($request->getContent()) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
        if ($user == null) {
          $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
        }
        if ($user != null) {
          $data["result"]["instructions"] = $request->getContent();

          /** @var ProjectRepository $projectRepository */
          $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
          if ($projectRepository->createProject($data["result"])) {
            $data["header"]["status"] = "201";
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The project instructions are missing.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
}