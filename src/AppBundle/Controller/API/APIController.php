<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 12/10/15
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class APIController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 * @deprecated
 */
class APIController extends UserController
{
  //todo verify if it needs to extend UserController

  //todo add authentication

  static $ROLES = ["admin", "reviewer", "evaluator", "vendor"];


  function json_validate($string)
  {
    // decode the JSON data
    $result = json_decode($string);

    // switch and check possible JSON errors
    switch (json_last_error()) {
      case JSON_ERROR_NONE:
        $error = ''; // JSON is valid // No error has occurred
        break;
      case JSON_ERROR_DEPTH:
        $error = 'The maximum stack depth has been exceeded.';
        break;
      case JSON_ERROR_STATE_MISMATCH:
        $error = 'Invalid or malformed JSON.';
        break;
      case JSON_ERROR_CTRL_CHAR:
        $error = 'Control character error, possibly incorrectly encoded.';
        break;
      case JSON_ERROR_SYNTAX:
        $error = 'Syntax error, malformed JSON.';
        break;
      // PHP >= 5.3.3
      case JSON_ERROR_UTF8:
        $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
        break;
      // PHP >= 5.5.0
      case JSON_ERROR_RECURSION:
        $error = 'One or more recursive references in the value to be encoded.';
        break;
      // PHP >= 5.5.0
      case JSON_ERROR_INF_OR_NAN:
        $error = 'One or more NAN or INF values in the value to be encoded.';
        break;
      case JSON_ERROR_UNSUPPORTED_TYPE:
        $error = 'A value of a type that cannot be encoded was given.';
        break;
      default:
        $error = 'Unknown JSON error occurred.';
        break;
    }

    return $error;
  }

  /**
   * Creates a copy of the uploaded file in the upload folder (specified in app/config/parameters.yml).
   * The file is moved to the destination folder only if it is valid.
   * This function is duplicated in CorpusController.
   *
   * @param UploadedFile $file
   * @param $projectId
   * @return mixed|null|string
   */
  function backupFile(UploadedFile $file, $projectId)
  {
    $target_path = $this->container->getParameter('uploaded_files_dir');
    //todo add DIRECTORY_SEPARATOR to the end of $target_path if not present
    if ($target_path != null) {

      if ($target_path)
        $target_path .= $projectId . DIRECTORY_SEPARATOR;

      if (!file_exists($target_path)) {
        mkdir($target_path, 0777);
      }

      $target_path .= $file->getClientOriginalName();

      // see http://api.symfony.com/3.0/Symfony/Component/HttpFoundation/File/UploadedFile.html#method_move
      //todo try: $file = $uploadedFile->move('/uploads/directory', $name);


      move_uploaded_file($file, $target_path);
      return $target_path;
    }
    return null;
  }

  private function getProjectSession($key)
  {
    /** @var Session $session */
    if ($this->get("session")->has("project")) {
      $projectSession = $this->get("session")->get("project");
      if (array_key_exists($key, $projectSession)) {
        return $projectSession[$key];
      }
    }
    return null;
  }

  /**
   * @author giuliano
   * @param $key
   * @param $value
   * @return null
   * @deprecated
   */
  private function setProjectSession($key, $value)
  {
    // API controllers doesn't use the session

    if ($this->get("session")->has("project")) {
      /** @var Session $session */
      $session = $this->get("session");

      $projectSession = $session->get("project");
      $projectSession[$key] = $value;
      $session->set("project", $projectSession);
    }
    return null;
  }

  private function getUserSession($key)
  {
    if ($this->get("session")->has("mysession")) {
      $mysession = $this->get("session")->get("mysession");
      if (array_key_exists($key, $mysession)) {
        return $mysession[$key];
      }
    }
    return null;
  }

  /**
   * Returns the user specified by the api key, if authorized to work on
   * the specified project with the specified role.
   *
   * @param $projectId
   * @param $role
   * @param $request
   * @return User|null
   */
  function findAuthorizedUserByProjectAndRole($projectId, $role, $request)
  {
    if (is_null($projectId)) {
      return null;
    }

    if (empty($role)) {
      return null;
    }

    $user = self::findAuthorizedUser($request);

    if ($user) {
      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $a = $roleRepository->getRole($user->getId(), $projectId, $role);
      if ($roleRepository->getRole($user->getId(), $projectId, $role)) {
        return $user;
      }
    }
    return null;
  }

  /**
   * Returns the user specified by the api kye, if authorized.
   *
   * @param Request $request
   * @return User|null
   */
  function findAuthorizedUser($request)
  {
    if (is_null($request)) {
      return null;
    }

    /** @var UserRepository $userRepository */
    $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');


    //removed the session authentication
//    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
//      $user = $userRepository->findOneBy(array("apiKey" => $request->headers->get("X-Api-Key")));
//    } else if (self::getUserSession("userid")) {
//      $user = $userRepository->findOneBy(array("id" => self::getUserSession("userid")));
//    }

    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      /** @var User $user */
      $user = $userRepository->findOneBy(array("apiKey" => $request->headers->get("X-Api-Key")));

      return $user;
    }
    return null;
  }

  // @param $action can be "show", "edit", "delete", "invite", "evaluate"
  function isUserEnable($user, $projectId, $action)
  {
    if ($user) {
      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');

      if ($action == "show") {
        if ($roleRepository->findBy(array("user" => $user->getId(), "project" => $projectId, "status" => "accepted"))) {
          return 1;
        }
      } else {

        /** @var ProjectRepository $projectRepository */
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');

        return $projectRepository->can($action, $projectId, $user->getId());
      }
    }

    return 0;
  }

  function initJSONArray($description = null)
  {
    $time = new \DateTime("now");
    $data = array();
    $data["header"] = array("date" => $time->format('l, Y-m-d H:i:s e'));
    if ($description != null)
      $data["header"]["description"] = $description;

    $data["result"] = array();
    return $data;
  }


  //update a task as completed

  /**
   * @param $task
   * @param $projectid
   * @param $userid
   * @return Progress
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  function doneTask($task, $projectid, $userid)
  {
    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

    /** @var Progress $progress */
    $progress = $repository_progress->updateProgress($task, $projectid, TRUE, $userid, null, null);

    if ($progress) {
      if ($progress->getTask() == ProgressRepository::EVALUATION_TASK) {
        //the code was moved in AdminController file (at the end of function evaluationAction)

        //todo Claudio: why there is no code here?

      } else {
        /** @var SentenceRepository $repository_sentence */
        $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

        if ($progress->getTask() == ProgressRepository::CHECKCORPUS_TASK) {

          self::sendEvaluationStart($projectid);

        } else if ($progress->getTask() == ProgressRepository::GUIDELINES_TASK) {


          $repository_sentence->applyPooling($projectid);


          $repository_sentence->createIntraCorpus($projectid);
        }
      }
    }
    return $progress;
  }

  // reset a task

  /**
   * @param $task
   * @param $projectid
   * @param $userTo
   * @param $usernameFrom
   * @return Progress
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  function undoneTask($task, $projectid, $userTo, $usernameFrom)
  {
    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository('AppBundle:Progress');

    /** @var Progress $progress */
    $progress = $repository_progress->updateProgress($task, $projectid, FALSE, $userTo->getId(), null, null);
    if ($progress) {
      if ($task == ProgressRepository::CHECKCORPUS_TASK) {
        self::sendEvaluationStop($projectid);
      } else if ($progress->getTask() == ProgressRepository::GUIDELINES_TASK) {

        //todo remove the pooling and intra here? Claudio:

        /** @var SentenceRepository $repository_sentence */
        $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

        // remove all intra TUs

        $repository_sentence->deleteIntraCorpus($projectid);


        $repository_sentence->deletePooling($projectid);

      } else if ($task == ProgressRepository::EVALUATION_TASK) {
        //put to null all user annotation
        if ($userTo) {
          $unsetConfirmed = $this->getDoctrine()->getRepository('AppBundle:Annotation')->unconfirmUserEvaluations($projectid, $userTo);
          if ($unsetConfirmed > 0) {
            self::sendEvaluationNotConfirmed($projectid, $userTo);
          }
        }
      } else if ($task == ProgressRepository::REVIEW_TASK) {
        self::sendTaskNotComplete($projectid, $task, $usernameFrom, "reviewer");
      }
      // The task has been reset by ...  to all admins
      self::sendTaskNotComplete($projectid, $task, $usernameFrom, "admin");
    }
    return $progress;
  }


  /**
   * returns json with status of a task
   * @Route("/task/incomplete", name="app_task_incomplete")
   * @param Request $request
   * @return Response
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  public function setTaskIncomplete(Request $request)
  {
    //Claudio


    $data = array("warning" => false, "message" => "Sorry, task reset failed!!");
    $myuserid = self::getUserSession("userid");


    if ($myuserid != null) {
      $projectid = self::getProjectSession("id");

      if ($projectid != null) {

        // $params["pid"] = $projectid;
        $task = $request->query->get("task");


        /** @var AnnotationRepository $repository_annotation */
        $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        if ($task == ProgressRepository::EVALFORMAT_TASK && $repository_annotation->countEvaluation($projectid) > 0) {
          $data["success"] = false;
          $data["message"] = "Before to reset this task you must delete ALL EVALUATION. See the <a href='" . $this->generateUrl('admin') . "?view=settings.delete'>delete project</a> panel.";
        } else {

          /** @var User $userTo */
          $userTo = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array("id" => $request->query->get("userid")));


          if ($userTo) {
            /** @var Progress $progress */


            $progress = self::undoneTask($task, $projectid, $userTo, self::getUserSession("username"));
            if ($progress && !$progress->getCompleted()) {
              /** @var ProgressRepository $repository_progress */
              $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
              //todo deprecate etasks
              $this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $myuserid));

              $data["success"] = true;
              $data["message"] = "The task has been reset correctly!";
            }
          }
        }
      }
    }


    return new Response(json_encode($data));
  }

  /**
   * update task
   * @Route("/task/complete", name="app_task_complete")
   * @param Request $request
   * @return Response
   * @throws \Doctrine\ORM\OptimisticLockException
   * @throws \Exception
   */
  public function setTaskComplete(Request $request)
  {

    //Claudio: deprecated

    $data = array("warning" => false, "message" => "The task cannot be changed now!");
    $userid = self::getUserSession("userid");

    if ($userid != null) {
      $projectid = $this->getProjectSession("id");

      if ($projectid != null) {

        $task = $request->query->get("task");


        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        $isDisabledTask = $repository_progress->findBy(array("project" => $projectid, "task" => $task, "disabled" => 0));


        if ($repository_progress->isValidTask($task) && $isDisabledTask != null) {
          if ($task == ProgressRepository::IMPORTCORPUS_TASK) {
            /** @var SentenceRepository $repository_sentence */
            $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

            //check if at least a corpus was uploaded correctly
            if ($repository_sentence->countTUs($projectid) == 0) {
              $data["message"] = "This task cannot be completed with no valid corpus!";
              return new Response(json_encode($data));
            }
          }

          /** @var Progress $progress */
          $progress = self::doneTask($task, $projectid, $userid);
          if ($progress->getCompleted()) {
            $data["success"] = true;
            $data["message"] = "The task is completed";

          }
          //add in the session the list of the enable tasks
          //todo deprecate etasks
          $this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $userid));
        }
      }
    }


    return new Response(json_encode($data));
  }


  //todo deprecate this method


  /**
   * @Route("/api/project/oldshow", name="project/oldshow", defaults={"_format" = "json"})
   */
  public function apiProject(Request $request)
  {
    //todo remove it
    $data = self::initJSONArray("Get all information about a project");

    if ($request->query->has("id")) {
      /** @var ProjectRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
      /** @var Project $project */
      $project = $repository->findActiveProject($request->query->get("id"));

      if ($project) {
        $user = self::findAuthorizedUser($request);
        if (self::isUserEnable($user, $project->getId(), "show")) {
          $p = array();
          $created = $project->getCreated()->format('l, Y-m-d H:i:s e');
          array_push($p,
            array("id" => $project->getId(),
              "name" => $project->getName(),
              "description" => $project->getDescription(),
              "evalformat"
              => array("type" => $project->getType(),
                "ranges" => json_decode($project->getRanges(), true),
                "requiref" => $project->getRequiref()
              ),
              "guidelines"
              => array(
                "specification"
                => array("randout" => $project->getRandout(),
                  "blindrev" => $project->getBlindrev(),
                  "maxdontknow" => $project->getMaxdontknow(),
                  "intragree" => $project->getIntragree() / 100,
                  "pooling" => $project->getPooling()
                ),
                "instructions" => $project->getInstructions()),
              "created" => $created));

          $user = self::findAuthorizedUserByProjectAndRole($project->getId(), "admin", $request);
          if ($user != null) {
            /** @var RoleRepository $roleRepository */
            $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
            array_push($p,
              array("users" => $roleRepository->getRoles($project->getId(), $user->getId())));

            $dql = "SELECT p.id, p.task, p.completed, p.disabled, p.modified, p.duedate, p.user, u.username
                    FROM AppBundle:Progress AS p
                    INNER JOIN AppBundle:User AS u
                    WHERE p.user=u.id and p.project=:pid
                    ORDER BY p.num, p.id";
            /** @var Query $query */
            $query = $this->getDoctrine()->getManager()->createQuery($dql);
            $query->setParameter('pid', $project->getId());

            array_push($p,
              array("tasks" => $query->getResult()));

            array_push($p,
              array("evaluationProgress" => $this->getDoctrine()->getRepository('AppBundle:Annotation')->getAnnotationProgress($project->getId())));
          }
          $data["result"]["projects"] = $p;
          $data["header"]["status"] = "200";
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to this project.";
        }
      } else {
        $data["header"]["status"] = "500";
        $data["header"]["description"] = "Project not found.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }


  //Claudio: old version
  //deprecated
  public function apiProjectReport(Request $request)
  {
    $data = self::initJSONArray("Get the project report about the done evaluations and the reviewing");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Report generation failed";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      if (self::isUserEnable($user, $projectId, "show")) {
        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $data["result"]["project"] = $projectId;
        $data["result"]["report"] = $annotation_repository->getEvalReportParams($projectId);
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "You don't have permission to access to the project.";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/refresh_token", name="api_refresh_token")
   */
  public function adminRefreshToken(Request $request)
  {
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Refresh the user access token.";
    /** @var User $user */
    $user = self::findAuthorizedUser($request);
    if ($user != null) {
      /** @var UserRepository $repository_user */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');
      if ($repository->updateUser($user->getId(), array("api_key" => "refresh"))) {
        $user = $repository->getUser(array('id' => $user->getId()));
        if ($user) {
          $data["header"]["status"] = "200";
          $data["result"]["token"] = $user->getApiKey();
        }
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/replyInvitation", name="user/replyInvitation", defaults={"_format" = "json"})
   */
  public function apiUserReply(Request $request)
  {
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Reply invitation failed!";
    $user = self::findAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("hash")) {
        $hash = $request->query->get("hash");

        if ($request->query->has("action")) {
          $action = $request->query->get("action");

          /** @var RoleRepository $repository */
          $repository = $this->getDoctrine()->getRepository('AppBundle:Role');

          /** @var Role $role */
          $role = $repository->findOneBy(array("hash" => $hash));
          if ($role != null) {
            if ($role->getUser() == $user->getId()) {
              if ($role->getStatus() == "pending") {
                if ($action == "accepted" || $action == "declined") {
                  $role = $repository->updateRole($role->getCreatedBy(), $role->getUser(), $role->getProject(), $role->getRole(), $action);
                  if ($role) {
                    if ($role->getStatus() == $action) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = "Accepts or declines an invitation to work on a specified project in the MT-EQuAl release installed on the specified hostname.";
                      $data["result"] = array("role" => $role->getRole(),
                        "project" => $role->getProject(),
                        "user" => $role->getUser(),
                        "role_status" => $role->getStatus(),
                        "hash" => $role->getHash());
                    }
                  }
                } else {
                  $data["header"]["status"] = "400";
                  $data["header"]["description"] = "The action values must be 'accepted' or 'declined'.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = 'The invitation failed because its status was not pending.';
              }
            } else {
              $data["header"]["status"] = "401";
              $data["header"]["description"] = "You have no permission to change this invitation.";
            }
          } else {
            $data["header"]["status"] = "403";
            $data["header"]["description"] = "This request is not valid or it has been already performed.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'action' is missing.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'hash' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/invite", name="user/invite", defaults={"_format" = "json"})
   */
  public function apiInviteUser(Request $request)
  {
    $data = self::initJSONArray("Invite a user in a project");
    $invitation = null;
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");

      /** @var User $requestUser */
      $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "admin", $request);
      if ($requestUser == null) {
        $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "owner", $request);
      }
      if ($requestUser == null) {
        $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "vendor", $request);
      }
      if ($requestUser != null) {
        if ($request->query->has("username")) {
          $role = "";
          if ($request->query->has("role") && in_array($request->query->get("role"), self::$ROLES)) {
            $role = $request->query->get("role");
          }
          if ($role != "") {
            /** @var UserRepository $userRepository */
            $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
            /** @var User $user */
            $user = $userRepository->getUser(array("username" => $request->query->get("username")));
            if ($user != null) {
              /** @var RoleRepository $roleRepository */
              $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");

              if (!$roleRepository->getRole($user->getId(), $projectId, $role)) {
                $invitation = $roleRepository->updateRole($requestUser->getId(), $user->getId(), $projectId, $role, null);

                if ($invitation != null) {
                  $data["header"]["status"] = "200";
                  $data["result"]["role"] = $invitation->getRole();
                  $data["result"]["role_status"] = $invitation->getStatus();
                  $data["result"]["user"] = $invitation->getUser();

                  try {
                    /** @var ProjectRepository $projectRepository */
                    $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
                    /** @var Project $project */
                    $project = $projectRepository->findActiveProject($invitation->getProject());

                    if ($project) {
                      $host = $request->getSchemeAndHttpHost();

                      //if (UserController::sendInvitationMail($user->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $user->getHash())) {
                      if ($this->sendInvitationMail($user, $requestUser, $project, $role, $host)) {
                        $data["header"]["message"] = "The email has been sent to " . $user->getUsername();
                        //send an email to who invited the user if he is not who call the request
                        if ($invitation->getCreatedBy() != $requestUser->getId()) {
                          /** @var User $inviteeUser */
                          $inviteeUser = $userRepository->getUser(array("id" => $invitation->getCreatedBy()));
                          if ($inviteeUser) {
                            //if ($this->sendInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $user->getHash(), $inviteeUser->getUsername())) {
                            if ($this->sendInvitationMail($inviteeUser, $requestUser, $project, $role, $host)) {
                              $data["header"]["message"] .= " and " . $inviteeUser->getUsername();
                            } else {
                              $data["header"]["message"] = "Sending email failed!";
                            }
                          }
                        }
                      } else {
                        $data["header"]["status"] = "403";
                        $data["header"]["message"] = "Sending email failed!";
                      }
                    } else {
                      $data["header"]["status"] = "500";
                      $data["header"]["description"] = "The project doesn't found.";
                    }
                  } catch (\Exception $e) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] = 'An error occurred sending the email message!';
                  }
                } else {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Invitation failed.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = "Invitation failed because this user has been already invite as $role.";
              }
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The user doesn't found.";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'role' is missing or not valid.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'username' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  // emails copied from InviteController

  /**
   * Sends an invite to work on the specified project by email.
   *
   * @author giuliano
   * @param User $inviter
   * @param User $invitee
   * @param Project $project
   * @param Role $role
   * @param $host
   * @return bool
   */
  public function sendInvitationMail($inviter, $invitee, $project, $role, $host)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $to = $invitee->getEmail();
    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $body = $this->renderView('mail/invite-user-email.twig',
      array('inviter' => $inviter, 'invitee' => $invitee, 'project' => $project, 'role' => $role, 'host' => $host)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Project invitation')
      ->setFrom($from)
      ->setTo($to)
      ->setBody(
        $body, 'text/html'
      );


    if (!$mailer->send($mail, $failures)) {
      return false;
    }
    return true;
  }

  /**
   * Sends a access revoked email.
   *
   * @param User $revoker
   * @param User $invitee
   * @param Project $project
   * @param $role
   * @return bool
   */
  public function sendAccessRevokedInvitationMail($revoker, $invitee, $project, $role)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    $to = $invitee->getEmail();
    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }

    $body = $this->renderView('mail/access-revoked-email.twig',
      array('$revoker' => $revoker, 'invitee' => $invitee, 'project' => $project, 'role' => $role)
    );

    $mail = \Swift_Message::newInstance()
      ->setSubject('[MT-equal] Access revoke')
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html'
      );

    if (!$mailer->send($mail, $failures)) {
      return false;
    }
    return true;
  }
  //
  /**
   * @Route("/api/user/revoke", name="user/revoke", defaults={"_format" = "json"})
   */
  public function apiRevokeUser(Request $request)
  {
    $data = self::initJSONArray("Revoke a user in a project");
    $invitation = null;
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");

      /** @var User $requestUser */
      $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "admin", $request);
      if ($requestUser == null) {
        $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "owner", $request);
      }
      if ($requestUser == null) {
        $requestUser = self::findAuthorizedUserByProjectAndRole($projectId, "vendor", $request);
      }
      if ($requestUser != null) {
        if ($request->query->has("username")) {
          $role = "";
          if ($request->query->has("role") && in_array($request->query->get("role"), self::$ROLES)) {
            $role = $request->query->get("role");
          }
          if ($role != "") {
            /** @var UserRepository $userRepository */
            $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
            /** @var User $user */
            $user = $userRepository->getUser(array("username" => $request->query->get("username")));
            if ($user != null) {
              /** @var RoleRepository $roleRepository */
              $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");

              if ($roleRepository->getRole($user->getId(), $projectId, $role)) {
                $invitation = $roleRepository->updateRole($requestUser->getId(), $user->getId(), $projectId, $role, "revoked");

                if ($invitation != null) {
                  $data["header"]["status"] = "200";
                  $data["result"]["role"] = $invitation->getRole();
                  $data["result"]["role_status"] = $invitation->getStatus();
                  $data["result"]["user"] = $invitation->getUser();

                  try {
                    /** @var ProjectRepository $projectRepository */
                    $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
                    /** @var Project $project */
                    $project = $projectRepository->findActiveProject($invitation->getProject());

                    if ($project) {
                      if ($this->revokeInvitationMail($user->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role)) {
                        $data["header"]["message"] = "The email has been sent to " . $user->getUsername();

                        //send an email to who invited the user if he is not who call the request
                        if ($role->getCreatedBy() != $requestUser->getId()) {
                          /** @var User $inviteeUser */
                          $inviteeUser = $userRepository->getUser(array("id" => $role->getCreatedBy()));
                          if ($inviteeUser) {
                            if ($this->revokeInvitationMail($inviteeUser->getEmail(), $user->getUsername(), $requestUser->getUsername(), $project->getName(), $role, $inviteeUser->getUsername())) {
                              $data["header"]["message"] .= " and " . $inviteeUser->getUsername();
                            } else {
                              $data["header"]["message"] = "Sending email failed!";
                            }
                          }
                        }
                      } else {
                        $data["header"]["message"] = "Sending email failed!";
                      }
                    } else {
                      $data["header"]["status"] = "500";
                      $data["header"]["description"] = "The project doesn't found.";
                    }
                  } catch (\Exception $e) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] = 'An error occurred sending the email message!' . $e;
                  }
                } else {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Revoke notification failed.";
                }
              } else {
                $data["header"]["status"] = "500";
                $data["header"]["description"] = "Revoke notification failed because this user has no role as $role in the project.";
              }
            } else {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = "The user doesn't found.";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'role' is missing or not valid.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'username' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }

    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/admin/resetdb", name="api_admin_resetdb")
   */
  public function resetDB(Request $request)
  {
    //Claudio: todo: fix authentication
    if ($request->query->has("secret")) {
      if ($request->query->get("secret") == $this->container->getParameter("secret")) {
        $tableToClean = array("Annotation", "Agreement", "Comment");

        $report = "Cleaning database ...";
        /** @var Query $query */
        $em = $this->getDoctrine()->getManager();
        foreach ($tableToClean as $table) {
          $query = $em->createQuery('DELETE AppBundle:' . $table);
          $query->execute();
        }
        $report .= "DONE!\n";

        //reset from start_evaluation for each project
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $em->getRepository("AppBundle:Progress");

        $projects = $em->getRepository('AppBundle:Project')->findBy(array("active" => 1));

        /** @var Project $project */
        foreach ($projects as $project) {
          //reset the evaluation task
          $report .= "Reset the start evaluation task in the project " . $project->getName() . " ...DONE!\n";
          //$progress = $repository_progress->updateProgress($repository_progress::START_EVALUATION_TASK, $project->getId(), FALSE, null, null, null);
          $progress = $repository_progress->updateProgress($repository_progress::CHECKCORPUS_TASK, $project->getId(), FALSE, null, null, null);
          if (!$progress) {
            $report .= "ERROR! Reset task failed.\n";
          }
          //start the evaluation task
          //$progress = $repository_progress->updateProgress($repository_progress::START_EVALUATION_TASK, $project->getId(), TRUE, 1, null, null);
          $progress = $repository_progress->updateProgress($repository_progress::CHECKCORPUS_TASK, $project->getId(), TRUE, 1, null, null);
          if (!$progress) {
            $report .= "ERROR! Reset task failed.\n";
          }
          //activate all evaluation tasks
          $evaluations = $repository_progress->findBy(array("project" => $project->getId(), "task" => "evaluation"));

          $em = $this->getDoctrine()->getManager();
          /** @var Progress $evaluation */
          foreach ($evaluations as $evaluation) {
            $report .= "- enable evaluation for user " . $evaluation->getUser() . "\n";

            $evaluation->setDisabled(false);
            $em->persist($evaluation);
          }
          $em->flush();
        }


        $tableToCount = array("Project", "Document", "Sentence", "User", "Role", "Progress", "Annotation", "Agreement", "Comment");
        $report .= "\n## Database report ##\n";

        foreach ($tableToCount as $table) {
          $items = $em->getRepository("AppBundle:" . $table)->findAll();
          $report .= "$table: " . count($items) . "\n";
        }

        return new Response($report);
      }
    }
    return new Response("Authorization failed! You are not an allowed user.\n");
  }

  /**
   * @Route("/api/admin/checkdb", name="api_admin_checkdb")
   */
  public function adminCheckDB(Request $request)
  {
    //Claudio: todo: fix authentication
    if ($request->query->has("secret") &&
      $request->query->get("secret") == $this->container->getParameter("secret")
    ) {
      $checkResult = "";
      $em = $this->getDoctrine()->getManager();

      //check evaluations vs. reviews
      $dql = "SELECT p.id, p.name, COUNT(p.id) as num
        FROM AppBundle:Annotation AS a
        INNER JOIN AppBundle:Project AS p
        WHERE a.project=p.id AND a.active=0
        GROUP BY p.id ORDER BY p.id";
      $query = $em->createQuery($dql);
      $projects = $query->execute();
      $activeHash = array();
      foreach ($projects as $project) {
        $activeHash[$project["name"]] = $project["num"];
      }

      $dql = "SELECT p.id, p.name, COUNT(p.id) as num
          FROM AppBundle:Annotation AS a
          INNER JOIN AppBundle:Project AS p
          WHERE a.project=p.id AND a.revuser>0
          GROUP BY p.id ORDER BY p.id";
      $query = $em->createQuery($dql);
      $projects = $query->execute();
      $revuserHash = array();
      foreach ($projects as $project) {
        $revuserHash[$project["name"]] = $project["num"];
      }

      $checkResult .= "# Revised evaluation: ";
      $report = "";
      foreach ($revuserHash as $pname => $num) {
        if (!array_key_exists($pname, $activeHash)) {
          $report .= "- '" . $pname . "' has " . $num . " reviews but no disabled evaluation\n";
        } else if ($num != $activeHash[$pname]) {
          $report .= "- '" . $pname . "' has " . $num . " reviews but " . $activeHash[$pname] . " disabled evaluation\n";
        }
        unset($activeHash[$pname]);
      }
      foreach ($activeHash as $pname => $num) {
        $report .= "- '" . $pname . "' has " . $num . " disabled evaluations but no review\n";
      }

      if ($report != "") {
        $checkResult .= "ERROR!\n$report";
      } else {
        $checkResult .= "OK.\n";
      }

      // check agreement
      /** @var  AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository('AppBundle:Agreement');

      $checkResult .= "# Check agreement measure: ";
      $report = "";
      $dql = "SELECT a.num AS num, COUNT(a.num) AS cnt, ag.n AS n, (a.project) AS project, (a.output_id) AS id
        FROM AppBundle:Annotation AS a
        INNER JOIN AppBundle:Agreement AS ag
        WHERE a.num=ag.num AND a.active=1 GROUP BY a.num";
      $query = $em->createQuery($dql);
      $agreements = $query->execute();
      foreach ($agreements as $agreement) {
        if ($agreement["cnt"] != $agreement["n"]) {
          $report .= "- " . $agreement["num"] . " (" . $agreement["project"] . ") must be reset\n";
          $repository_agreement->updateAgreement($agreement["id"], $agreement["project"]);
        }
      }
      if ($report != "") {
        $checkResult .= "ERROR!\n$report";
      } else {
        $checkResult .= "OK.\n";
      }

      return new Response($checkResult);

    }
    return new Response("Authorization failed! You are not an allowed user.\n");
  }


  /**
   * returns json with status of a task
   * @Route("/api/task/check", name="api_check_task")
   */
  public function checkTask(Request $request)
  {
    $data = array("completed" => false, "disabled" => false, "enabletasks" => "");
    $userid = self::getUserSession("userid");
    if ($userid != null) {
      $projectid = self::getProjectSession("id");
      if ($projectid != null) {
        $task = $request->query->get("task");
        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        if ($repository_progress->isValidTask($task)) {
          /** @var Progress $progress */
          $progress = $repository_progress->getProgress($task, $projectid, $userid);
          if ($progress) {
            $data["completed"] = $progress->getCompleted();
            $data["disabled"] = $progress->getDisabled();
          }


          //$data["enabletasks"] = join("' and '", $repository_progress->enableTasks($projectSession["id"], $userid));
          $data["enabletasks"] = join("' and '", $repository_progress->dependencyTasks($task));

        }
      }
    }
    return new Response(json_encode($data));
  }

  /**
   * change disabled status of a task (if it is true it moves to false and viceversa
   * @Route("/task/change", name="app_change_task")
   */
  public function changeStatusTask(Request $request)
  {
    //
    $data = array("success" => false);

    if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }
    $projectId = self::getProjectSession("id");
    $userId = $request->query->get("userid");
    $task = $request->query->get("task");
    //$myuserid = self::getUserSession("userid");

    /** @var ProgressRepository $repository_progress */
    $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");

    /** @var Progress $progress */
    $progress = $repository_progress->findOneBy(array("task" => $task, "project" => $projectId, "user" => $userId));
    if ($progress && !$progress->getCompleted()) {
      $em = $this->getDoctrine()->getManager();

      if ($progress->getDisabled()) {
        $progress->setDisabled(false);
      } else {
        $progress->setDisabled(true);
      }

      $progress->setModified(new \DateTime("now"));

      $em->persist($progress);
      $em->flush();

      //update the modified time of the project
      $this->getDoctrine()->getRepository("AppBundle:Project")->updateModifiedTime($projectId);

      self::sendEvaluationStatus($projectId, $userId, $progress->getDisabled());
      $data["success"] = true;
    }

    return new Response(json_encode($data));
  }

  /**
   * returns json with status of a task
   * @Route("/task/reset", name="app_reset_task")
   */
  public function resetTask(Request $request)
  {
    //
    if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }

    $messagetype = "danger";
    $message = "Task reset failed!";
    $myuserid = self::getUserSession("userid");
    //$params["view"] = 'settings.progress';
    if ($myuserid != null) {
      $projectid = self::getProjectSession("id");
      if ($projectid != null) {
        $params["pid"] = $projectid;
        $task = $request->query->get("task");

        /** @var AnnotationRepository $repository_annotation */
        $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        if ($task == ProgressRepository::EVALFORMAT_TASK && $repository_annotation->countEvaluation($projectid) > 0) {
          $messagetype = "warning";
          $message = "Before to reset this task you must delete ALL EVALUATION. See the <a href='" . $this->generateUrl('admin') . "?view=settings.delete'>delete project</a> panel.";
        } else {
          /** @var User $userTo */
          $userTo = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array("id" => $request->query->get("userid")));
          //
          if ($userTo) {
            /** @var Progress $progress */
            //
            $progress = self::undoneTask($task, $projectid, $userTo, self::getUserSession("username"));
            if ($progress && !$progress->getCompleted()) {
              /** @var ProgressRepository $repository_progress */
              $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
              //todo deprecate etasks
              $this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $myuserid));

              $messagetype = "success";
              $message = "The task has been reset correctly!";
            }
          }
        }
      }
    }
    $this->addFlash(
      $messagetype,
      $message
    );

    return $this->redirectToRoute('admin', $params);
  }


  /**
   * returns json with status of a task
   * @Route("/api/task/duedate", name="api_duedate_task"), defaults={"_format" = "json"}
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function setDueDateTask(Request $request)
  {
    //Claudio: this implementation is used by the old UI

    $data = array();
    $userid = self::getUserSession("userid");
    if ($userid != null) {
      if (self::getProjectSession("id")) {
        $task = $request->query->get("task");
        $duedate = $request->query->get("duedate");

        //
        //

        if (!empty($duedate)) {
          $duedate .= " 23:59:59";
        } else {
          $duedate = null;
        }

        /** @var ProgressRepository $repository_progress */
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        /** @var Progress $progress */
        $progress = $repository_progress->setDueDate($task, $duedate);
        $data["message"] = "OK";
        if ($progress) {
          if ($duedate != null) {
            $data["duedate"] = $progress->getDuedate()->format('Y-m-d H:i:s');
          }
        } else {
          $data["message"] = "Updating due date failed!";
        }
      }
    }
    //
    //

    //return new Response(json_encode($data));
    return new Response(json_encode($data), 200, array('Content-Type' => 'application/json'));
  }

  /**
   * send a message to a user to inform him that he have to confirm all done evaluations again
   * @param $projectid
   * @param $user
   * @return bool
   * @throws \Exception
   */
  private
  function sendEvaluationNotConfirmed($projectid, $user)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {

      if ($this->container->hasParameter('mailer_sender_address')) {
        $from = $this->container->getParameter('mailer_sender_address');
      } else {
        $from = $this->container->getParameter('mailer_user');
      }

      /** @var User $user */
      $mail = \Swift_Message::newInstance()
        ->setSubject('[MT-equal] You hav unconfirmed evaluations')
        ->setFrom($from)
        ->setTo($user->getEmail())
        ->setBody(
          $this->renderView('mail/evalunconfirm.email.twig',
            //blind user
            //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
            array('username' => $user->getUsername(), 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
          ), 'text/html'
        );

      $failures = array();
      $result = $mailer->send($mail, $failures);
      if (!$result) {
        throw new \Exception($failures);
      }
      return true;
    }
    return false;
  }

  /**
   * send a message to each user involved in the project about stoping evaluation
   * @param $projectid
   * @throws \Exception
   * @return integer
   */
  private
  function sendEvaluationStop($projectid)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");

      //get the counter of available TUs
      /** @var SentenceRepository $repository_sentence */
      $repository_sentence = $em->getRepository("AppBundle:Sentence");
      $tus = $repository_sentence->countTUs($projectid);

      $evaluators = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "evaluator");
      $vendors = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "vendor");
      $users = array_merge($evaluators, $vendors);
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          $doneTUs = $repository_annotation->countDone($projectid, $userRow["id"]);

          array_push($emailAlreadySent, $userRow["id"]);
          if ($doneTUs < $tus) {

            if ($this->container->hasParameter('mailer_sender_address')) {
              $from = $this->container->getParameter('mailer_sender_address');
            } else {
              $from = $this->container->getParameter('mailer_user');
            }

            $mail = \Swift_Message::newInstance()
              ->setSubject('[MT-equal] The evaluation of the project "' . $project->getName() . '" has been suspended')
              ->setFrom($from)
              ->setTo($userRow["email"])
              ->setBody(
                $this->renderView('mail/evaluationstop.email.twig',
                  //blind user
                  //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                  array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                ), 'text/html'
              );

            $failures = array();
            $result = $mailer->send($mail, $failures);
            if (!$result) {
              throw new \Exception($failures);
            } else {
              $countSent++;
            }
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to all admins involved in the project about reset a task
   * @param $projectid
   * @throws \Exception
   * @return integer
   */
  private function sendTaskNotComplete($projectid, $task, $userFrom, $roleType)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();
    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      $users = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, $roleType);
      /* owner is not alert anymore
        $owner = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "owner");
        $users = array_merge($users, $owner);
      */
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if ($userRow["username"] != $userFrom && !in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);

          if ($this->container->hasParameter('mailer_sender_address')) {
            $from = $this->container->getParameter('mailer_sender_address');
          } else {
            $from = $this->container->getParameter('mailer_user');
          }

          $mail = \Swift_Message::newInstance()
            ->setSubject('[MT-equal] A task has been reset')
            ->setFrom($from)
            ->setTo($userRow["email"])
            ->setBody(
              $this->renderView('mail/tasknotcomplete.email.twig',
                //blind user
                //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                array('task' => $task, 'userTo' => $userRow["username"], 'userFrom' => $userFrom, 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
              ), 'text/html'
            );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to each evaluator and vendor involved in the project about start evaluation
   * @param $projectid
   * @return int
   * @throws \Exception
   */
  private function sendEvaluationStart($projectid)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();

    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));

    if ($project) {
      /** @var ProgressRepository $repository_progress */
      $repository_progress = $em->getRepository("AppBundle:Progress");

      /** @var AnnotationRepository $repository_annotation */
      $repository_annotation = $em->getRepository("AppBundle:Annotation");

      //get the counter of available TUs
      /** @var SentenceRepository $repository_sentence */
      $repository_sentence = $em->getRepository("AppBundle:Sentence");
      $tus = $repository_sentence->countTUs($projectid);

      $evaluators = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "evaluator");
      $vendors = $em->getRepository('AppBundle:Role')->getUserByRole($projectid, "vendor");
      $users = array_merge($evaluators, $vendors);

      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        $doneTUs = $repository_annotation->countDone($projectid, $userRow["id"]);
        if ($doneTUs == $tus) {
          //update evaluation task for this user as already completed
          $repository_progress->updateProgress($repository_progress::EVALUATION_TASK, $projectid, TRUE, $userRow["id"]);
        }
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);

          if ($this->container->hasParameter('mailer_sender_address')) {
            $from = $this->container->getParameter('mailer_sender_address');
          } else {
            $from = $this->container->getParameter('mailer_user');
          }

          $mail = \Swift_Message::newInstance()
            ->setSubject('[MT-equal] The project "' . $project->getName() . '" is ready for evaluation')
            ->setFrom($from)
            ->setTo($userRow["email"])
            ->setBody(
              $this->renderView('mail/evaluationstart.email.twig',
                //blind user
                //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
              ), 'text/html'
            );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }
        }
      }
    }
    return $countSent;
  }


  /**
   * send a message to an evaluator and vendor involved in the project about start evaluation
   * @param $projectid
   * @return int
   * @throws \Exception
   */
  private
  function sendEvaluationStatus($projectId, $userId, $disabled)
  {
    $countSent = 0;
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');
    $time = new \DateTime("now");

    $em = $this->getDoctrine();

    /** @var Project $project */
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    if ($project) {
      /** @var RoleRepository $role_repository */
      $role_repository = $em->getRepository('AppBundle:Role');
      $inviter = $role_repository->getInviter($projectId, $userId, "evaluator");
      $evaluator = $role_repository->getRoles($projectId, $userId);

      $users = array_merge($evaluator, $inviter);

      $subject = '[MT-EQuAl] The project "' . $project->getName() . '" is ready for evaluation';
      $template = 'mail/evaluationstart.email.twig';
      if ($disabled) {
        $subject = '[MT-EQuAl] The evaluation of the project "' . $project->getName() . '" has been stopped';
        $template = 'mail/evaluationstop.email.twig';
      }
      $emailAlreadySent = array();
      foreach ($users as $key => $userRow) {
        if (!in_array($userRow["id"], $emailAlreadySent)) {
          array_push($emailAlreadySent, $userRow["id"]);

          if ($this->container->hasParameter('mailer_sender_address')) {
            $from = $this->container->getParameter('mailer_sender_address');
          } else {
            $from = $this->container->getParameter('mailer_user');
          }

          $mail = \Swift_Message::newInstance()
            ->setSubject($subject . ' (' . $time->format('Y-m-d H:i:s') . ')')
            ->setFrom($from)
            ->setTo($userRow["email"])
            ->setBody(
              $this->renderView($template,
                //blind user
                //array('hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectid, 'projectname' => $project->getName())
                array('username' => $userRow["username"], 'hostname' => $this->container->getParameter('hostname'), 'projectid' => $projectId, 'projectname' => $project->getName())
              ), 'text/html'
            );

          $failures = array();
          $result = $mailer->send($mail, $failures);
          if (!$result) {
            throw new \Exception($failures);
          } else {
            $countSent++;
          }

        }
      }
    }
    return $countSent;
  }

  /**
   * Returns the content of the uploaded file.
   * This procedure doesn't save the file.
   *
   * @Route("/api/file/content", name="api_file_content")
   * @param Request $request
   * @return Response
   * @deprecated
   * @see Project
   */
  public
  function getContentFromTextUploadedFile(Request $request)
  {

    $content = "";
    $message = "The upload failed.";
    $success = false;
    $uploadedFile = $request->files->get('upfile');
    if ($uploadedFile && ($uploadedFile instanceof UploadedFile) && ($uploadedFile->getError() == '0')) {
      if (file_exists($uploadedFile)) {
        $extension = $uploadedFile->guessExtension();

        if ($extension == "html" || $extension == "txt" || $extension == "xml") {
          $content = file_get_contents($uploadedFile);

          // cleaning html
          $config = \HTMLPurifier_Config::createDefault();
          $purifier = new \HTMLPurifier($config);
          $content = $purifier->purify($content);

          if ($extension == "txt") {
            $content = preg_replace("/\n/s", "<br>", $content);
          }
          $success = true;
          $message = "The file has been uploaded.";
          //todo purify the file
        } else {
          $message = "The file cannot be uploaded: only plain text or HTML files can be uploaded.";
        }
      }

    }
    $response = new Response(json_encode(array('success' => $success, 'message' => $message, 'content' => $content)));
    return $response;
  }

  /**
   * returns a report about user's rows
   * @Route("/api/user/report", name="api_user_report")
   * @return Response
   */
  public
  function apiUserReport()
  {
    /** @var UserRepository $repository */
    $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

    $query = $userRepository->createQueryBuilder('e')->select('count(e)')->getQuery();

    $data = array(
      'user_count' => $query->getSingleScalarResult(),
    );

    return new Response(
      json_encode($data), 200, array('Content-Type' => 'application/json')
    );
  }

  /**
   * returns a report about user's rows
   * @Route("/api/roletype/report", name="api_roletype_report", defaults={"_format" = "json"})
   * @return Response
   */
  public
  function apiRoletypeReport()
  {
    $dql = "SELECT r.name
        FROM AppBundle:RoleType AS r
        ORDER BY r.name";
    /** @var Query $query */
    $query = $this->getDoctrine()->getManager()->createQuery($dql);

    $items = $query->getResult();
    return new Response(
      json_encode($items),
      200,
      array('Content-Type' => 'application/json')
    );

  }

//  /**
//   * @Route("/my_project_report", name="my_project_report")
//   * @Method("GET")
//   * @param Request $request
//   * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
//   */
//  public
//  function myProjectReport(Request $request)
//  {
//    $this->get('logger')->info($request);
//    //this is invoked by the share report
//    //this variables are fed to replace the missing session
//    $projectId = $request->query->get("project");
//    $userId = $request->query->get("user");
//
//
//    if ($projectId) {
//      /** @var AnnotationRepository $annotation_repository */
//      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
//      $report = $annotation_repository->getEvalReportParams($projectId, $userId);
//
//      /** @var ProjectRepository $project_repository */
//      //$project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
//      //$project = $project_repository->getProjectFromID($projectId);
//
//      //$sentence_repository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
//      //$targetLabels = $sentence_repository->targetLabels($projectId);
//
//
//      /*if ($project) {
//        $report["evaluationFormat"] = $project->getType();
//        $report["targetLabels"] = $targetLabels;
//      }*/
//
//
//      return new Response($this->renderView('listing/evalreport.twig', $report));
//    }
//
//    return $this->redirectToRoute('admin');
//  }

//  /**
//   * Returns the evaluation report in HTML.
//   *
//   * @Route("/api/annotation/report", name="api_annotation_report")
//   * @Method("GET")
//   * @param Request $request
//   * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
//   * @deprecated
//   */
//  public
//  function annotationReport(Request $request)
//  {
//    $projectId = self::getProjectSession("id");
//    $userId = self::getUserSession("userid");
//
//    if ($projectId) {
//      /** @var AnnotationRepository $annotation_repository */
//      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
//      $report = $annotation_repository->getEvalReportParams($projectId, self::getUserSession("userid"));
//
//      // this is the link used to share the report
//      $report["link"] = $params["link"] = $this->container->getParameter('hostname') . "/share/" . $projectId . "/" . $userId;
//
//
//      $report["evaluationFormat"] = self::getProjectSession("type");
//      //todo add targets
//
//      return new Response($this->renderView('listing/evalreport.twig', $report));
//    }
//
//    return $this->redirectToRoute('admin');
//  }


  /**
   * returns all imported corpus for a project
   * @Route("/api/corpus/report", name="api_corpus_report", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiCorpusReport()
  {
    // used by the GUI

    $id = self::getProjectSession("id");
    if ($id != null) {
      /*$dql = "SELECT c.id, c.filename, s.type, count(s.type) as mycount, s.updated
        FROM AppBundle:Sentence AS s
        INNER JOIN AppBundle:Corpus AS c
        WHERE s.corpus=c.id and s.project=:pid
        GROUP BY c.id,s.type,c.filename";**/

      //Claudio: changed the query to solve the problem with Mysql >= 5.7 this is incompatible with sql_mode=only_full_group_by

      $dql = "SELECT c.id, c.filename,s.type,s.updated,count(c.id) AS mycount
              FROM AppBundle:Sentence AS s INNER JOIN AppBundle:Corpus AS c
              WHERE s.corpus=c.id and s.project=:pid 
              GROUP BY c.id,c.filename,s.type,s.updated";


      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('pid', $id);

      $items = $query->getResult();
      $json = $this->get('serializer')->serialize($items, 'json');

      $response = new Response($json);
      return $response;
    }
    return null;
  }


  /**
   * Invoked to accept or decline to work on a project.
   *
   * @Route("/api/role/reply", name="api_role_reply")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function replyInvitation(Request $request)
  {
    $message = 'Invitation failed.';
    $userid = self::getUserSession("userid");

    $params = $request->query->all();
    $parout = array();
    $redirectPage = 'dashboard';
    if (self::getProjectSession("id")) {
      $redirectPage = 'admin';
    }
    if (array_key_exists('view', $params)) {
      $parout{"view"} = $params["view"];
    }

    if (array_key_exists('hash', $params) && !empty($params["hash"])) {
      $em = $this->getDoctrine()->getManager();
      /** @var RoleRepository $repository */
      $repository = $em->getRepository('AppBundle:Role');
      /** @var Role $role */
      $role = $repository->findOneBy(array("hash" => $params["hash"]));
      if ($role != null) {
        if ($role->getStatus() == "pending") {
          if ($params["action"] == "accepted" || $params["action"] == "declined") {
            $repository->updateRole($role->getCreatedBy(), $role->getUser(), $role->getProject(), $role->getRole(), $params["action"]);
            /*
                $role->setStatus($params["action"]);
                $role->setActivated(new \DateTime());
                $em->persist($role);
                $em->flush();
            */

            if ($params["action"] == "accepted") {
              $message = 'Thanks to have accepted our invitation.';
            } else if ($params["action"] == "declined") {
              $message = 'We\'re sorry that you have declined our invitation.';
            }


            if ($userid != null && $role->getUser() == $userid) {
              $this->addFlash("info", $message);
              return $this->redirectToRoute('dashboard');
            } else {
              if ($params["action"] == "accepted") {
                $message .= " Please login to work on the project.";
              }
              $this->addFlash("info", $message);
              return new Response($this->renderView('default/sign-in.twig'));
            }
          } else {
            //check other values
            $message = 'This request is not valid.';
          }
        } else {
          $message = 'The invitation failed because its status was not pending.';
        }
      } else {
        $message = "This request is not valid or it has been already done.";
      }
    }
    $this->addFlash("warning", $message);
    return $this->redirectToRoute($redirectPage, $parout);
  }

  /**
   * get roles for a project
   * @Route("/api/project/roles", name="api_project_roles", defaults={"_format" = "json"})
   * @return Response
   */
  public
  function apiProjectRoles(Request $request)
  {
    //todo check if used

    $pid = $request->query->get("pid");
    if ($pid > 0) {
      $dql = "SELECT u.username,u.email,r.role
        FROM AppBundle:Role AS r
        INNER JOIN AppBundle:User AS u
        WHERE r.user = u.id AND r.project = :pid AND r.role != :role";

      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('pid', $pid);
      $query->setParameter('role', "owner");

      $items = $query->getResult();
      $json = $this->get('serializer')->serialize(array('roles' => $items), 'json');

      /*
      $count = $role_repository->createQueryBuilder('u');
      $count->select('count(u.role)');
      $count = $count->getQuery()->getSingleScalarResult();

      $json = $this->get('serializer')->serialize(array('roles' => $items, 'total' => $count), 'json');
      */
      $response = new Response($json);
      return $response;
    }
    return new Response("[]");
  }

  /**
   * get roles for a project
   * @Route("/api/project/", name="getEvaluationFormat", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function getEvaluationFormat(Request $request)
  {
    $json = "[]";
    if ($request->query->has("id")) {


      $em = $this->getDoctrine()->getManager();
      $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $request->query->get("id")));
      $eval = array();
      $eval["type"] = $project->getType();
      $eval["requiref"] = $project->getRequiref();
      $eval["ranges"] = $project->getRanges();
      //$eval["ciao"] = json_decode($project->getRanges(), true);
    }


    //
    return new Response(json_encode($json), 200, array('Content-Type' => 'application/json'));
  }

  /**
   * get roles for a project
   * @Route("/api/project/single/document", name="api_project_single_document", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function apiProjectSingleDocument(Request $request)
  {
    $json = "[]";
    if ($request->query->has("id")) {
      //
      $em = $this->getDoctrine()->getManager();
      $document = $em->getRepository('AppBundle:Document')->findOneBy(array("id" => $request->query->get("id")));
      $json = $document->getData();
    }
    //

    //
    return new Response(json_encode($json), 200, array('Content-Type' => 'application/json'));
  }

  /**
   * get roles for a project
   * @Route("/api/project/document", name="api_project_document", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function apiProjectDocument(Request $request)
  {
    $json = "[]";
    if ($request->query->has("type")) {
      $dql = "SELECT d.id, d.format, d.url, d.used, d.data
        FROM AppBundle:Document AS d
        WHERE d.type=:type
        ORDER BY d.format ASC, d.used DESC";

      /** @var Query $query */
      $query = $this->getDoctrine()->getManager()->createQuery($dql);
      $query->setParameter('type', $request->query->get("type"));

      $items = $query->getResult();
      if (count($items) > 0) {
        $json = $this->get('serializer')->serialize($items, 'json');
      }
    }
    return new Response($json);
  }

  /**
   * now you can easily looks for users that you have already been invited to your projects or who invited you to their projects (updating the previous version that gets the list of registered users matching by substring of user email or MT-EQuAl username or role).
   * In this way a user can invite the rest of users available in the system if and only s/he knows the exact her/his MT-EQuAl username (and role optionally) or email and by listing of the suggested usernames joint to him by previously invitations.
   *
   * @Route("/api/user/find", name="api_user_find", defaults={"_format" = "json"})
   * @return Response
   */
  public function apiFindUser(Request $request)
  {
    $userid = self::getUserSession("userid");

    if ($userid != null) {
      if ($request->query->has("search") || $request->query->has("role")) {
        $search = $request->query->get("search");
        $role = $request->query->get("role");
        $searchTerm = ($search == null || $search == "") ? "" : "(u.username LIKE :searchlike
            OR u.email LIKE :searchlike)";
        $roleTerm = ($role == null || $role == "") ? "" : "u.roles LIKE :rolelike";

        if ($searchTerm != "" && $roleTerm != "") {
          $roleTerm = " AND " . $roleTerm;
        }
        $dql = "SELECT DISTINCT u.id,u.username,u.email,u.firstname,u.lastname
            FROM AppBundle:User as u
            INNER JOIN AppBundle:Role AS r
            WHERE (u.id=r.user OR u.id=r.created_by OR u.email=:search) AND (r.user=:userid OR r.created_by=:userid) AND u.id!=:userid
              AND u.active = 1 AND " . $searchTerm . $roleTerm;

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('userid', $userid);
        $query->setParameter('search', trim($search));

        if ($search != null) {
          $query->setParameter('searchlike', '%' . trim($search) . '%');
        }
        if ($role != null) {
          $query->setParameter('rolelike', '%' . $role . '%');
        }

        $items = $query->getResult();
        $json = $this->get('serializer')->serialize($items, 'json');

        return new Response($json);
      }
    }
    return new Response("{}");
  }

  /**
   * Returns the favourite users (invited in other projects)
   *
   * @Route("/api/user/getFavourites", name="getFavourites", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @author giuliano
   * @return Response
   * @deprecated
   *
   */
  public function getFavourites(Request $request)
  {

    if ($request->query->has("role")) {

      $userid = self::getUserSession("userid");
      if ($userid != null) {

        //SELECT *, GROUP_CONCAT(DISTINCT( r.role) SEPARATOR ', ') FROM `role` as r INNER join user as u WHERE r.created_by=1 and r.user!=1 and u.id = r.user GROUP by u.id
        /*$dql = "SELECT u.id,u.username,u.email,u.firstname,u.lastname,r.role,GROUP_CONCAT(DISTINCT r.role SEPARATOR ', ') AS roles
              FROM AppBundle:Role AS r
              INNER JOIN AppBundle:User AS u
              WHERE r.created_by=:userid AND r.user!=:userid AND u.id = r.user AND u.active = 1
              GROUP by u.id,r.role";*/

        $dql = "SELECT u.id,u.username,u.email,u.firstname,u.lastname,r.role
            FROM AppBundle:Role AS r
            INNER JOIN AppBundle:User AS u
            WHERE r.created_by=:userid AND r.user!=:userid AND u.id = r.user AND u.active = 1 AND r.role=:userRole
            GROUP by u.id,r.role";

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('userid', $userid);
        $query->setParameter('userRole', $request->query->get("role"));

        $items = $query->getResult();

        $json = $this->get('serializer')->serialize($items, 'json');

        return new Response($json);
      }
    }
    return new Response($this->get('serializer')->serialize([], 'json'));
  }

  /**
   * now you can easily looks for users that you have already been invited to your projects or who invited you to their projects (updating the previous version that gets the list of registered users matching by substring of user email or MT-EQuAl username or role).
   * In this way a user can invite the rest of users available in the system if and only s/he knows the exact her/his MT-EQuAl username (and role optionally) or email and by listing of the suggested usernames joint to him by previously invitations.
   *
   * @author giuliano
   * @Route("/api/user/myFindUser", name="myFindUser", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function myFindUser(Request $request)
  {
    $userID = self::getUserSession("userid");

    if ($userID != null) {
      $projectID = self::getProjectSession("id");


      if ($projectID != null) {


        if ($request->query->has("search")) {
          $search = $request->query->get("search");


          $dql1 = "SELECT u.id, u.username, u.email, u.firstname, u.lastname
            FROM AppBundle:User as u           
            WHERE u.username=:search OR u.email=:search";


          /** @var Query $query */
          $query1 = $this->getDoctrine()->getManager()->createQuery($dql1);
          $query1->setParameter('search', trim($search));

          $users = $query1->getResult();

          //todo investigate why this is not used
          foreach ($users as $user) {


            $dql2 = "SELECT r.user, r.project, r.role, r.created_by, r.status
            FROM AppBundle:Role as r           
            WHERE r.user=:aUser and r.project=:aProject";
            $query2 = $this->getDoctrine()->getManager()->createQuery($dql2);

            $query2->setParameter('aUser', $user["id"]);
            $query2->setParameter('aProject', $projectID);

            $foundUsers = $query2->getResult();
            $foundUsersCount = count($foundUsers);

          }


          $json = $this->get('serializer')->serialize($users, 'json');

          return new Response($json);
        }
      }
    }
    return new Response("{}");
  }

  /**
   * remove all evaluations done by evaluators in a project
   * @Route("/api/evaluations/delete", name="api_evaluations_delete")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function apiDeleteEvaluations(Request $request)
  {

    /*if ($request->query->has("userid")) {
      $targetUserID = $request->query->get("userid");
    }*/
    //todo replace the session
    $userID = self::getUserSession("userid");
    $projectID = self::getProjectSession("id");


    $messageType = "danger";
    $message = "The evaluations cannot be deleted.";
    if ($userID != null && $projectID != null) {
      /** @var ProjectRepository $project_repository */
      /*$project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $permission = $project_repository->can("delete", $projectID, $userID);
      if ($permission == 1) {

        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $annotation_repository->deleteAllEvaluations($projectID);

        $messagetype = "success";
        $message = "All evaluations have been deleted!";
      } else {
        $message = "Sorry by you don't have enough privileges!";
        //$message = "user_without_privileges";
      }*/

      /** @var ProgressRepository $repository_progress */
      $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progressCheckCorpus = $repository_progress->findOneBy(array("project" => $projectID, "task" => "check_corpus"));

      if ($progressCheckCorpus) {
        if ($progressCheckCorpus->getCompleted()) {
          $messageType = "warning";
          $message = "The evaluations cannot be deleted, the evaluation must be stopped before.";
          $this->addFlash($messageType, $message);
          return $this->redirectToRoute('admin', array("pid" => $projectID, "view" => "settings.delete"));
        }
      }

      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
      $roles = $roleRepository->findBy(array('project' => $projectID, 'user' => $userID, 'role' => 'admin'));
      if (count($roles) > 0) {

        $annotation_repository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        $annotation_repository->deleteAllEvaluations($projectID);

        $messageType = "success";
        $message = "All evaluations have been deleted.";
      }
    }
    $this->addFlash($messageType, $message);
    return $this->redirectToRoute('admin', array("pid" => $projectID, "view" => "settings.delete"));
  }


  /**
   * TODO: NOT USED!!
   * assign a role to a user for a project
   * @Route("/api/user/info", name="api_user_info")
   * @return Response
   */
  public
  function apiUserInfo(Request $request)
  {
    $userid = self::getUserSession("userid");
    $userInfo = array();
    if ($userid != null) {
      /** @var UserRepository $user_repository */
      $user_repository = $this->getDoctrine()->getRepository("AppBundle:User");

    }
    return new Response(
      json_encode($userInfo),
      200,
      array('Content-Type' => 'application/json')
    );
  }

  /**
   * TODO: NOT USED!!
   * assign a role to a user for a project
   * @Route("/api/project/info", name="api_project_info")
   * @return Response
   */
  public
  function apiInfoProject(Request $request)
  {
    $project_data = array();
    $projectid = self::getProjectSession("id");
    if ($projectid != null) {
      /** @var ProjectRepository $project_repository */
      $project_repository = $this->getDoctrine()->getRepository("AppBundle:Project");
      /** @var Project $project */
      $project_data = $project_repository->getProject($projectid);
      if ($project_data == null) {
        $project_data["id"] = $projectid;
      }
    }
    return new Response(
      json_encode($project_data),
      200,
      array('Content-Type' => 'application/json')
    );
  }

  /**
   * get an evaluation info: if there is at least a review, a comment, evals distribution and the agreement
   * @Route("/evaluation/get", name="get_evaluation")
   * @Method("GET")
   */
  public
  function getEvaluation(Request $request)
  {

    // todo: claudio check this
    $html = "";
    $params = $request->query->all();
    //todo deprecate etasks
    if (array_key_exists("num", $params) &&
      array_key_exists("review", $this->get("session")->get("project")["etasks"])
    ) {

      /** @var  AgreementRepository $agreement_repository */
      $agreement_repository = $this->getDoctrine()->getRepository('AppBundle:Agreement');
      //update Agreement
      if (array_key_exists("oid", $params) && $params["oid"] != null) {
        $agreement_repository->updateAgreement($params["oid"], $this->get("session")->get("project")["id"]);
      }

      /** @var AnnotationRepository $annotation_repository */
      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $evals = $annotation_repository->getEvaluations($params["num"]);
      $revuser = 0;
      $ranges = $this->get("session")->get("project")["ranges"];
      if (array_key_exists("0", $evals)) {
        $revuser += $evals["0"][1];
        $html .= '<td class="cell-right">' . $evals["0"][0] . '</td>';
      } else {
        $html .= '<td class="cell-right">0</td>';
      }
      $decisionOptions = array(0 => '',
        1 => 'Yes',
        2 => 'No',
        3 => 'N/A');

      foreach (json_decode($ranges, TRUE) as $evalType) {
        $num = 0;
        if ($this->get("session")->get("project")["type"] == "tree") {
          $num = "";
          $listElem = array();
          foreach ($evals as $eval => $evalInfo) {
            $revuser += $evalInfo[1];

            $ev = substr($eval, (intval($evalType["val"]) - 1), 1);
            if ($ev != 0) {
              $listElem{$ev} += $evalInfo[0];
            }
          }
          foreach ($listElem as $eval => $cnt) {
            $num .= "$cnt<sup><b>" . $decisionOptions{$eval} . "</b></sup> ";
          }
          if ($num == "") {
            $num = "--";
          }
        } else {
          if (array_key_exists($evalType["val"], $evals)) {
            $revuser += $evals[$evalType["val"]][1];
            $num = $evals[$evalType["val"]][0];
          }
        }
        $html .= "<td class='cell-right'>$num</td>";
      }

      $data = $agreement_repository->getSentenceAgreement($params["num"]);
      $text = "";
      $agreement = "--";
      foreach ($data as $value) {
        $text = $value["text"];
        $agreement = $value["agreement"];
      }

      /** @var CommentRepository $comment_repository */
      $comment_repository = $this->getDoctrine()->getRepository('AppBundle:Comment');
      $commentCount = $comment_repository->getCommentCount($params["num"]);
      $commentInfo = "";
      if ($commentCount > 0) {
        $commentInfo = ' <span class="pull-right"><i class="fa fa-comments-o"></i>' . $commentCount . '</span>';
      }

      //if exist at least a revision
      $revInfo = "";
      if ($revuser > 0) {
        $revInfo = "<div style='padding: 0; display: inline; position: relative; left: -8px;'><i class='fa fa-pencil bg-primary' style='padding: 2px' title='reviewed'></i></div> ";
      } else {
        $userId = $this->get("session")->get("mysession")["userid"];
        if ($userId && $annotation_repository->isViewedEvaluation($params["num"], $userId)) {
          $revInfo = "<div style='padding: 0; display: inline; position: relative; left: -8px;'><i class='fa fa-eye' style='padding: 2px; background: #BA494F; color: #fff' title='viewed'></i></div> ";
        }
      }
      $html = "<td>$revInfo$text$commentInfo</td>$html";


      if ($agreement == "1.000") {
        $agreement = "1";
      }
      $html .= '<td class="cell-right"><label>' . $agreement . '</label></td>';
    }
    return new Response($html);
  }

  /**
   * Saves an evaluation
   *
   * @Route("/evaluation/save_evaluation", name="save_evaluation")
   * @Method("POST");
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function saveEvaluation(Request $request)
  {
    $params = $request->request->all();
    $status = "error";
    $finalEval = "-1";
    $message = "The evaluation cannot be saved";
    $userid = $this->get("session")->get("mysession")["userid"];

    //the project id must be taken from the sentence to annotate
    //the user can open multiple projects, the last one sets the project id
    $projectid = $this->get("session")->get("project")["id"];

    //todo investigate why there is authentication
    /*if (!$this->isCsrfTokenValid('authenticate', $params["CsrfToken"])) {
      // invalid token
      return new Response($this->renderView('exception/error-500.twig'));
    }*/

    if ($userid != null && $projectid != null &&
      array_key_exists("id", $params) &&
      array_key_exists("num", $params) &&
      array_key_exists("eval", $params)
    ) {
      $em = $this->getDoctrine()->getManager();
      /** @var User $user */
      $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userid));

      /** @var Sentence $sentence */
      $sentence = $em->getRepository('AppBundle:Sentence')->findOneBy(array("num" => $params["num"]));

      /** @var Project $project */
      $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $sentence->getProject()));


      if ($user && $project && $sentence) {
        //\AppBundle\Util\Log::write("user: " . $user->getUsername());

        /** @var AnnotationRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

        //todo investigate if this is need? It causes a bug to the tree
        //remove don't know evaluation
        //Claudio: removed to allow to save 0 for a single target/output for the tree
        //todo investigate if it creates problems to the other evaluation format
        /*

        $dontknows = $repository->findBy(
          array("output_id" => $params["id"], "project" => $project, "eval" => Annotation::DONTKNOW_VALUE, "user" => $user));
        if (count($dontknows) > 0) {
          foreach ($dontknows as $dontknow) {
            $em->remove($dontknow);
          }
          $em->flush();
        }
        */
        //save the current evaluation
        /** @var Annotation $annotation */
        $annotation = $repository->findOneBy(
          array("num" => $params["num"], "user" => $user));

        if ($annotation == null) {

          $message = "A new evaluation has been saved";
          $annotation = new Annotation();
          $annotation->setOutputId($params["id"]);
          $annotation->setNum(intval($params["num"]));
          $annotation->setUser($user);
          $annotation->setProject($project);
          $annotation->setEval(trim($params["eval"]));
          $em->persist($annotation);
          $em->flush();
        } else {
          if ($annotation->getEval() == trim($params["eval"])) {

            $message = "The evaluation has not been changed";
            //$em->remove($annotation);
          } else {

            $message = "An existing evaluation has been updated";
            $annotation->setEval(trim($params["eval"]));
            $em->persist($annotation);
            $em->flush();
          }
        }

        $finalEval = $annotation->getEval();
        //reset progress
        $repository->unconfirmEvaluation($params["id"], $projectid, $user);

        $status = "OK";
      }
    }
    //
    //return new Response(json_encode(array("message" => $message)), 200, array('Content-Type' => 'application/json'));
    $a = array();
    $a["status"] = $status;
    $a["message"] = $message;
    $a["eval"] = $finalEval;

    return new Response(json_encode($a));
  }

  /**
   * send an evaluation
   * @Route("/evaluation/resetrev", name="reset_rev_evaluation")
   * @Method("GET")
   */
  public
  function resetRevEvaluation(Request $request)
  {
    $params = $request->query->all();
    $message = "error";
    $userid = $this->get("session")->get("mysession")["userid"];
    $projectid = $this->get("session")->get("project")["id"];
    if ($userid != null && $projectid != null &&
      array_key_exists("num", $params)
    ) {

      /** @var AnnotationRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      if ($repository->resetReview($params["num"])) {
        $message = "OK";
      }
    }
    return new Response(
      json_encode(array("message" => $message))
    );
  }

  /**
   * Saves a review
   *
   * @Route("/evaluation/save_rev_evaluation", name="save_rev_evaluation")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public
  function saveRevEvaluation(Request $request)
  {
    //todo investigate why there is no authentication

    $params = $request->query->all();
    $message = "error";
    $userid = $this->get("session")->get("mysession")["userid"];
    $projectid = $this->get("session")->get("project")["id"];


    if ($userid != null && $projectid != null &&
      array_key_exists("id", $params) &&
      array_key_exists("num", $params) &&
      array_key_exists("euser", $params) &&
      array_key_exists("eval", $params)
    ) {
      $eval = trim($params["eval"]);

      $em = $this->getDoctrine()->getManager();
      /** @var User $user */
      $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $userid));


      /** @var Project $project */
      $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectid));


      if ($user) {

        /** @var AnnotationRepository $repository */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
        //disable evaluation done by evaluator
        /** @var Annotation $evaluator_annotation */

        $evaluator_annotation = $repository->findOneBy(array("num" => $params["num"], "user" => $params["euser"], "revuser" => 0));


        if ($evaluator_annotation) {


          //save the current evaluation
          /** @var Annotation $reviewer_annotation */
          $reviewer_annotation = $repository->findOneBy(array("num" => $params["num"], "user" => $params["euser"], "active" => 1));

          if ($reviewer_annotation == null || $reviewer_annotation == $evaluator_annotation) {
            /** @var User $euser */
            $euser = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $params["euser"]));


            if ($euser) {
              $evaluator_annotation->setActive(false);
              $em->persist($evaluator_annotation);

              $annotation = new Annotation();
              $annotation->setOutputId($params["id"]);
              $annotation->setNum(intval($params["num"]));
              $annotation->setUser($euser);
              $annotation->setProject($project);
              $annotation->setEval($eval);
              $annotation->setRevuser($userid);
              $annotation->setRevtime(new \DateTime());
              $annotation->setConfirmed($evaluator_annotation->getConfirmed());
              $em->persist($annotation);

            }
          } else {


            if ($reviewer_annotation->getEval() == $eval || preg_match('/^00+$/', $eval)) {
              $em->remove($reviewer_annotation);

              $evaluator_annotation->setActive(true);
              $em->persist($evaluator_annotation);

            } else {
              $reviewer_annotation->setEval($eval);
              $reviewer_annotation->setRevtime(new \DateTime());
              $em->persist($reviewer_annotation);

            }
          }
          $em->flush();

          //update Agreement
          /** @var  AgreementRepository $repository_agreement */
          $repository_agreement = $em->getRepository('AppBundle:Agreement');
          $repository_agreement->updateAgreement($params["id"], $projectid);

          $message = "OK";
        }

      }
    }

    //todo add the changed values

    //return new Response(json_encode(array("message" => $message)), 200, array('Content-Type' => 'application/json'));
    $a = array();
    $a["message"] = $message;

    return new Response(json_encode($a));
  }


  /**
   * returns a list with the latest comments
   * @Route("/api/commentOLD", name="api_commentOLD")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function getComment(Request $request)
  {
    $userId = self::getUserSession("userid");
    if ($userId != null) {
      if (!in_array("admin", self::getUserSession("roles")) ||
        !in_array("reviewer", self::getUserSession("roles"))
      ) {
        $userId = null;
      }
      $projectId = self::getProjectSession("id");

      if ($projectId != null) {
        $obj = $request->query->get("obj");
        $refId = $request->query->get("refid");
        $showall = $request->query->get("showall");
        if (!empty($obj) && !empty($refId)) {
          /** @var RoleRepository $repository_role */
          $repository_role = $this->getDoctrine()->getRepository("AppBundle:Role");

          $evaluators = $repository_role->getAnonymousEvaluators($projectId);

          //
          //
          //
          //

          /** @var CommentRepository $repository_comment */
          $repository_comment = $this->getDoctrine()->getRepository("AppBundle:Comment");

          $comments = $repository_comment->lastComment($projectId, $obj, $refId, $userId, $showall);

          // add the anonymous label
          for ($i = 0; $i < count($comments); $i++) {

            //
            //
            //
            //
            $comments[$i]["anonymous"] = $evaluators[$comments[$i]["userid"]];
          }
          //
          return new Response(
            json_encode($comments), 200, array('Content-Type' => 'application/json')
          );
        }
      }
    }

    return new Response(json_encode(array("success" => false)));
  }

  /**
   * Returns a current value of the kappa score (considering the reviewing)
   *
   * @Route("/api/kappa", name="api_kappa")
   * @param Request $request
   * @return Response
   * @deprecated
   */
  public function getKappa(Request $request)
  {
    /** @var AnnotationRepository $repository_annotation */
    $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
    $k = $repository_annotation->calculateKappaQuality(self::getProjectSession("id"), false);
    if ($k != 0) {
      $k = round($k, 3);
    }
    return new Response(json_encode(array("kappa" => $k)));
  }
}

?>