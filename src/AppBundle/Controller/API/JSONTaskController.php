<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 18:48
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
//use AppBundle\Controller\API\APIController;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class JSONTaskController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONTaskController extends JSONController
{
  /**
   * Shows the task status
   *
   * @Route("/api/task/show", name="api_task_show")
   * @param Request $request
   * @return Response
   */
  public function apiTaskShow(Request $request)
  {
    //Implemented by Claudio

    $data = self::initJSONArray("Shows the task status");
    $data["header"]["status"] = "200";

    $tasks = array();
    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");

      $user = self::findAuthorizedUser($request);
      if ($user) {
        $task["user"] = $user->getUsername();
        if ($projectId != null) {
          $task = $request->query->get("task");
          $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
          $t = array();
          if ($task) {

            $t["task"] = $task;
            $t["id"] = $projectId;

            if ($repository_progress->isValidTask($task)) {
              $progress = $repository_progress->getProgress($task, $projectId, $user->getId());


              if ($progress) {
                $t["completed"] = $progress->getCompleted();
                $t["disabled"] = $progress->getDisabled();
                $t["modified"] = $progress->getModified();
                $t["duedate"] = $progress->getDueDate();
                $t["username"] = $user->getUsername();
                $t["user"] = $progress->getUser();
              }

              //$data["enabletasks"] = join("' and '", $repository_progress->enableTasks($projectSession["id"], $userid));

              //todo check this part
              $t["enabletasks"] = join("' and '", $repository_progress->dependencyTasks($task));
            }
            array_push($tasks, $t);
            $data["result"]["tasks"] = $tasks;
          } else {
            //$data["header"]["status"] = "400";
            //$data["header"]["description"] = "The parameter 'task' is missing.";
            $report = $repository_progress->taskReport($projectId, $user->getId());
            if ($report) {
              $data["result"] = $report;
            }
          }
        }
      }
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Sets the due date of the specified task
   *
   * @Route("/api/task/dueBy", name="api_dueBy_task")
   * @param Request $request
   * @return Response
   */
  public function apiTaskDueBy(Request $request)
  {
    //Implemented by Claudio

    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Failed to set the task incomplete!";
    //
    $user = self::findAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("id")) {
        $projectId = $request->query->get("id");
        $data["result"]["id"] = $projectId;

        if ($request->query->has("task")) {
          $task = $request->query->get("task");

          if ($request->query->has("date")) {
            $date = $request->query->get("date");
            if (!empty($date)) {
              $date .= " 23:59:59";
            } else {
              $date = null;
            }
            $data["result"]["date"] = $date;
            $data["result"]["task"] = $task;
            //todo: the exception is not captured
            try {
              $d = new \DateTime($date, new \DateTimeZone('UTC'));
            } catch (Exception $e) {
              $data["header"]["status"] = "500";
              $data["header"]["description"] = $e;
              return new Response(json_encode($data, JSON_PRETTY_PRINT));
            }
            $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
            $progress = $repository_progress->findOneBy(array("task" => $task));
            $progress1 = $repository_progress->setDueDate($progress->getId(), $date);
            $data["result"]["progress"] = $progress1;

            if ($progress1) {
              $date1 = $progress1->getDuedate()->format('Y-m-d H:i:s');
              if ($date1 != null) {
                $data["header"]["status"] = "200";
                $data["result"]["date"] = $date1;
                $data["header"]["description"] = "Due date set to " . $date1;
              }
            } else {
              $data["header"]["description"] = "Updating due date failed!";
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'date' is missing.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'task' is missing.";
        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Sets the specified task to incomplete
   *
   * @Route("/api/task/undone", name="task/undone")
   * @param Request $request
   * @return Response
   */
  public function apiTaskUndone(Request $request)
  {
    //Implemented by Claudio

    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Failed to set the task incomplete!";
    //
    $user = self::findAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("id")) {
        $projectid = $request->query->get("id");
        //
        $task = $request->query->get("task");
        //

        $repository_annotation = $this->getDoctrine()->getRepository("AppBundle:Annotation");
        if ($task == ProgressRepository::EVALFORMAT_TASK && $repository_annotation->countEvaluation($projectid) > 0) {
          $data["header"]["status"] = "403";
          $data["header"]["description"] = "Before to reset this task you must delete ALL EVALUATION";
        } else {

          $userTo = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array("id" => $user->getId()));
          //
          if ($userTo) {

            //todo investigate why undoneTask is missing
            $progress = self::undoneTask($task, $projectid, $userTo, $user->getId());
            if ($progress && !$progress->getCompleted()) {

              $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
              $repository_progress->enableTasks($projectid, $user->getId());

              $data["header"]["status"] = "200";
              $data["header"]["description"] = "The task has been reset correctly!";
            }
          }
          //
        }


        //
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Sets the specified task to complete
   *
   * @Route("/api/task/done", name="task/done")
   * @param Request $request
   * @return Response
   */
  public function apiTaskDone(Request $request)
  {
    //Implemented by Claudio

    $data["header"]["status"] = "500";
    $data["header"]["description"] = "Failed to set the task complete!";

    $user = self::findAuthorizedUser($request);
    if ($user) {
      if ($request->query->has("id")) {
        $projectid = $request->query->get("id");
        //
        $task = $request->query->get("task");
        //
        $repository_progress = $this->getDoctrine()->getRepository("AppBundle:Progress");
        $isDisabledTask = $repository_progress->findBy(array("project" => $projectid, "task" => $task, "disabled" => 0));
        //
        if ($repository_progress->isValidTask($task) && $isDisabledTask != null) {
          //
          if ($task == ProgressRepository::IMPORTCORPUS_TASK) {
            //
            $repository_sentence = $this->getDoctrine()->getRepository("AppBundle:Sentence");

            //check if at least a corpus was uploaded correctly
            if ($repository_sentence->countTUs($projectid) == 0) {
              $data["header"]["description"] = "This task cannot be completed without a valid corpus!";
              return new Response(json_encode($data, JSON_PRETTY_PRINT));
            }
          }
          //
          $progress = self::doneTask($task, $projectid, $user->getId());
          if ($progress->getCompleted()) {
            $data["header"]["status"] = "200";
            $data["header"]["description"] = "Task set to complete!";
          }
          //
          //add in the session the list of the enable tasks
          //todo deprecate etasks
          //$this->setProjectSession("etasks", $repository_progress->enableTasks($projectid, $user->getId()));
          $repository_progress->enableTasks($projectid, $user->getId());

          //
          //$data["header"]["compleded"] = $progress->getCompleted();
          $data["header"]["task"] = $task;
          // $data["header"]["project"] = $progress->getProject();
          $data["header"]["user"] = $user->getId();
          // $data["header"]["comp"] = $progress->getCompleted();
          // $data["header"]["dep"] = $repository_progress->getProgress($task, $projectid, $user)->getTask();

          //$data["header"]["valid"] = $repository_progress->isValidTask($task);
        }
        //
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }


    $data["header"]["msg"] = "hello!";
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
}