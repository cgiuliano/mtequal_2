<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 18:49
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class JSONAdminController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONAdminController extends JSONController
{
  /**
   * @Route("/api/admin/show", name="api_admin_stats")
   * @param Request $request
   * @return Response
   */
  public function adminShow(Request $request)
  {
    //Implemented by Claudio
    $data = self::initJSONArray("Shows useful information on this MT-EQuAl instance");
    $data["header"]["status"] = "200";

    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      if ($request->headers->get("X-Api-Key") == $this->container->getParameter("secret")) {
        $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');


        $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
        $users = $userRepository->findBy(array("active" => 1), array('username' => 'DESC'));

        $p = array();
        /** @var User $user */
        foreach ($users as $user) {
          $u = array("id" => $user->getId(),
            "username" => $user->getUsername(),
            "firstname" => $user->getFirstname(),
            "lastname" => $user->getLastname(),
            "affiliation" => $user->getAffiliation(),
            "email" => $user->getEmail(),
            "roles" => $user->getRoles(),
            "registered" => $user->getRegistered()->format('l, Y-m-d H:i:s e'),
            "logged" => $user->getLogged()->format('l, Y-m-d H:i:s e'),
          );
          array_push($p, $u);
        }
        $data["header"]["status"] = "200";
        $data["result"]["users"] = $p;

        $data["result"]["projects"] = $projectRepository->getProjects();
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/admin/remove", name="api_admin_remove")
   * @param Request $request
   * @return Response
   */
  public function adminRemove(Request $request)
  {
    //Implemented by Claudio

    $data = self::initJSONArray("Remove permanently a user or a project from the database");
    $data["header"]["status"] = "200";

    if ($request->headers != null && $request->headers->has("X-Api-Key")) {
      if ($request->headers->get("X-Api-Key") == $this->container->getParameter("secret")) {
        if ($request->query->has("type")) {
          $type = $request->query->get("type");
          if ($request->query->has("id") || $request->query->has("name")) {
            $em = $this->getDoctrine()->getManager();
            /** @var RoleRepository $repositoryRole */
            $repositoryRole = $em->getRepository('AppBundle:Role');
            if ($type == "user") {
              /** @var User $user */
              $user = null;
              if ($request->query->has("id")) {
                $user = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $request->query->get("id")));
              } else if ($request->query->has("name")) {
                $user = $em->getRepository('AppBundle:User')->findOneBy(array("username" => $request->query->get("name")));
              }
              if ($user) {
                $username = $user->getUsername();
                try {
                  $roles = $repositoryRole->findBy(array("user" => $user->getId()));
                  /** @var Role $userRole */
                  foreach ($roles as $userRole) {
                    $em->getRepository('AppBundle:Progress')->deleteAllProgress($userRole->getProject(), $userRole->getUser());
                    $em->getRepository('AppBundle:Annotation')->deleteEvaluations($userRole->getProject(), $userRole->getUser(), true);

                    $em->remove($userRole);
                    $em->flush();
                  }
                  //$repositoryRole->removeInvitation($user->getId());
                  $em->remove($user);
                  $em->flush();

                  $data["header"]["description"] = "Removed an existing user.";
                  $data["result"]["type"] = $type;
                  $data["result"]["username"] = $username;
                } catch (\Exception $e) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                }
              } else {
                $data["header"]["status"] = "400";
                $data["header"]["description"] = "The user was not found.";
              }
            } else if ($type == "project") {
              /** @var Project $project */
              if ($request->query->has("id")) {
                $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $request->query->get("id")));
              } else if ($request->query->has("name")) {
                $project = $em->getRepository('AppBundle:Project')->findOneBy(array("name" => $request->query->get("name")));
              }
              if ($project) {
                $projectName = $project->getName();
                try {
                  $roles = $repositoryRole->findBy(array("project" => $project->getId()));
                  /** @var Role $userRole */
                  foreach ($roles as $userRole) {
                    $em->getRepository('AppBundle:Progress')->deleteAllProgress($userRole->getProject());
                    $em->getRepository('AppBundle:Annotation')->deleteEvaluations($userRole->getProject(), $userRole->getUser(), false);
                    $em->remove($userRole);
                    $em->flush();
                  }
                  if (!$em->getRepository('AppBundle:Corpus')->deleteAllCorpus($userRole->getProject())) {
                    $data["header"]["status"] = "500";
                    $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                  }
                  $em->remove($project);
                  $em->flush();

                  $data["header"]["description"] = "Removed an existing project.";
                  $data["result"]["type"] = $type;
                  $data["result"]["name"] = $projectName;
                } catch (\Exception $e) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] .= "Some errors occurred deleting project corpus.";
                }
              } else {
                $data["header"]["status"] = "400";
                $data["header"]["description"] = "The project was not found.";
              }
            }
          } else {
            $data["header"]["status"] = "400";
            $data["header"]["description"] = "The parameter 'id' or 'name' is missing.";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'type' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
}