<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 16:50
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;


use AppBundle\Entity\User;

use AppBundle\Repository\AgreementRepository;

use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserException;
use AppBundle\Repository\UserRepository;
use AppBundle\Util\ParameterException;
use Doctrine\ORM\Query;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class JSONUserController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONUserController extends JSONController
{

  /**
   * @author giuliano
   * @Route("/api/user/review", name="user/review", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function apiUserReview(Request $request)
  {
    set_time_limit(0);
    $data = self::initJSONArray("Performs a fake review that creates a perfect agreement. Used for testing.");
    $data["header"]["status"] = "200";

    if ($request->query->has("id")) {
      $data["result"]["project"] = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findOneBy(array("id" => $request->query->get("id")));
      $type = $project->getType();
      $intraAgreement = $project->getIntragree();
      if ($user) {
        //$data["result"]["user"]["id"] = $user->getId();
        //$data["result"]["user"]["name"] = $user->getUsername();
        $data["result"]["username"] = $user->getUsername();
        //todo change access right
        if (self::isUserEnable($user, $request->query->get("id"), "show")) {
          $data["result"]["X-Api-Key"] = $request->headers->get("X-Api-Key");
          //todo: if 0 it returns the don't know too

          $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
          $sentences = $sentenceRepository->findBy(array("project" => $project->getId()));

          $min = 1;
          if ($type == "ranking") {
            $max = count($sentenceRepository->targetLabels($project->getId()));

          } else {
            $ranges = $project->getRanges();
            $labels = self::getEvaluationLabels($ranges);
            $max = count($labels);

          }

          $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
          $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
          $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");
          //
          $roles = $roleRepository->findBy(array('project' => $project->getId(), 'role' => 'evaluator'));
          //print_r($roles);
          //
          $evaluators = array();
          $i = 0;
          //find evaluators names
          foreach ($roles as $role) {
            //
            $u = $userRepository->findOneBy(array("id" => $role->getUser()));
            //print_r($u);
            //
            $evaluators[$u->getId()] = $u->getUsername();
            //
            $i++;
          }
          //
          $a = array();
          $count = 1;

          foreach ($sentences as $sentence) {
            if ($sentence->getType() != "source" && $sentence->getType() != "reference" && $sentence->getSimto() == NULL && $sentence->getIsintra() <= 1) {
              //
              $newEval = self::randomEvaluation($type, $min, $max);
              //todo if $eval = 0 set all don't know
              $r = array();
              foreach ($roles as $role) {
                //
                //
                $userEvaluation = $annotationRepository->findOneBy(array("num" => $sentence->getNum(), "active" => 1, "project" => $project->getId(), "user" => $role->getUser()));
                if ($userEvaluation) {
                  $m = array();
                  if ($userEvaluation->getEval() != $newEval) {
                    self::review($sentence->getNum(), $sentence->getId(), $role->getUser(), $project->getId(), $user, $newEval);
                    //
                    $m["reviewer"]["id"] = $user->getId();
                    $m["reviewer"]["name"] = $user->getUsername();
                    $m["new-eval"] = $newEval;
                    $m["new-label"] = $labels[$newEval];

                  }
                  //
                  //print_r($userEvaluation);

                  $m["evaluator"]["id"] = $role->getUser();
                  $m["evaluator"]["name"] = $evaluators[$role->getUser()];
                  $m["eval"] = $userEvaluation->getEval();
                  $m["label"] = $labels[$userEvaluation->getEval()];
                  $m["confirmed"] = $userEvaluation->getConfirmed();
                  $m["modified"] = $userEvaluation->getModified();
                  array_push($r, $m);


                  //$eval = self::randomEvaluation($type, $min, $max);
                  //
                  $s = array();
                  $s["i"] = $count;
                  $s["output_id"] = $sentence->getId();
                  $s["num"] = $sentence->getNum();
                  $s["text"] = $sentence->getText();
                  $s["isintra"] = $sentence->getIsintra();
                  $s["project"] = $project->getId();
                  $s["type"] = $type;
                  $s["min"] = $min;
                  $s["max"] = $max;
                  $s["evaluations"] = $r;
                  $count++;
                  array_push($a, $s);
                } else {
                  //todo check this case
                }

              }
            }
            //complete the task
            //$repositoryProgress = $this->getDoctrine()->getRepository('AppBundle:Progress');
            //todo check this
            //$progress = $repositoryProgress->updateProgress($repositoryProgress::REVIEW_TASK, $project->getId(0), TRUE, $user->getId(), "reviewer", null);

            $data["result"]["reviews"] = $a;
            $data["result"]["ranges"] = json_decode($ranges, TRUE);
            $data["result"]["type"] = $type;
            $data["result"]["min"] = $min;
            $data["result"]["max"] = $max;
            $data["result"]["intra"] = $intraAgreement;
            //$data["result"]["message"] = "Hello!";
            //$data["result"]["progress"] = $progress->getTask();
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized to evaluate";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  private function review($num, $outputId, $evaluatorId, $projectId, $reviewer, $eval)
  {
    //
    $em = $this->getDoctrine()->getManager();
    $repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

    /** @var Annotation $evaluator_annotation */
    $evaluator_annotation = $repository->findOneBy(array("num" => $num, "user" => $evaluatorId, "revuser" => 0));
    $project = $em->getRepository('AppBundle:Project')->findOneBy(array("id" => $projectId));

    if ($evaluator_annotation) {
      //save the current evaluation
      /** @var Annotation $reviewer_annotation */
      $reviewer_annotation = $repository->findOneBy(array("num" => $num, "user" => $evaluatorId, "active" => 1));

      if ($reviewer_annotation == null || $reviewer_annotation == $evaluator_annotation) {
        /** @var User $evaluator */
        $evaluator = $em->getRepository('AppBundle:User')->findOneBy(array("id" => $evaluatorId));

        if ($evaluator) {
          //disable evaluation done by evaluator
          $evaluator_annotation->setActive(false);
          $em->persist($evaluator_annotation);

          $annotation = new Annotation();
          $annotation->setOutputId($outputId);
          $annotation->setNum(intval($num));
          $annotation->setUser($evaluator);
          $annotation->setProject($project);
          $annotation->setEval($eval);
          $annotation->setRevuser($reviewer->getId());
          $annotation->setRevtime(new \DateTime());
          $annotation->setConfirmed($evaluator_annotation->getConfirmed());
          $em->persist($annotation);
        }
      } else {
        if ($reviewer_annotation->getEval() == $eval || preg_match('/^00+$/', $eval)) {
          $em->remove($reviewer_annotation);

          $evaluator_annotation->setActive(true);
          $em->persist($evaluator_annotation);
        } else {
          $reviewer_annotation->setEval($eval);
          $reviewer_annotation->setRevtime(new \DateTime());
          $em->persist($reviewer_annotation);
        }
      }
      $em->flush();

      //update Agreement
      /** @var  AgreementRepository $repository_agreement */
      $repository_agreement = $em->getRepository('AppBundle:Agreement');
      $repository_agreement->updateAgreement($outputId, $projectId);

    }
    return;
  }


  /**
   * @author giuliano
   * @Route("/api/user/evaluate", name="user/evaluate", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserEvaluate(Request $request)
  {
    set_time_limit(0);
    //
    $data = self::initJSONArray("Performs a fake evaluation. Used for testing.");
    $data["header"]["status"] = "200";

    if ($request->query->has("id")) {
      $data["result"]["project"] = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findOneBy(array("id" => $request->query->get("id")));
      $type = $project->getType();
      $intraAgreement = $project->getIntragree();
      //
      if ($user) {

        //$data["result"]["user"]["id"] = $user->getId();
        //$data["result"]["user"]["name"] = $user->getUsername();
        $data["result"]["username"] = $user->getUsername();
        //
        if (self::isUserEnable($user, $request->query->get("id"), "evaluate")) {
          $data["result"]["X-Api-Key"] = $request->headers->get("X-Api-Key");
          //$repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');
          $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
          $sentences = $sentenceRepository->findBy(array("project" => $project->getId()));

          //todo: if 0 it returns the don't know too
          $min = 1;
          if ($type == "ranking") {
            $max = count($sentenceRepository->targetLabels($project->getId()));

          } else {
            $ranges = $project->getRanges();
            $labels = self::getEvaluationLabels($ranges);
            $max = count($labels);

          }

          $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
          $a = array();
          $count = 1;
          $em = $this->getDoctrine()->getManager();
          foreach ($sentences as $sentence) {
            if ($sentence->getType() != "source" && $sentence->getType() != "reference" && $sentence->getSimto() == NULL) {
              $eval = strval(self::randomEvaluation($type, $min, $max));
              //
              //todo if $eval = 0 set all don't know
              //remove don't know evaluation
              //Claudio: removed
              /*
              $doNotKnownEvaluations = $annotationRepository->findBy(array("output_id" => $sentence->getId(), "project" => $project->getId(), "eval" => Annotation::DONTKNOW_VALUE, "user" => $user));

              if (count($doNotKnownEvaluations) > 0) {
                foreach ($doNotKnownEvaluations as $doNotKnown) {
                  $em->remove($doNotKnown);
                }
                $em->flush();
              }
*/
              //save the current evaluation
              $annotation = $annotationRepository->findOneBy(array("num" => $sentence->getNum(), "user" => $user));


              if ($annotation == null) {
                //
                $annotation = new Annotation();
                $annotation->setOutputId($sentence->getId());
                $annotation->setNum(intval($sentence->getNum()));
                $annotation->setUser($user);
                $annotation->setProject($project);
                $annotation->setEval(trim($eval));
                $em->persist($annotation);
                $em->flush();
              } else {
                if ($annotation->getEval() == trim($eval)) {
                  //
                  //Claudio: I don't understand this part: if the same value is send the evaluation is removed
                  //$em->remove($annotation);
                } else {
                  //
                  $annotation->setEval(trim($eval));
                  $em->persist($annotation);
                  $em->flush();
                }
              }


              //reset progress
              $annotationRepository->confirmEvaluation($sentence->getId(), $project->getId(), $user);

              $s = array();
              $s["i"] = $count;
              $s["output_id"] = $sentence->getId();
              $s["num"] = $sentence->getNum();
              $s["text"] = $sentence->getText();
              //$s["user"]["id"] = $user->getId();
              //$s["user"]["name"] = $user->getUsername();
              $s["username"] = $user->getUsername();
              $s["isintra"] = $sentence->getIsintra();
              $s["project"] = $project->getId();
              $s["eval"] = $eval;
              $s["confirmed"] = $annotation->getConfirmed();
              $s["modified"] = $annotation->getModified();
              $s["type"] = $type;
              $s["label"] = $labels[$eval];
              $s["min"] = $min;
              $s["max"] = $max;
              $count++;
              array_push($a, $s);
            }
            //complete the task
            $repositoryProgress = $this->getDoctrine()->getRepository('AppBundle:Progress');
            $progress = $repositoryProgress->updateProgress($repositoryProgress::EVALUATION_TASK, $project->getId(0), TRUE, $user->getId(), "evaluator", null);

            $data["result"]["evaluations"] = $a;
            $data["result"]["ranges"] = json_decode($ranges, TRUE);
            $data["result"]["type"] = $type;
            $data["result"]["min"] = $min;
            $data["result"]["max"] = $max;
            $data["result"]["intra"] = $intraAgreement;

            //$data["result"]["progress"] = $progress->getTask();
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized to evaluate";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @author giuliano
   * @Route("/api/user/evaluations", name="user/evaluations", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserEvaluations(Request $request)
  {
    $data = self::initJSONArray("Shows all evaluations done by the user on the specified project.");
    $data["header"]["status"] = "200";

    if ($request->query->has("id")) {
      $data["result"]["project"] = $request->query->get("id");
      $user = self::findAuthorizedUser($request);
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findOneBy(array("id" => $request->query->get("id")));
      $type = $project->getType();
      $intraAgreement = $project->getIntragree();
      if ($user) {
        //$data["result"]["user"]["id"] = $user->getId();
        //$data["result"]["user"]["name"] = $user->getUsername();
        $data["result"]["username"] = $user->getUsername();
        if (self::isUserEnable($user, $request->query->get("id"), "evaluate")) {

          $data["result"]["X-Api-Key"] = $request->headers->get("X-Api-Key");

          $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
          $sentences = $sentenceRepository->findBy(array("project" => $project->getId()));

          $min = 1;
          $max = 1;
          if ($type == "ranking") {
            $max = count($sentenceRepository->targetLabels($project->getId()));

          } else {
            $ranges = $project->getRanges();
            $labels = self::getEvaluationLabels($ranges);
            $max = count($labels);

          }

          $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

          //active = true is not necessary because the intra is calculated before the review?
          //todo check if it's run after returns different numbers
          //maybe revuser = 0
          $annotations = $annotationRepository->findBy(array("project" => $project->getId(), "user" => $user));

          $evaluations = array();
          foreach ($annotations as $annotation) {
            $e = array();
            $e["eval"] = $annotation->getEval();
            $e["username"] = $annotation->getUser()->getUsername();
            $e["modified"] = $annotation->getModified();
            $e["confirmed"] = $annotation->getConfirmed();
            $e["active"] = $annotation->getActive();
            $e["revuser"] = $annotation->getRevuser();
            $evaluations[$annotation->getNum()] = $e;
          }
          $a = array();
          $count = 1;
          foreach ($sentences as $sentence) {
            if ($sentence->getType() != "source" && $sentence->getType() != "reference" && $sentence->getSimto() == NULL) {
              $e = $evaluations[$sentence->getNum()];
              $s = array();
              $s["i"] = $count;
              $s["output_id"] = $sentence->getId();
              $s["num"] = $sentence->getNum();
              $s["text"] = $sentence->getText();
              //$s["user"]["id"] = $user->getId();
              //$s["user"]["name"] = $user->getUsername();
              $s["username"] = $e["username"];
              $s["active"] = $e["active"];
              $s["revuser"] = $e["revuser"];
              $s["isintra"] = $sentence->getIsintra();
              $s["project"] = $project->getId();
              $s["eval"] = $e["eval"];
              $s["type"] = $type;
              $s["label"] = $labels[$e["eval"]];
              $s["min"] = $min;
              $s["max"] = $max;
              $count++;
              array_push($a, $s);

            }
            $data["result"]["evaluations"] = $a;
            $data["result"]["ranges"] = json_decode($ranges, TRUE);
            $data["result"]["type"] = $type;
            $data["result"]["min"] = $min;
            $data["result"]["max"] = $max;
            $data["result"]["intra"] = $intraAgreement;
          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "Unauthorized to evaluate";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";

    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Updates a user entity.
   *
   * @author giuliano
   * @Route("/api/user/update", name="api_user_update", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserUpdate(Request $request)
  {
    $data = self::initJSONArray("Updates the specified user.");

    /** @var User $user */
    $user = self::findAuthorizedUser($request);
    if ($user) {

      $params = $request->query->all();

      /** @var UserRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:User');

      try {

        $user = $repository->update($user, $params);
        $data["header"]["status"] = "200";
        $data["result"]["username"] = $user->getUsername();
        $data["result"]["firstname"] = $user->getFirstname();
        $data["result"]["lastname"] = $user->getLastname();
        $data["result"]["affiliation"] = $user->getAffiliation();
        $data["result"]["email"] = $user->getEmail();
        //$data["result"]["roles"] = $user->getRoles();
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      } catch (ParameterException $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot update the account: ' . lcfirst($e->getMessage());
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      } catch (UserException $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot update the account: ' . lcfirst($e->getMessage());
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      } catch (Exception $e) {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = 'Cannot create the account: ' . lcfirst($e->getMessage());
        return new Response(json_encode($data, JSON_PRETTY_PRINT));
      }

    }

    $data["header"]["status"] = "401";
    $data["header"]["description"] = "Unauthorized.";
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Creates a user entity.
   *
   * @author giuliano
   * @Method("GET")
   * @Route("/api/user/create", name="api_user_create", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserCreate(Request $request)
  {
    $this->get('logger')->info($request);
    $data = self::initJSONArray("Creates a user account with the specified username, password, and email. Username and email must be unique.");
    $params = $request->query->all();

    try {
      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");

      $emailDomainValidator = $this->get('app.email_domain_validator');
      $canCreate = $emailDomainValidator->validate($params);

      $user = $userRepository->create($params, $canCreate);

      $hash['hash'] = $user->getHash();
      $userRepository->activateAccount($hash, 1);

      $data["header"]["status"] = "200";
      $data["result"]["username"] = $user->getUsername();
      //$data["header"]["password"] = $request->query->get("password");
      $data["result"]["email"] = $user->getEmail();
      //$data["result"]["hash"] = $user->getHash();
      $data["result"]["X-Api-Key"] = $user->getApiKey();
      $data["result"]["id"] = $user->getId();

      return new Response(json_encode($data, JSON_PRETTY_PRINT));

    } catch (ParameterException $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot create the account: ' . lcfirst($e->getMessage());
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    } catch (UserException $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot create the account: ' . lcfirst($e->getMessage());
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = 'Cannot create the account.';
      return new Response(json_encode($data, JSON_PRETTY_PRINT));
    }
  }

  /**
   * Updates a user entity.
   *
   * @author giuliano
   * @Route("/api/user/delete", name="user/delete", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserDelete(Request $request)
  {
    $data = self::initJSONArray("Deletes an account in the MT-EQuAl release installed on the specified hostname.");
    $user = self::findAuthorizedUser($request);

    if ($user) {

      $repository = $this->getDoctrine()->getRepository('AppBundle:User');

      $data["header"]["status"] = "200";
      $data["result"]["user"] = $user->getId();
      $data["result"]["X-Api-Key"] = $request->headers->get("X-Api-Key");
      if ($repository->deleteUser($user->getId())) {
        $data["result"]["message"] = $user->getUsername() . "(id = " . $user->getId() . ") has been deleted";
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = $user->getUsername() . "(id = " . $user->getId() . ") cannot be deleted";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @author giuliano
   * @Route("/api/user/show", name="user/show", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserShow(Request $request)
  {
    $data = self::initJSONArray("Shows an account in the MT-EQuAl release installed on the specified hostname.");

    $user = self::findAuthorizedUser($request);
    if ($user) {

      $data["header"]["status"] = "200";
      $data["result"]["id"] = $user->getId();
      $data["result"]["username"] = $user->getUsername();
      $data["result"]["firstname"] = $user->getFirstname();
      $data["result"]["lastname"] = $user->getLastname();
      $data["result"]["email"] = $user->getEmail();
      $data["result"]["X-Api-Key"] = $user->getApiKey();
      $data["result"]["affiliation"] = $user->getAffiliation();
      $data["result"]["registered"] = $user->getRegistered();
      $data["result"]["logged"] = $user->getLogged();
      $data["result"]["roles"] = $user->getRoles();
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/requests", name="user/requests", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUserRequests(Request $request)
  {
    $user = self::findAuthorizedUser($request);
    if ($user) {
      /** @var RoleRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Role');

      $data["header"]["status"] = "200";
      $data["result"]["requests"] = $repository->getUserRequests($user->getId());
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }


  /**
   * @Route("/api/user/list", name="user/list", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiUsers(Request $request)
  {
    //todo check if this method is replaced by admin/show and in case remove it or limit to the projects

    $data = self::initJSONArray("Get the list of the registered users");
    if (self::findAuthorizedUser($request)) {
      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
      $users = $userRepository->findBy(array("active" => 1), array('username' => 'DESC'));

      $p = array();
      /** @var User $user */
      foreach ($users as $user) {
        $u = array("id" => $user->getId(),
          "username" => $user->getUsername(),
          "firstname" => $user->getFirstname(),
          "lastname" => $user->getLastname(),
          "affiliation" => $user->getAffiliation(),
          "email" => $user->getEmail(),
          "roles" => $user->getRoles(),
          "registered" => $user->getRegistered()->format('l, Y-m-d H:i:s e'),
          "logged" => $user->getLogged()->format('l, Y-m-d H:i:s e'),
        );
        array_push($p, $u);
      }
      $data["header"]["status"] = "200";
      $data["result"]["users"] = $p;
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/api/user/search", name="user/search", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function apiSearchUsers(Request $request)
  {
    //todo check if this method is replaced by admin/show and in case remove it or limit to the projects

    $data = self::initJSONArray("Get the list of the registered users");
    if (self::findAuthorizedUser($request)) {
      if ($request->query->has("q")) {
        $dql = "SELECT u.id,u.username,u.firstname,u.lastname,u.affiliation,u.email,u.roles,u.registered,u.logged
        FROM AppBundle:User AS u
        WHERE u.active = 1 AND u.username LIKE :q";

        /** @var Query $query */
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setParameter('q', '%' . $request->query->get("q") . '%');

        $data["header"]["status"] = "200";
        $data["result"]["users"] = $query->getResult();
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter 'q' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized - You need to use your access token";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Test function to calculate the intra-annotator agreement, to be completed
   *
   * @Route("/api/user/intra", name="user/intra", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function intra(Request $request)
  {
    $projectId = 39;

    $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
    $sentences = $sentenceRepository->findBy(array("project" => 39));
    $s = array();
    $n = 0;
    foreach ($sentences as $sentence) {


      if ($sentence->getType() != "source" && $sentence->getType() != "reference") {
        $s[$sentence->getNum()] = $sentence;
        $f = $sentence->getIsintra();
        if ($sentence->getIsintra() > 0) {

        }
      }
      $n++;
    }
    $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
    //$roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
    //$roles = $roleRepository->findBy(array("project" => $projectId, "role" => "evaluator"));
    //$r = array();
    //$counts = array();
    //array_push($r, count($roles));

    $userId = 66;
    $annotations = $annotationRepository->findBy(array("user" => $userId, "project" => $projectId));
    $a = array();
    $i = 1;
    foreach ($annotations as $annotation) {

      $num = $annotation->getNum();
      $sentence = $s[$num];
      $eval = $annotation->getEval();
      $user = $annotation->getUser();
      $a[$annotation->getNum()] = $i . " '" . $sentence->getText() . "' " . $eval . " " . $user->getId() . " " . $user->getUsername() . " " . $sentence->getIsintra() . " '" . $sentence->getSimto() . "'";
      $i++;
      //$counts[$num][$eval]++;
    }

    //array_push($r, $a);


    $data = self::initJSONArray("Intra.");
    //$data["result"]["sentences"] = $s;
    $data["result"]["annotations"] = $a;
    $data["result"]["a"] = count($a);
    $data["result"]["s"] = count($s);
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * Test function to calculate the inter-annotator agreement, to be completed
   *
   * @Route("/api/user/inter", name="user/inter", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function inter(Request $request)
  {
    $projectId = 39;

    $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
    $sentences = $sentenceRepository->findBy(array("project" => 39));
    $s = array();
    foreach ($sentences as $sentence) {
      $s[$sentence->getNum()] = $sentence;
    }
    $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
    $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
    $roles = $roleRepository->findBy(array("project" => $projectId, "role" => "evaluator"));
    $r = array();
    $counts = array();
    array_push($r, count($roles));
    foreach ($roles as $role) {
      $userId = $role->getUser();
      $annotations = $annotationRepository->findBy(array("user" => $userId));
      $a = array();

      foreach ($annotations as $annotation) {

        $num = $annotation->getNum();
        $sentence = $s[$num];
        $eval = $annotation->getEval();
        $user = $annotation->getUser();
        $a[$annotation->getNum()] = "'" . $sentence->getText() . "' " . $eval . " " . $user->getId() . " " . $user->getUsername();
        $counts[$num][$eval]++;
      }

      array_push($r, $a);
    }

    $data = self::initJSONArray("Intra.");
    //$data["result"]["sentences"] = $s;
    $data["result"]["annotations"] = $r;
    $data["result"]["counts"] = $counts;
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
}