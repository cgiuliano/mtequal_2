<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 27/11/16
 * Time: 18:48
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Annotation;
use AppBundle\Entity\Corpus;
use AppBundle\Entity\Progress;
use AppBundle\Entity\Role;
use AppBundle\Entity\Project;
use AppBundle\Entity\Sentence;
use AppBundle\Entity\User;
use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\SentenceRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Util\UploadedFileSanitizer;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\UserController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class JSONCorpusController
 *
 * @author giuliano
 * @package AppBundle\Controller\API
 */
class JSONCorpusController extends JSONController
{
  /**
   * @Route("/api/corpus/show", name="corpus/show", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiCorpus(Request $request)
  {
    // Claudio: added check on user role

    $data = self::initJSONArray("Get the list of TUs from the uploaded files");
    if ($request->query->has("id")) {
      $user = self::findAuthorizedUser($request);
      if ($user) {
        if (self::isUserEnable($user, $request->query->get("id"), "show")) {

          /** @var CorpusRepository $repository_corpus */
          $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');

          /** @var SentenceRepository $repository_sentence */
          $repository_sentence = $this->getDoctrine()->getRepository('AppBundle:Sentence');
          $sentences = $repository_sentence->findBy(array("project" => $request->query->get("id")),
            array("corpus" => "ASC", "simto" => "ASC", "num" => "ASC", "isintra" => "ASC"));
          $corpusId = -1;
          $tuId = -1;
          $p = array();
          $s = array();
          $tus = array();
          /** @var Sentence $sentence */
          foreach ($sentences as $sentence) {
            if ($sentence->getIsintra() <= 1) {
              if ($tuId != $sentence->getId()) {
                if (count($s) > 0) {
                  //Claudio: fixed bug wrong id
                  array_push($tus, array("id" => $tuId, "sentences" => $s));
                  $s = array();
                }
                $tuId = $sentence->getId();
              }
              if ($corpusId != $sentence->getCorpus()) {
                if ($corpusId != -1) {
                  /** @var Corpus $corpus */
                  $corpus = $repository_corpus->getCorpusFromID($corpusId);
                  //$repository_corpus->statsCorpus($corpusId);
                  $c = array("filename" => $corpus->getFilename(),
                    "errors" => $corpus->getErrors(),
                    "size" => $corpus->getSentences(),
                    "TUs" => $tus);
                  array_push($p, $c);
                }
                $s = array();
                $tus = array();
                $corpusId = $sentence->getCorpus();
              }
              array_push($s, array("num" => $sentence->getNum(),
                "language" => $sentence->getLanguage(),
                "type" => $sentence->getType(),
                "text" => $sentence->getText(),
                "comment" => $sentence->getComment(),
                "updated" => $sentence->getUpdated()->format('l, Y-m-d H:i:s e')));
            }
          }

          //Claudio: fixed bug missing last TUs
          if (count($s) > 0) {
            array_push($tus, array("id" => $tuId, "sentences" => $s));
            $s = array();
          }

          if ($corpusId != -1) {
            /** @var Corpus $corpus */
            $corpus = $repository_corpus->getCorpusFromID($corpusId);
            //$repository_corpus->statsCorpus($corpusId);
            $c = array("filename" => $corpus->getFilename(),
              "errors" => $corpus->getErrors(),
              "size" => $corpus->getSentences(),
              "TUs" => $tus);
            array_push($p, $c);
          }

          if (count($p) == 0) {
            $data["header"]["status"] = "204";
          } else {
            $data["header"]["status"] = "200";
            $data["result"]["corpus"] = $p;

          }
        } else {
          $data["header"]["status"] = "401";
          $data["header"]["description"] = "You don't have permission to access to the project.";

        }
      } else {
        $data["header"]["status"] = "400";
        $data["header"]["description"] = "The parameter project 'id' is missing.";
      }
    } else {
      $data["header"]["status"] = "401";
      $data["header"]["description"] = "Unauthorized";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }

  /**
   * @author giuliano
   * @Route("/api/corpus/delete", name="corpus/delete", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function apiCorpusDelete(Request $request)
  {

    $data = self::initJSONArray("Deletes a file from a corpus");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "File delete failed!";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $data["result"]["id"] = $projectId;
      $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
      }
      if ($user) {
        if ($request->query->has('filename')) {
          $filename = $request->query->get('filename');
          $data["result"]["filename"] = $filename;
          //$data["result"]["length"] = $request->query->get('file');


          $repositoryCorpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');

          $corpus = $repositoryCorpus->findCorpus($filename, $projectId);

          if ($corpus) {
            $data["result"]["corpusId"] = $corpus->getId();
            $data["result"]["filename"] = $corpus->getFilename();
            $data["header"]["status"] = "200";
            $data["header"]["description"] = "The specified file has been deleted";
            if (!$repositoryCorpus->deleteCorpus($corpus->getId())) {
              $data["header"]["description"] = "The specified file has not been deleted";
            }
          } else {
            $data["header"]["description"] = "The specified file doesn't exist";
          }
        } else {
          $data["header"]["status"] = "400";
          $data["header"]["description"] = "The parameter 'filename' is missing.";
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }


  /**
   * @Route("/api/corpus/import", name="corpus/import", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function apiCorpusImport(Request $request)
  {
    $data = self::initJSONArray("Import a corpus from a file");
    $data["header"]["status"] = "500";
    $data["header"]["description"] = "File upload failed!";

    if ($request->query->has("id")) {
      $projectId = $request->query->get("id");
      $data["result"]["id"] = $projectId;
      $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "admin", $request);
      if ($user == null) {
        $user = self::findAuthorizedUserByProjectAndRole($data["result"]["id"], "owner", $request);
      }
      if ($user != null) {
        $uploadedFile = $request->files->get('file');
        if ($uploadedFile instanceof UploadedFile) {
          $filename = $uploadedFile->getClientOriginalName();
          $data["result"]["filename"] = $filename;
          $data["result"]["length"] = $uploadedFile->getSize();

          /** @var CorpusRepository $repository_corpus */
          $repository_corpus = $this->getDoctrine()->getRepository('AppBundle:Corpus');

          /** @var Corpus $corpus */
          $corpus = $repository_corpus->findCorpus($filename, $projectId);

          if ($corpus != null && $corpus->getSentences() > 0) {
            // corpus exists
            $data["header"]["description"] = "The file $filename is already present! " .
              $this->get('translator')->trans("mess#delete_file_before_upload");
          } else {
            // new corpus

            // sanitize the file
            try {
              UploadedFileSanitizer::sanitize($uploadedFile, true);
            } catch (Exception $e) {
              return new Response(json_encode($data, JSON_PRETTY_PRINT));
            }

            /** @var FileValidator $fileValidator */
            $fileValidator = $repository_corpus->validateFile($uploadedFile, $projectId);

            if ($fileValidator != null) {

              if (!$fileValidator->isWrong) {

                //the file has been sanitized
                //todo backup on the DB?
                $targetfile = self::backupFile($uploadedFile, $projectId);

                if ($targetfile == null) {
                  $data["header"]["status"] = "500";
                  $data["header"]["description"] = "Backup file on the server failed!";
                } else {
                  $fileValidator->uploadFile = new UploadedFile($targetfile, $fileValidator->origFilename);

                  // modified by Claudio
                  $corpus = $repository_corpus->create($fileValidator->uploadFile, $fileValidator->origFilename, $projectId, -1, "");
                  if ($corpus) {

                    /** @var Corpus $corpus */
                    $sentences = $repository_corpus->addFileData($corpus);
                    if ($sentences != null) {
                      $data["header"]["status"] = "200";
                      $data["header"]["description"] = $fileValidator->message;
                      $data["result"]["sentences"] = $sentences;
                    } else {
                      $data["header"]["status"] = "400";
                      $data["header"]["description"] = "Cannot load the sentences from the uploaed file";
                    }
                  } else {
                    $data["header"]["status"] = "400";
                    $data["header"]["description"] = "Cannot import the file";
                  }
                }
              } else {
                //Claudio: added validation error message to the header
                $data["header"]["description"] = implode("\n", $fileValidator->message);

              }
            } else {
              $data["header"]["description"] = "Failed to validate the file! ";
            }
          }
        } else {
          $data["header"]["description"] = 'Uploading file failed!';
        }
      } else {
        $data["header"]["status"] = "401";
        $data["header"]["description"] = "Unauthorized";
      }
    } else {
      $data["header"]["status"] = "400";
      $data["header"]["description"] = "The parameter project 'id' is missing.";
    }
    return new Response(json_encode($data, JSON_PRETTY_PRINT));
  }
}