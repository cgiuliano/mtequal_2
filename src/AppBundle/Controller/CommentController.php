<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 09/12/2017
 * Time: 17:17
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\CommentParametersSanitizer;
use AppBundle\Util\CommentParametersValidator;
use AppBundle\Util\GenericException;
use AppBundle\Util\RequiredParametersChecker;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentController
 * @package AppBundle\Controller
 */
class CommentController extends Controller
{
  /**
   * Returns the comments.
   *
   * @author giuliano
   * @Route("comment/get", name="app_project_get_comments", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function getComment(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      RequiredParametersChecker::check($params, ['obj', 'refid']);
      CommentParametersValidator::validate($params);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'evaluator', 'vendor', 'owner', 'reviewer']);

      /** @var RoleRepository $repository_role */
      $repository_role = $this->getDoctrine()->getRepository("AppBundle:Role");

      $evaluators = $repository_role->getAnonymousEvaluators($project->getId());

      /** @var CommentRepository $repository_comment */
      $repository_comment = $this->getDoctrine()->getRepository("AppBundle:Comment");

      $comments = $repository_comment->getComments($project->getId(), $params['obj'], $params['refid']);

      // add the anonymous label
      for ($i = 0; $i < count($comments); $i++) {
        $comments[$i]["anonymous"] = $evaluators[$comments[$i]["userid"]];
      }

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Comments found.',
        'comments' => $comments,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'apiKey' => '',
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot get comments: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot get comments.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Saves the comment.
   *
   * @author giuliano
   * @Route("comment/save", name="app_project_save_comment", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function save(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['obj', 'refid', 'comment']);
      CommentParametersSanitizer::sanitize($params);
      CommentParametersValidator::validate($params);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['evaluator', 'reviewer']);

      /** @var Comment $comment */
      $comment = new Comment($user->getId(), $project->getId());

      $comment->setComment($params['comment']);
      $comment->setObj($params['obj']);
      $comment->setRefID(intval($params['refid']));

      $commentRepository = $this->getDoctrine()->getRepository("AppBundle:Comment");
      $commentRepository->save($comment);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Comment saved.',
        'valid' => true], JSON_PRETTY_PRINT));


    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot save the comment: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot save the comment.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }
}