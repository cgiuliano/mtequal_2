<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/12/2017
 * Time: 08:17
 */

namespace AppBundle\Controller;


use AppBundle\Importing\FileValidator;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CorpusRepository;
use AppBundle\Repository\DatasetException;

use AppBundle\Repository\ProjectRepository;

use AppBundle\Repository\SentenceRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\GenericException;

use AppBundle\Util\UploadedFileSanitizer;
use Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DataController
 * @package AppBundle\Controller
 */
class DatasetController extends Controller
{

  /**
   * Resets the dataset, invoked to force to re-created the dataset.
   *
   * @Route ("dataset/reset", name="app_project_reset_dataset", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function reset(Request $request)
  {
    try {

      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      $project->setDatasetCreated(false);
      $project->setDonePooling(0);
      $projectRepository->save($project);

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Dataset reset.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot reset the dataset: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot reset the dataset.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Creates the dataset, if specified, applies pooling and creates the redundant data
   * for calculating the intra-annotator agreement.
   * Invoked in evaluation launch.
   *
   * @Route ("dataset/create", name="app_project_create_dataset", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function create(Request $request)
  {
    try {
      ini_set('max_execution_time', 0);

      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

      $message = 'Dataset created.';

      // todo replace $project->getDonePooling()
      if ($project->getDonePooling() == "1" || $project->getDatasetCreated()) {
        $message = 'Dataset already created.';
      } else {
        $sentenceRepository->deletePooling($project->getId());

        if ($project->getPooling()) {
          $sentenceRepository->applyPooling($project->getId());
          $message .= ' Applied \'' . $project->getPooling() . '\' pooling algorithm.';
        }

        $sentenceRepository->deleteIntraAnnotatorAgreement($project->getId());

        if ($project->getIntragree() > 0) {
          $sentenceRepository->createIntraCorpus($project->getId());
          $message .= ' Added ' . $project->getIntratus() . ' redundant TUs (' . $project->getIntragree() . '%)';
        }

        // todo replace with setDatasetCreated or hasDataset
        $project->setDonePooling("1");
        $project->setDatasetCreated(true);
        $projectRepository->save($project);
      }

      return new Response(json_encode([
        'code' => -1,
        'message' => $message,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot create the dataset: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot create the dataset.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Renders the dataset.
   * Invoked by dataset check-up.
   *
   * @author giuliano
   * @Route("dataset/tus", name="app_project_get_dataset")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function getTUs(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $data["done"] = $annotationRepository->listOfDone($project->getId());

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
      if ($request->query->has("corpus") && trim($request->query->get("corpus")) != "") {
        //todo is this invoked yet?
        $data["corpus"] = trim($request->query->get("corpus"));
        $data["sentences"] = $sentenceRepository->findBy(array("corpus" => $data["corpus"], "project" => $project->getId()), array("simto" => "ASC", "num" => "ASC", "isintra" => "ASC"));
      } else {
        $data["sentences"] = $sentenceRepository->findBy(array("project" => $project->getId()), array("simto" => "ASC", "num" => "ASC", "isintra" => "ASC"));
      }
      $data["tus"] = $sentenceRepository->countTUs($project->getId());
      $data["targets"] = $sentenceRepository->targetLabels($project->getId());

      $data["textualReferences"] = $sentenceRepository->textualReferenceCount($project->getId());
      $data["urlReferences"] = $sentenceRepository->urlReferenceCount($project->getId());
      $data["pooledTUs"] = $sentenceRepository->countPooledTUs($project->getId());
      $data["redundantTUs"] = $sentenceRepository->countRedundantTUs($project->getId());

      return $this->render('listing/sentence.twig', $data);
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Renders the files.
   * Invoked by import dataset.
   *
   * @author giuliano
   * @Route("import/files", name="app_project_get_files")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function getFiles(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var CorpusRepository $corpusRepository */
      $corpusRepository = $this->getDoctrine()->getRepository('AppBundle:Corpus');
      $files = $corpusRepository->findBy(array("project" => $project->getId()), array('date' => 'DESC'));


      return $this->render('listing/import-dataset.twig', array('files' => $files, 'project' => $project));
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Deletes all translation units imported from a specified file.
   * Invoked by dataset check-up.
   *
   * @author giuliano
   * @Route("dataset/delete", name="app_project_delete_file", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function delete(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      /** @var CorpusRepository $corpusRepository */
      $corpusRepository = $this->getDoctrine()->getRepository('AppBundle:Corpus');
      $corpus = $corpusRepository->findCorpusById($params);

      // remove the original uploaded file in the uploads dir
      if (file_exists($corpus->getFilepath())) {
        unlink($corpus->getFilepath());
      }

      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
      $sentenceRepository->deleteCorpus($project->getId(), $corpus->getId());

      $corpusRepository->delete($project->getId(), $corpus->getId());


      return new Response(json_encode([
        'code' => -1,
        'message' => 'The file has been removed.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot remove the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot remove the file.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Uploads a dataset from a file.
   *
   * @author giuliano
   * @Route("dataset/upload", name="app_project_upload_file", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function upload(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      $uploadedFile = $request->files->get('upfile');
      if (!$uploadedFile instanceof UploadedFile) {
        throw new DatasetException('Cannot upload the file.', 'Wait some minutes and try again.');
      }

      $filename = $uploadedFile->getClientOriginalName();
      /** @var CorpusRepository $corpusRepository */
      $corpusRepository = $this->getDoctrine()->getRepository('AppBundle:Corpus');

      if ($corpusRepository->findOneBy(array("filename" => $filename, "project" => $project->getId()))) {
        throw new DatasetException('Cannot upload the file: the file \'' . $filename . '\' is already present.', 'Specify another file.');
      }

      UploadedFileSanitizer::sanitize($uploadedFile, true);

      /** @var FileValidator $fileValidator */
      $fileValidator = $corpusRepository->validateFile($uploadedFile, $project->getId());
      if (is_null($fileValidator) || $fileValidator->isWrong) {
        throw new DatasetException('Cannot validate the file: ' . implode("\n", $fileValidator->message), 'Specify a valid file.');
      }

      $targetPath = $this->saveFile($uploadedFile, $project->getId());

      if (is_null($targetPath)) {
        throw new DatasetException('Cannot save the file.', 'Wait some minutes and try again.');
      }

      $corpus = $corpusRepository->create($targetPath,
        $fileValidator->origFilename,
        $project->getId(),
        -1,
        implode("\n", $fileValidator->message));

      // parsing file
      $size = $corpusRepository->addFileData($corpus);

      return new Response(json_encode([
        'code' => -1,
        'cid' => $corpus->getId(),
        'size' => $size,
        'message' => 'The file has been uploaded.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()),
        'valid' => false], JSON_PRETTY_PRINT));
    }

  }

  /**
   * Creates a copy of the uploaded file in the upload folder (specified in app/config/parameters.yml).
   * The file is moved to the destination folder only if it is valid.
   *
   * @param UploadedFile $file
   * @param $projectId
   * @return mixed|string
   * @throws DatasetException
   */
  private function saveFile(UploadedFile $file, $projectId)
  {
    $target_path = $this->container->getParameter('uploaded_files_dir');

    if ($target_path != null) {
      //todo add DIRECTORY_SEPARATOR to the end of $target_path if not present
      $target_path .= $projectId . DIRECTORY_SEPARATOR;
      if (!file_exists($target_path)) {
        mkdir($target_path, 0777);
      }

      $target_path .= $file->getClientOriginalName();
      //todo try: $file = $uploadedFile->move('/uploads/directory', $name);

      move_uploaded_file($file, $target_path);
      return $target_path;
    }
    throw new DatasetException('Cannot save the file.', 'Wait some minutes and try again.');
  }
}