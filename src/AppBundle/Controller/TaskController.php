<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 07/12/17
 * Time: 08:49
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Progress;
use AppBundle\Entity\Project;

use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationRepository;

use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\ProjectRepository;

use AppBundle\Repository\SentenceRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\Constants;
use AppBundle\Util\GenericException;
use AppBundle\Util\GenericValidator;

use AppBundle\Util\ParameterNotValidException;

use AppBundle\Util\RequiredParametersChecker;

use DateTime;
use DateTimeZone;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class TaskController extends Controller
{
  /**
   * Returns the due dates.
   *
   * @author giuliano
   * @Route("/task/due", name="app_task_set_due_date", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function setDueDate(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['task', 'date']);
      GenericValidator::validateDate($params['date']);
      GenericValidator::validateIntIdentifier($params['task']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      //$params['dueDate'] .= " 23:59:59";

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

      /** @var Progress $progress */
      $progress = $progressRepository->findOneBy(array("id" => $params['task']));
      $progress->setDuedate(new DateTime($params['date'], new DateTimeZone('UTC')));
      $progressRepository->save($progress);

      return new Response(json_encode([
        'code' => -1,
        'date' => $progress->getDuedate()->format('Y-m-d H:i:s'),
        'message' => 'Due date set.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot set the due date.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }


  /**
   * Returns the task list.
   *
   * @author giuliano
   * @Route("/task/all", name="app_task_get_tasks", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function getTasks(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      RequiredParametersChecker::check($params, ['pid']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'reviewer', 'owner']);


      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");

      $params = $request->query->all();
      $tasks = $progressRepository->getTasks($params['pid']);

      return new Response(json_encode([
        'code' => -1,
        'tasks' => $tasks,
        'message' => 'The tasks.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot find the task.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Returns task info.
   *
   * @author giuliano
   * @Method("GET")
   * @Route("/task/get", name="app_task_get_task", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public
  function getTask(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      RequiredParametersChecker::check($params, ['pid', 'task']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'reviewer', 'owner']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progress = $progressRepository->findProgressByProjectIdAndTask($params);

      return new Response(json_encode([
        'code' => -1,
        'user' => $progress->getUser(),
        'project' => $progress->getProject(),
        'completed' => $progress->getCompleted(),
        'num' => $progress->getNum(),
        'id' => $progress->getId(),
        'dueDate' => $progress->getDuedate(),
        'disabled' => $progress->getDisabled(),
        'task' => $progress->getTask(),
        'modified' => $progress->getModified(),
        'message' => 'The task.',
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot find the task.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * ...
   *
   * @Route("/task/setComplete", name="set_task_complete", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function complete(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'reviewer', 'owner']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progress = $progressRepository->findProgressByProjectIdAndTask($params);

      $progress = $this->setAndGetTaskComplete($project, $user, $progress);

      $progress->setUser($user->getId());
      $progress->setModified(new DateTime('now'));
      $progressRepository->save($progress);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $progress,
        'message' => 'The task has been marked complete.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot mark as complete the task.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * ...
   *
   * @Route("/task/setIncomplete", name="set_task_incomplete", defaults={"_format" = "json"})
   * @Method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function incomplete(Request $request)
  {
    $params = $request->request->all();

    if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
      return new JsonResponse(['link' => $request->getSchemeAndHttpHost() . $this->get('router')->generate('admin'),
        'message' => 'Invalid or missing CSRF token.'], 403);
    }

    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $repository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isAuthorized($user, $project, ['admin', 'reviewer', 'owner']);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
      $progress = $progressRepository->findProgressByProjectIdAndTask($params);

      $progress = $this->setAndGetTaskIncomplete($project, $user, $progress);

      $progress->setUser($user->getId());
      $progress->setModified(new DateTime('now'));
      $progressRepository->save($progress);

      return new Response(json_encode([
        'code' => -1,
        'id' => $project->getId(),
        'progress' => $progress,
        'message' => 'The task has been marked incomplete.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot mark as incomplete the task.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ParameterNotValidException
   * @throws TaskException
   */
  private function setAndGetTaskComplete($project, $user, $progress)
  {
    try {
      switch ($progress->getTask()) {
        case 'evalformat':
          return $this->setAndGetEvaluationFormatComplete($project, $user, $progress);
        case 'guidelines':
          return $this->setAndGetGuidelinesComplete($project, $user, $progress);
        case 'import_corpus':
          return $this->setAndGetImportCorpusComplete($project, $user, $progress);
        case 'review':
          return $this->setAndGetReviewComplete($project, $user, $progress);
        case 'finalize':
          return $this->setAndGetFinalizeComplete($project, $user, $progress);
      }

    } catch (Exception $e) {
      throw  new TaskException('Cannot mark as complete the task.', $e->getMessage());
    }

    throw  new ParameterNotValidException($progress->getTask(), 'Unrecognized task: \'' . $progress->getTask() . '\'');
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ParameterNotValidException
   * @throws TaskException
   */
  private function setAndGetTaskIncomplete($project, $user, $progress)
  {
    try {
      switch ($progress->getTask()) {
        case 'evalformat':
          return $this->setAndGetTaskIncompleteIFFCheckCorpusIncomplete($project, $user, $progress);
        case 'guidelines':
          return $this->setAndGetTaskIncompleteIFFCheckCorpusIncomplete($project, $user, $progress);
        case 'import_corpus':
          return $this->setAndGetTaskIncompleteIFFCheckCorpusIncomplete($project, $user, $progress);
        case 'review':
          return $this->setAndGetTaskIncompleteIFFFinalizeIncomplete($project, $user, $progress);
        case 'finalize':
          return $this->setAndGetFinalizeIncomplete($project, $user, $progress);

      }


    } catch (Exception $e) {
      throw  new TaskException('Cannot mark as incomplete the task.', $e->getMessage());
    }
    throw  new ParameterNotValidException($progress->getTask(), 'Unrecognized task: \'' . $progress->getTask() . '\'');
  }


  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ParameterNotValidException
   * @throws ProjectException
   */
  private
  function setAndGetEvaluationFormatComplete($project, $user, $progress)
  {
    if ($project->getType() == "") {
      throw new ProjectException(
        'The evaluation format has not been selected', 'Choose one among \'Binary\', \'Scale\', \'Ranking\', and \'Tree\'');
    }

    if ($project->getType() == "binary" || $project->getType() == "scale") {


      $ranges = json_decode($project->getRanges());

      if ($ranges == null) {
        throw new ParameterNotValidException("ranges", "Specify the evaluation format.");
      }

      if (count($ranges) < 2) {
        throw new ParameterNotValidException("ranges", "Specify at least 2 labels for the evaluation format.");
      }

      $previousLabel = '';
      $count = 0;
      foreach ($ranges as $range) {

        if ($previousLabel == $range->label) {
          throw new ParameterNotValidException("ranges", "Specify valid labels for the evaluation format, the label '" . $range->label . "' is duplicated.");
        }

        if (strlen($range->label) == 0) {
          throw new ParameterNotValidException("ranges", "Specify valid labels for the evaluation format, the " . Constants::ORDINALS[$count] . " is empty");
        }
        $count++;
        $previousLabel = $range->label;
      }
    }

    $progress->setCompleted(TRUE);
    $progress->setDisabled(TRUE);

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ProjectException
   */
  private
  function setAndGetImportCorpusComplete($project, $user, $progress)
  {
    /** @var SentenceRepository $sentenceRepository */
    $sentenceRepository = $this->getDoctrine()->getRepository("AppBundle:Sentence");

    //check if at least a corpus was uploaded correctly
    $count = $sentenceRepository->countTUs($project->getId());

    if ($count == 0) {
      throw new ProjectException('The dataset is empty', 'At least a translation unit has to be imported.');
    }

    $progress->setCompleted(TRUE);
    $progress->setDisabled(TRUE);

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   */
  private function setAndGetGuidelinesComplete($project, $user, $progress)
  {
    $progress->setCompleted(TRUE);
    $progress->setDisabled(TRUE);

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws Exception
   */
  private function setAndGetTaskIncompleteIFFCheckCorpusIncomplete($project, $user, $progress)
  {
    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
    $checkCorpus = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "check_corpus"));
    if ($checkCorpus->getCompleted()) {
      throw new Exception('The evaluation has to be stopped before.');
    }

    $progress->setCompleted(FALSE);
    $progress->setDisabled(TRUE);
    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws Exception
   */
  private function setAndGetTaskIncompleteIFFFinalizeIncomplete($project, $user, $progress)
  {
    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
    $finalize = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "finalize"));
    if ($finalize->getCompleted()) {
      throw new Exception('The report has to be marked as incomplete before.');
    }

    $progress->setCompleted(FALSE);
    $progress->setDisabled(TRUE);
    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ProjectException
   */
  private
  function setAndGetReviewComplete($project, $user, $progress)
  {

    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
    $evaluations = $progressRepository->findBy(array("project" => $project->getId(), "task" => "evaluation"));


    /** @var Progress $evaluation */
    foreach ($evaluations as $evaluation) {

      if (!$evaluation->getCompleted()) {
        throw new ProjectException('All evaluations have to be completed before.', '');
      }
    }

    $progress->setCompleted(TRUE);
    $progress->setDisabled(TRUE);

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   * @throws ProjectException
   */
  private
  function setAndGetFinalizeComplete($project, $user, $progress)
  {
    /** @var ProgressRepository $progressRepository */
    $progressRepository = $this->getDoctrine()->getRepository("AppBundle:Progress");
    $review = $progressRepository->findOneBy(array("project" => $project->getId(), "task" => "review"));

    if ($review && !$review->getCompleted()) {
      throw new ProjectException('The review has to be marked as complete before.', '');
    }

    // check if evaluations have been completed
    /** @var  AnnotationRepository $annotationRepository */
    //$annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');

    $evaluations = $progressRepository->findBy(array("project" => $project->getId(), "task" => "evaluation"));


    /** @var Progress $evaluation */
    foreach ($evaluations as $evaluation) {
      //$countDone = $annotationRepository->countDone($project->getId(), $evaluation->getUser());
      if (!$evaluation->getCompleted()) {
        throw new ProjectException('All evaluations have be completed before.', '');
      }
    }


    $progress->setCompleted(TRUE);
    $progress->setDisabled(TRUE);

    return $progress;
  }

  /**
   * @author giuliano
   * @param Project $project
   * @param User $user
   * @param Progress $progress
   * @return Progress
   */
  private function setAndGetFinalizeIncomplete($project, $user, $progress)
  {
    $progress->setCompleted(FALSE);
    $progress->setDisabled(TRUE);

    return $progress;
  }
}