<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 09/12/2017
 * Time: 05:59
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Agreement;
use AppBundle\Entity\Annotation;

use AppBundle\Entity\User;
use AppBundle\Repository\AgreementRepository;
use AppBundle\Repository\AnnotationException;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\CommentRepository;

use AppBundle\Repository\ProgressRepository;

use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\SentenceRepository;

use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ViewedDataRepository;
use AppBundle\Service\AuthenticationException;

use AppBundle\Util\GenericException;

use AppBundle\Util\RequiredParametersChecker;

use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends Controller
{
  /**
   * Returns the reviewed pair.
   *
   * @param Request $request
   * @return Response
   * @author giuliano
   * @Route("review/line", name="app_project_review_line", defaults={"_format" = "json"})
   * @Method("GET")
   * @deprecated
   */
  public function getLine(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isReviewer($user, $project);

      /** @var AgreementRepository $agreementRepository */
      $agreementRepository = $this->getDoctrine()->getRepository('AppBundle:Agreement');
      /** @var Agreement $agreement */
      $agreement = $agreementRepository->findOneBy(array('num' => $params['target']));

      /** @var CommentRepository $commentRepository */
      $commentRepository = $this->getDoctrine()->getRepository('AppBundle:Comment');
      $sourceComments = $commentRepository->findBy(array('refid' => $params['source']));
      $targetComments = $commentRepository->findBy(array('refid' => $params['target']));
      $comments = count($sourceComments) + count($targetComments);

      /** @var ViewedDataRepository $viewedDataRepository */
      $viewedDataRepository = $this->getDoctrine()->getRepository('AppBundle:ViewedData');
      $viewedEvaluations = $viewedDataRepository->findOneBy(array('num' => $params['target']));

      return new Response(json_encode([
        'code' => -1,
        'message' => 'agreement line',
        'id' => $params['id'],
        'num' => $agreement->getNum(),
        'values' => json_decode($agreement->getValues()),
        'k' => $agreement->getK(),
        'n' => $agreement->getN(),
        'agreement' => $agreement->getAgreement(),
        'comments' => $comments,
        'viewed' => ($viewedEvaluations !== null),
        'type' => $project->getType(),
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'apiKey' => '',
        'exception' => 'Exception',
        'message' => 'Cannot refresh the access token.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Renders the evaluations to review.
   *
   * @param Request $request
   * @return Response
   * @author giuliano
   * @Route("review/agreement", name="app_project_get_agreement")
   * @Method("GET")
   */
  public function getAgreement(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isReviewer($user, $project);

      /** @var ProgressRepository $progressRepository */
      $progressRepository = $this->getDoctrine()->getRepository('AppBundle:Progress');

      /** @var SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
      $targets = $sentenceRepository->targetLabels($project->getId());

      $incompleteEvaluations = $progressRepository->findBy(array('task' => 'evaluation',
        'project' => $project->getId(),
        'completed' => 0));

      //check if all evaluations are complete
      if (count($incompleteEvaluations) > 0
      ) {
        return new Response($this->renderView('listing/agreement.twig',
          array('project' => $project)));
      }

      /** @var AgreementRepository $agreementRepository */
      $agreementRepository = $this->getDoctrine()->getRepository('AppBundle:Agreement');

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $evaluators = $roleRepository->getUserByRole($project->getId(), 'evaluator');

      //todo is this used?
      $evaluatorCount = -1;
      if ($request->query->has('complete') && $request->query->get('complete') == 'on') {
        $evaluatorCount = count($evaluators);
      }

      if ($request->query->has('disagreement') && $request->query->get('disagreement') == 'on') {
        $agreement = $agreementRepository->getDisagreement($project->getId(), $evaluatorCount);
        $evalCounter = $agreementRepository->getDisagreementCountEvaluation($project->getId(), $evaluatorCount);
      } else {
        $agreement = $agreementRepository->getAgreement($project->getId(), $evaluatorCount);
        $evalCounter = $agreementRepository->getAgreementCountEvaluation($project->getId(), $evaluatorCount);
      }

      /** @var CommentRepository $commentRepository */
      $commentRepository = $this->getDoctrine()->getRepository('AppBundle:Comment');
      $comments = $commentRepository->getCommentsDistribution($project->getId());

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $k = $annotationRepository->calculateKappaQuality($project->getId(), false);
      if ($k != 0) {
        $k = round($k, 3);
      }

      /** @var ViewedDataRepository $viewedDataRepository */
      $viewedDataRepository = $this->getDoctrine()->getRepository('AppBundle:ViewedData');
      $viewedEvaluations = $viewedDataRepository->getViewedEvaluations($project->getId(), $user->getId());

      return new Response($this->renderView('listing/agreement.twig',
        array('data' => $agreement,
          'evalcounter' => $evalCounter,
          'evaluators' => count($evaluators),
          'k' => $k,
          'viewedrev' => $viewedEvaluations,
          'comments' => $comments,
          'pid' => $project->getId(),
          'targets' => $targets,
          'ranges' => json_decode($project->getRanges()),
          'project' => $project)));


    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Renders the evaluation pair (source/target) to review. This is displayed into the modal.
   *
   * @param Request $request
   * @return Response
   * @author giuliano
   * @Route("review/pair", name="app_project_get_pair")
   * @Method("GET")
   */
  public function getPair(Request $request)
  {
    try {
      $this->get('logger')->info($request);
//      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
//        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
//      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isReviewer($user, $project);

      /** @var  SentenceRepository $sentenceRepository */
      $sentenceRepository = $this->getDoctrine()->getRepository('AppBundle:Sentence');
      $targets = $sentenceRepository->targetLabels($project->getId());

      /** @var  AnnotationRepository $repository_annotation */
      $repository_annotation = $this->getDoctrine()->getRepository('AppBundle:Annotation');

      /** @var  RoleRepository $repository_role */
      $repository_role = $this->getDoctrine()->getRepository("AppBundle:Role");

      $sentences = $sentenceRepository->getTUOneSentence($params{"num"}, $params{"id"}, $project->getId());
      $countTUs = $sentenceRepository->countTUs($project->getId());
      $countDone = $repository_annotation->countDone($project->getId(), $user->getId());

      $evaluators = $repository_role->getUsers($project->getId(), $user->getId(), "evaluator");
      $userRoles = $repository_role->getUserRoles($user->getId(), $project->getId());
      $done = $repository_annotation->isDone($params["id"], $project->getId(), $user->getId());
//      if ($repository_annotation->isDone($params{"id"}, $project->getId(), $user)) {
//        $done = 1;
//      }

      $annotations = $repository_annotation->getTargetAnnotations($params["num"]);

      /** @var ViewedDataRepository $viewedDataRepository */
      $viewedDataRepository = $this->getDoctrine()->getRepository('AppBundle:ViewedData');
      $viewedDataRepository->update($params['num'], $project->getId(), $user->getId());

      return new Response($this->renderView('listing/review.twig', array(
        'id' => $params['id'],
        'pid' => $project->getId(),
        'num' => $params['num'],
        'targets' => $targets,
        'project' => $project,
        'userRoles' => $userRoles,
        'sentences' => $sentences,
        'countTUs' => $countTUs,
        'countDone' => $countDone,
        'evaluators' => $evaluators,
        'done' => $done,
        'annotations' => $annotations
      )));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * Returns the kappa statistics.
   *
   * @param Request $request
   * @return Response
   * @author giuliano
   * @Route("review/kappa", name="app_project_get_kappa", defaults={"_format" = "json"})
   * @Method("GET")
   */
  public function getKappa(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isReviewer($user, $project);

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
      $k = $annotationRepository->calculateKappaQuality($project->getId(), false);
      if ($k != 0) {
        $k = round($k, 3);
      }
      return new Response(json_encode([
        'code' => -1,
        'message' => 'Fleiss\' kappa.',
        'kappa' => $k,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'apiKey' => '',
        'exception' => 'Exception',
        'message' => 'Cannot refresh the access token.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Saves the review.
   *
   * @param Request $request
   * @return Response
   * @author giuliano
   * @Route("review/save", name="app_project_save_review", defaults={"_format" = "json"})
   * @Method("POST")
   */
  public function save(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['id', 'num', 'evaluator', 'eval']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isReviewer($user, $project);

      /** @var AnnotationRepository $annotationRepository */
      $annotationRepository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      //disable evaluation done by evaluator
      /** @var Annotation $evaluatorAnnotation */

      $evaluatorAnnotation = $annotationRepository->findOneBy(array("num" => $params["num"], "user" => $params["evaluator"], "revuser" => 0));

      if (is_null($evaluatorAnnotation)) {
        throw  new AnnotationException('Cannot find the segment to review.', 'Specify a valid segment.');
      }

      //save the current evaluation
      /** @var Annotation $reviewerAnnotation */
      $reviewerAnnotation = $annotationRepository->findOneBy(array("num" => $params["num"], "user" => $params["evaluator"], "active" => 1));

      if ($reviewerAnnotation == null || $reviewerAnnotation === $evaluatorAnnotation) {

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');
        /** @var User $evaluator */
        $evaluator = $userRepository->findOneBy(array('id' => $params['evaluator'], 'active' => 1));

        if ($evaluator) {
          $evaluatorAnnotation->setActive(false);
          $annotationRepository->save($evaluatorAnnotation);
          //$em->persist($evaluatorAnnotation);

          $reviewerAnnotation = new Annotation();
          $reviewerAnnotation->setOutputId($params["id"]);
          $reviewerAnnotation->setNum(intval($params["num"]));
          $reviewerAnnotation->setUser($evaluator);
          $reviewerAnnotation->setProject($project);
          $reviewerAnnotation->setEval($params['eval']);
          $reviewerAnnotation->setRevuser($user->getId());
          $reviewerAnnotation->setRevtime(new \DateTime());
          $reviewerAnnotation->setConfirmed($evaluatorAnnotation->getConfirmed());
          //$em->persist($annotation);
          $annotationRepository->save($reviewerAnnotation);
        }
      } else {
        if ($reviewerAnnotation->getEval() == $params['eval'] || preg_match('/^00+$/', $params['eval'])) {
          //$em->remove($reviewerAnnotation);
          $annotationRepository->delete($reviewerAnnotation);

          $evaluatorAnnotation->setActive(true);
          //$em->persist($evaluatorAnnotation);
          $annotationRepository->save($evaluatorAnnotation);

        } else {
          $reviewerAnnotation->setEval($params['eval']);
          $reviewerAnnotation->setRevtime(new \DateTime());
          //$em->persist($reviewerAnnotation);
          $annotationRepository->save($reviewerAnnotation);
        }
      }
      //$em->flush();

      /** @var  AgreementRepository $agreementRepository */
      $agreementRepository = $this->getDoctrine()->getRepository('AppBundle:Agreement');
      $agreementRepository->updateAgreement($params["id"], $project->getId());


      return new Response(json_encode([
        'code' => -1,
        'message' => 'The review has been saved.',

        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot upload the file: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'apiKey' => '',
        'exception' => 'Exception',
        'message' => 'Cannot review the evaluation.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

}