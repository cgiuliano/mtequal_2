<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 09/12/2017
 * Time: 14:58
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Project;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\ProjectException;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\UserException;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Util\InviteParametersValidation;
use AppBundle\Util\MailDeliveryException;
use AppBundle\Util\ParameterException;

use AppBundle\Util\PasswordGenerator;
use AppBundle\Util\ReplyParametersValidation;
use AppBundle\Util\RequiredParametersChecker;
use AppBundle\Util\StringGenerator;
use AppBundle\Util\UsernameGenerator;
use DateInterval;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ErrorController extends Controller
{
  /**
   * Renters an error page.
   *
   * @Route("/error/{code}", name="app_error_page")
   * @param Request $request
   * @Method("GET")
   * @param $code
   * @return Response
   */
  public function error(Request $request, $code)
  {
    $this->get('logger')->info($request);

    $params = $request->query->all();
    switch ($code) {
      case 401:
        return new Response($this->renderView('exception/error-401.twig', array('message' => $params['message'], 'code' => 401)));
      case 403:
        return new Response($this->renderView('exception/error-403.twig', array('message' => $params['message'], 'code' => 403)));
      default:
        return new Response($this->renderView('exception/error-500.twig', array('message' => $params['message'], 'code' => 500)));
    }
  }
}