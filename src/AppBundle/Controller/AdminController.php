<?php

namespace AppBundle\Controller;

use AppBundle\Repository\AnnotationRepository;

use AppBundle\Repository\ProgressRepository;
use AppBundle\Repository\RoleRepository;
use AppBundle\Repository\ProjectRepository;

use AppBundle\Util\AdminParametersSanitizer;
use AppBundle\Util\AdminValidator;
use AppBundle\Util\GenericException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{

  /**
   * @Route("/", name="homepage")
   * @param Request $request
   * @author giuliano
   * @return Response
   */
  public function homepage(Request $request)
  {
    $this->get('logger')->info($request);

    try {
      $authenticatedUser = $this->get('app.authenticated_user');
      $authenticatedUser->isAuthenticated();

      return $this->redirectToRoute('app_dashboard_page');
    } catch (Exception $e) {
      return new Response($this->renderView('default/homepage.twig'));
    }
  }

  /**
   * Routes to the project pages.
   *
   * @Route("/admin", name="admin")
   * @Method("GET")
   * @param Request $request
   * @return Response
   * @author giuliano
   */
  public function adminAction(Request $request)
  {
    $this->get('logger')->info($request);

    $params = $request->query->all();
    try {
      AdminParametersSanitizer::sanitize($params);
      AdminValidator::validate($params);

      $session = $this->get("session");
      if (!$session->has("user")) {
        return $this->redirectToRoute('homepage');
      }


      $user = $session->get("user");

      /** @var RoleRepository $roleRepository */
      $roleRepository = $this->getDoctrine()->getRepository("AppBundle:Role");


      //show a project
      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");

      //$projectStatus = $projectRepository->getProject($params["pid"]);
      $project = $projectRepository->findOneBy(array("id" => $params['pid'], "active" => true));

      $userRoles = $roleRepository->getUserRoles($user->getId(), $params["pid"]);


      if (array_key_exists("view", $params)) {

        if (preg_match('/^settings.delete/', $params["view"])) {
          //delete project
          /** @var AnnotationRepository $annotationRepository */
          $annotationRepository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
          $projectUsers = $roleRepository->getRoles($params['pid'], $user->getId());
          $params["users"] = array();
          foreach ($projectUsers as $projectUser) {
            $params["users"][$projectUser["id"]] = $projectUser["username"];
          }
          $params["annotations"] = $annotationRepository->getAnnotationProgress($params['pid']);

        } else if (preg_match('/^settings.guidelines/', $params["view"])) {
          //guidelines (specifications)
          /** @var AnnotationRepository $annotation_repository */
          $annotationRepository = $this->getDoctrine()->getRepository("AppBundle:Annotation");
          $params["countIntraEvals"] = $annotationRepository->countIntraEvaluation($params['pid']);
          $params["countEvals"] = $annotationRepository->countEvaluation($params['pid']);
        }

      } else {

        $params['projectRoles'] = $roleRepository->statsRoles($params['pid'], "accepted");//$params['roles'];

        $params["roles"] = $roleRepository->getUserRoles($user->getId(), $params["pid"]);
        if (count($params['roles']) == 1) {
          if (in_array("evaluator", $params['roles'])) {
            $params["view"] = "evaluation";
          }
        }
      }

      $params["userRoles"] = $userRoles;
      $params['project'] = $project;

      return new Response($this->renderView('default/admin.twig', $params));

    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }

  }

  /**
   * Renders the dashboard.
   *
   * @Route("/dashboard", name="app_dashboard_page")
   * @author giuliano
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function dashboardAction(Request $request)
  {
    $this->get('logger')->info($request);

//    if (!$this->isCsrfTokenValid('authenticate', $request->query->get("CsrfToken"))) {
//      return new Response($this->renderView('exception/error-403.twig'), 403);
//    }

    try {
      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var RoleRepository $repository */
      $repository = $this->getDoctrine()->getRepository('AppBundle:Role');
      $pending = $repository->pendingCount($user->getId());

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $projects = $projectRepository->getProjectList($user->getId());

      return new Response($this->renderView('default/dashboard.twig', array('pending' => $pending, 'projects' => $projects)));
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }
  }

  /**
   * @author giuliano
   * @param $path
   * @return Response
   */
  public function pageNotFoundAction($path)
  {
    //throw new NotFoundHttpException('No route found for ' . $path);
    return new Response($this->renderView('exception/page-not-found.twig', array('path' => $path)));
  }
}
