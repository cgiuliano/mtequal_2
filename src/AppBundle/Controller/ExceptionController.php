<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/01/18
 * Time: 16:53
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExceptionController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class ExceptionController extends Controller
{
  /**
   * Renters an error page.
   *
   * @Route("/exception", name="app_exception_page")
   * @param Request $request
   * @Method("GET")
   * @return Response
   */
  public function error(Request $request)
  {
    $this->get('logger')->info($request);

    $params = $request->query->all();

    return new Response($this->renderView('exception/code-exception.twig', $params));

  }
}