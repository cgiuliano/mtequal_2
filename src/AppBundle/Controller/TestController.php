<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/11/17
 * Time: 18:08
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\AuthenticationException;
use AppBundle\Service\AuthorizationException;
use AppBundle\Util\GenericException;
use AppBundle\Util\MailDeliveryException;
use AppBundle\Util\SanitizationException;
use AppBundle\Util\StringGenerator;

use AppBundle\Util\MissingRequiredParameterException;
use AppBundle\Util\ParameterException;

use AppBundle\Util\ProjectParametersValidator;
use AppBundle\Util\ProjectParametersSanitizer;
use AppBundle\Util\RequiredParametersChecker;
use AppBundle\Util\UploadedFileSanitizer;
use AppBundle\Util\UserParametersSanitizer;
use AppBundle\Util\UserParametersValidator;

use DateTime;
use Swift_Mailer;
use Swift_SendmailTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Config\Definition\Exception\Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zend\Validator\Date;

/**
 * Class TestController
 *
 * @author giuliano
 * @package AppBundle\Controller
 */
class TestController extends Controller
{
  /**
   * @Route("/test/users", name="test_users", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function showUsers(Request $request)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->getDoctrine()->getRepository("AppBundle:User");
    $users = $userRepository->findBy(array('active' => 1));

    $data = [];
    /** @var User $user */
//    foreach ($users as $user) {
//      array_push($data, $user->toArray());
//    }

    return new Response(json_encode([
      'users' => $data,
      'size' => count($users),
      'message' => 'Active users.',
      'code' => 0,
      'valid' => true], JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/test/project", name="test_project", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function validateProject(Request $request)
  {
    $params = $request->query->all();

    try {
      RequiredParametersChecker::check($params, ['name']);
      ProjectParametersSanitizer::sanitize($params);
      ProjectParametersValidator::validate($params);
    } catch (ParameterException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'parameter' => $e->getParameter(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }

    return new Response(json_encode([
      'name' => $params['name'],
      'description' => $params['description'],
      'type' => $params['type'],
      'ranges' => $params['ranges'],
      'message' => 'Project valid.',
      'code' => 0,
      'valid' => true], JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/test/user", name="test_user", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function validateUser(Request $request)
  {
    $params = $request->query->all();

    try {
      RequiredParametersChecker::check($params, ['username', 'password', 'email']);

      UserParametersSanitizer::sanitize($params);
      UserParametersValidator::validate($params);
    } catch (ParameterException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'parameter' => $e->getParameter(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }

    return new Response(json_encode([
      'username' => $params['username'],
      'email' => $params['email'],
      'password' => $params['password'],
      'confirm_password' => $params['confirm_password'],
      'firstName' => $params['firstName'],
      'lastName' => $params['lastName'],
      'affiliation' => $params['affiliation'],
      'notes' => $params['notes'],
      'roles' => $params['roles'],
      'message' => 'User valid.',
      'valid' => true], JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/test/authorized", name="test_authorized_user", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   * @throws MissingRequiredParameterException
   * @throws \AppBundle\Repository\ProjectException
   * @throws \AppBundle\Repository\UserException
   */
  public function authorizedUser(Request $request)
  {
    $params = $request->query->all();
    try {
//      RequiredParametersChecker::check($params, ['uid', 'pid']);

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");

      $project = $projectRepository->findActiveProjectById($params);

      /** @var UserRepository $userRepository */
      $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

      $user = $userRepository->findActiveUserById($params);

      $authorizedUser = $this->get('app.authorized_user');

      $isOwnerOrAdmin = $authorizedUser->isOwnerOrAdmin($user, $project);
      $isOwner = $authorizedUser->isOwnerOrAdmin($user, $project);
      $isAdmin = $authorizedUser->isAdmin($user, $project);
      $isEvaluator = $authorizedUser->isEvaluator($user, $project);
      $isVendor = $authorizedUser->isVendor($user, $project);
      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'content' => $request->getContent(),
        'username' => $user->getUsername(),
        'project' => $project->getName(),
        'isOwnerOrAdmin' => $isOwnerOrAdmin,
        'isOwner' => $isOwner,
        'isAdmin' => $isAdmin,
        'isEvaluator' => $isEvaluator,
        'isVendor' => $isVendor,
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (AuthorizationException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),

        'message' => $e->getMessage(),
        'tip' => $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }

  }


  /**
   * @Route("/test/upload", name="test_upload", defaults={"_format" = "json"})
   * @param Request $request
   * @return Response
   */
  public function validateUploadedFile(Request $request)
  {
    $params = $request->query->all();

    $uploadedFile = $request->files->get('file');
    if (!$uploadedFile->isValid()) {
      return new Response(json_encode([
        'message' => 'Uploading file failed.',
        'code' => 1,
        'valid' => false], JSON_PRETTY_PRINT));
    }
    $targetDir = $this->getParameter('uploaded_files_dir');
    try {
      UploadedFileSanitizer::sanitize($uploadedFile);
      $uploadedFile->move($targetDir, $uploadedFile->getClientOriginalName());

      return new Response(json_encode([
        'file' => $uploadedFile->getClientOriginalName(),
        'extension' => $uploadedFile->getClientOriginalName(),
        'type' => $uploadedFile->getClientMimeType(),
        'code' => 0,
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (SanitizationException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'parameter' => $e->getParameter(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Renters the test page.
   *
   * @Route("/test/testMethods", name="test_methods")
   * @param Request $request
   * @return Response
   */
  public function testAction(Request $request)
  {
    $this->get('logger')->info($request);

    $roles = array('owner', 'admin');

    $ranges = '[{"color":"#ff0000","label":"bad","val":1},{"color":"#40ff00","label":"good","val":2}]';
    return new Response($this->renderView('util/test.twig', array('roles' => $roles, 'ranges' => $ranges)));
  }

  /**
   * @Route("/test/get", name="test_get", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testGet(Request $request)
  {
    $this->get('logger')->info($request);

    return new Response(json_encode(['query' => $request->query->all(),
      'request' => $request->request->all(),
      'headers' => $request->headers->all(),
      'content' => $request->getContent(),
      'valid' => true], JSON_PRETTY_PRINT));
  }

  /**
   * @Route("/test/post", name="test_post", defaults={"_format" = "json"})
   * @Method("POST")
   * @param Request $request
   * @return Response
   */
  public function testPost(Request $request)
  {
    $this->get('logger')->info($request);

    return new Response(json_encode(['query' => $request->query->all(),
      'request' => $request->request->all(),
      'headers' => $request->headers->all(),
      'content' => $request->getContent(),
      'valid' => true], JSON_PRETTY_PRINT));
  }

  private function log($label, $array)
  {
    $this->get('logger')->info($label . ':');
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $this->get('logger')->info($key . ': ' . print_r($value));
      } else {
        $this->get('logger')->info($key . ': ' . $value);
      }

    }
  }

  /**
   * @Route("/test/account", name="test_account", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testAccount(Request $request)
  {
    $params = $request->query->all();

    $this->get('logger')->info($request);
    try {
      $emailDomainValidator = $this->get('app.email_domain_validator');
      $emailDomainValidator->validate($params);

      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'content' => $request->getContent(),
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (ParameterException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'parameter' => $e->getParameter(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @Route("/test/username", name="test_username", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testUsernameFromEmail(Request $request)
  {
    $params = $request->query->all();

    $this->get('logger')->info($request);
    try {
      RequiredParametersChecker::check($params, ['email']);

      $usernameGenerator = $this->get('app.username_generator');
      $username = $usernameGenerator->fromEmail($params['email']);

      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'content' => $request->getContent(),
        'username' => $username,
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        //'exception' => $e->getExceptionName(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @Route("/test/tokenGenerator", name="test_token_generator", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testTokenGenerator(Request $request)
  {
    $this->get('logger')->info($request);
    try {

      $token = StringGenerator::getAlphaNumeric(32);

      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'token' => $token,
        'content' => $request->getContent(),
        'valid' => true], JSON_PRETTY_PRINT));
    } catch (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @Route("/test/exception", name="test_exception")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testException(Request $request)
  {
    $this->get('logger')->info($request);
    try {

      throw new AuthenticationException('An authentication exception occurred.', 'Try to login again.');

    } catch (GenericException $e) {
      return new Response($this->redirectToRoute('app_exception_page', array('message' => $e->getMessage(), 'code' => $e->getCode(), 'tip' => $e->getTip())));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
    }

  }

  /**
   * @Route("/test/sendMail", name="test_send_mail", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function testSendMail(Request $request)
  {
    $this->get('logger')->info($request);
    try {

      $params = $request->query->all();
      $now = new DateTime("now");

      RequiredParametersChecker::check($params, array('from', 'to'));


      /** @var \Swift_Mailer $mailer */
      //$mailer = $this->get('mailer');

      // Create the Transport
      $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');

      $this->get('logger')->info($transport->getLocalDomain());
      $this->get('logger')->info($transport->getSourceIp());

      // Create the Mailer using your created Transport
      $mailer = new Swift_Mailer($transport);

      $host = $request->getSchemeAndHttpHost();
      $hostname = gethostname();
      $username = get_current_user();
      $to = $params['to'];
      //$from = $this->container->getParameter('mailer_user');
      //$from = $username . '@' . $hostname;
//      if ($this->container->hasParameter('mailer_sender_address')) {
//        $from = $this->container->getParameter('mailer_sender_address');
//      } else {
//        $from = $this->container->getParameter('mailer_user');
//      }
      $from = $params['from'];
      $this->get('logger')->info('from ' . $from . ' to ' . $to);
      $mail = \Swift_Message::newInstance()
        ->setSubject('test mail ' . $now->format('Y-m-d H:i:s'))
        ->setFrom($from, 'MT-equal Team')
        ->setTo($to)
        ->setBody(
          'test mail', 'text/html'
        );

      if (!$mailer->send($mail, $failures)) {
        throw new MailDeliveryException($from, $to, $failures);
      }

      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'to' => $to,
        'from' => $from,
        'failures' => $failures,
        'content' => $request->getContent(),
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (MissingRequiredParameterException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getMessage(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));

    } catch (MailDeliveryException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getMessage(),
        'to' => $e->getTo(),
        'from' => $e->getFrom(),
        'failures' => $e->getFailures(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @Route("/test/sendSmtpMail", name="test_send__smtp_mail", defaults={"_format" = "json"})
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function testSmtpMail(Request $request)
  {
    $this->get('logger')->info($request);
    try {

      $params = $request->query->all();


      RequiredParametersChecker::check($params, array('email'));

      /** @var \Swift_Mailer $mailer */
      $mailer = $this->get('mailer');

      $to = $params['email'];
      //$from = $this->container->getParameter('mailer_user');
      if ($this->container->hasParameter('mailer_sender_address')) {
        $from = $this->container->getParameter('mailer_sender_address');
      } else {
        $from = $this->container->getParameter('mailer_user');
      }

      $mail = \Swift_Message::newInstance()
        ->setSubject('test mail')
        ->setFrom($from)
        ->setTo($to)
        ->setBody(
          'test mail', 'text/html'
        );

      if (!$mailer->send($mail, $failures)) {
        throw new MailDeliveryException($from, $to, $failures);
      }

      return new Response(json_encode(['query' => $request->query->all(),
        'request' => $request->request->all(),
        'headers' => $request->headers->all(),
        'to' => $to,
        'from' => $from,
        'failures' => $failures,
        'content' => $request->getContent(),
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (MissingRequiredParameterException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getMessage(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));

    } catch (MailDeliveryException $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => $e->getMessage(),
        'to' => $e->getTo(),
        'from' => $e->getFrom(),
        'failures' => $e->getFailures(),
        'message' => $e->getMessage(),
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * @Route("/test/flash", name="test_flash")
   * @Method("GET")
   * @param Request $request
   * @return Response
   * @throws \Exception
   */
  public function testFlash(Request $request)
  {
//    try {
//      throw new Exception('Test exception.');
//    } catch (Exception $e) {
//      $this->addFlash(
//        'danger',
//        $e->getMessage()
//      );
//
//
//    }

    $this->addFlash(
      'info',
      'ciao'
    );
    return new Response($this->renderView('util/test.twig'));
  }
}