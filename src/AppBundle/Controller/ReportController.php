<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 11/12/2017
 * Time: 17:21
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;

use AppBundle\Repository\AnnotationRepository;

use AppBundle\Repository\ProjectRepository;

use AppBundle\Repository\ReportRepository;
use AppBundle\Service\AuthenticationException;

use AppBundle\Util\GenericException;
use AppBundle\Util\MailDeliveryException;

use AppBundle\Util\ParameterNotValidException;
use AppBundle\Util\RequiredParametersChecker;

use AppBundle\Util\StringGenerator;
use DateTime;
use Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ReportController
 * @package AppBundle\Controller
 */
class ReportController extends Controller
{
  /**
   * Renders a page that contains the project report specified by the token.
   *
   * @Route("/report/share/{token}", name="app_render_project_report")
   * @Method("GET")
   * @author giuliano
   * @param $token
   * @return Response
   */
  public function showReport($token)
  {
    //StringGenerator::validateAlphaNumeric($token);

    try {
      if (!StringGenerator::isAlphaNumeric($token)) {
        throw new ParameterNotValidException("token", "Invalid token.", "Specify a valid token, it can contain uppercase and lowercase English letters, numbers, . (dot) and - (hyphen), and _ (underscore)");
      }

      //this is the container for the evaluation report
      return $this->render('share/report.twig', array('token' => $token));

    } catch (Exception $e) {
      return $this->render('exception/invalid.twig');
    }
  }

  /**
   * Renders the public report.
   * Invoked by the share report.
   *
   * @Route("/report/get", name="app_get_report")
   * @Method("GET")
   * @param Request $request
   * @return Response
   * @see showReport
   */
  public
  function getReport(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      $params = $request->query->all();

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      RequiredParametersChecker::check($params, ['token']);

      /** @var ReportRepository $reportRepository */
      $reportRepository = $this->getDoctrine()->getRepository('AppBundle:Report');
      $report = $reportRepository->getReport($params['token']);

      /** @var AnnotationRepository $annotation_repository */
      $annotation_repository = $this->getDoctrine()->getRepository('AppBundle:Annotation');
      $reportParams = $annotation_repository->getEvalReportParams($report->getProject(), $report->getUser());
      $reportParams["token"] = $report->getToken();
      $reportParams["sharable"] = false;
      //blind review is set to true for sharable reports
      //$reportParams['blindReview'] = true;

      return new Response($this->renderView('listing/evalreport.twig', $reportParams));
    } catch (GenericException $e) {
      return new Response($this->renderView('exception/invalid.twig', array('e' => $e)));
    } catch (Exception $e) {
      return new Response($this->renderView('exception/invalid.twig', array('e' => $e)));
    }
  }


  /**
   * Shares a link to the project report by email.
   *
   * @Route("/report/send", name="share_report_by_email", defaults={"_format" = "json"})
   * @method("POST")
   * @author giuliano
   * @param Request $request
   * @return Response
   */
  public function sendReport(Request $request)
  {
    try {
      $this->get('logger')->info($request);
      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Cannot authenticate the user: invalid or missing CSRF token.');
      }

      $params = $request->request->all();

      RequiredParametersChecker::check($params, ['mailto', 'token']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ReportRepository $reportRepository */
      $reportRepository = $this->getDoctrine()->getRepository('AppBundle:Report');
      $report = $reportRepository->getReport($params['token']);

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository('AppBundle:Project');
      //$project = $projectRepository->findActiveProjectById($params);
      /** @var Project $project */
      $project = $projectRepository->findOneBy(array("id" => $report->getProject(), "active" => true));

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      $addresses = explode(',', $params['mailto']);
      foreach ($addresses as $to) {
        $this->get('logger')->info("sharing report with " . $to);
        $this->sendShareReportMail($user, $project, $to, $params['token']);
      }

      return new Response(json_encode([
        'code' => -1,
        'message' => 'Project report shared.',
        'valid' => true], JSON_PRETTY_PRINT));

    } catch (GenericException $e) {
      return new Response(json_encode(['code' => $e->getCode(),
        'exception' => $e->getExceptionName(),
        'message' => 'Cannot share the project report: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(),
        'valid' => false], JSON_PRETTY_PRINT));
    } catch
    (Exception $e) {
      return new Response(json_encode([
        'code' => $e->getCode(),
        'exception' => 'Exception',
        'message' => 'Cannot share the project report.',
        'valid' => false], JSON_PRETTY_PRINT));
    }
  }

  /**
   * Sends a link to a shareable report by email.
   *
   * @param User $user
   * @param Project $project
   * @param $to
   * @param $token
   * @throws MailDeliveryException
   * @throws Exception
   */
  public function sendShareReportMail($user, $project, $to, $token)
  {
    /** @var \Swift_Mailer $mailer */
    $mailer = $this->get('mailer');

    //$from = $this->container->getParameter('mailer_user');
    if ($this->container->hasParameter('mailer_sender_address')) {
      $from = $this->container->getParameter('mailer_sender_address');
    } else {
      $from = $this->container->getParameter('mailer_user');
    }
    $body = $this->renderView('mail/share.email.twig', array('user' => $user, 'project' => $project, 'token' => $token));
    $now = new DateTime("now");
    $mail = \Swift_Message::newInstance()
      ->setSubject("[MT-equal] " . $project->getName() . " - Invitation to view " . $now->format('Y-m-d H:i:s'))
      ->setFrom($from)
      ->setTo($to)
      ->setBody($body, 'text/html');

    if (!$mailer->send($mail, $failures)) {
      throw new MailDeliveryException($from, $to, $failures);
    }
  }
}