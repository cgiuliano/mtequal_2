<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 07/12/17
 * Time: 09:42
 */

namespace AppBundle\Controller;

use AppBundle\Util\GenericException;

class TaskException extends GenericException
{
  /**
   * TaskException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string|null $tip
   */
  public function __construct($message = 'Cannot mark the task.', $tip = null)
  {
    parent::__construct($message, 15, $tip);
  }
}