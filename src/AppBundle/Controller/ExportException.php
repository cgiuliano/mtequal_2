<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 12/12/17
 * Time: 11:24
 */

namespace AppBundle\Controller;

use AppBundle\Util\GenericException;

class ExportException extends GenericException
{
  /**
   * ExportException constructor.
   *
   * @author giuliano
   * @param string $message
   * @param string $tip
   */

  public function __construct($message, $tip = null)
  {
    parent::__construct($message, 19, $tip);
  }
}