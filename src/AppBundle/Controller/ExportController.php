<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 08/12/2017
 * Time: 08:25
 */

namespace AppBundle\Controller;

use AppBundle\Repository\AnnotationRepository;
use AppBundle\Repository\ProjectRepository;
use AppBundle\Repository\RoleRepository;

use AppBundle\Service\AuthenticationException;

use AppBundle\Util\GenericException;
use AppBundle\Util\GenericValidator;

use AppBundle\Util\RequiredParametersChecker;
use Exception;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;

/**
 * Class ExportController
 * @package AppBundle\Controller
 */
class ExportController extends Controller
{
  /**
   * Exports the datset.
   *
   * @author giuliano
   * @Route("/export/dataset", name="app_project_export_dataset")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public function dataset(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('Invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      $path = $this->container->getParameter('uploaded_files_dir');
      $path .= $project->getId() . DIRECTORY_SEPARATOR;

      if (!file_exists($path)) {
        throw new ExportException('Unable to find the dataset.', 'First a dataset must be imported.');
      }

      $temp = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'dataset_' . $project->getId() . '.zip';
      $this->zip($path, $temp);

      if (!file_exists($temp)) {
        throw new ExportException('Unable to compress the dataset.');
      }

      /** @var $response Response */
      $response = new Response();
      $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
      $response->headers->set('Expires', '-1');
      $response->headers->set('Pragma', 'public');
      $response->headers->set('Content-Type', 'application/zip');
      $response->headers->set('Content-Transfert-encoding', 'binary');
      $response->headers->set('Content-Disposition', 'attachment; filename=' . basename($temp));
      $response->headers->set('Content-Length', filesize($temp));
      $response->setContent(file_get_contents($temp));

      unlink($temp);

      return $response;
    } catch (GenericException $e) {
      //return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
      return new Response('Cannot export the dataset: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(), 404);
    } catch (Exception $e) {
      //return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
      return new Response('Cannot export the dataset: ' . lcfirst($e->getMessage()), 404);
    }
  }

  /**
   * @param $target_path
   * @param $zipName
   * @throws ExportException
   */
  private function zip($target_path, $zipName)
  {

    $zip = new ZipArchive();
    if ($zip->open($zipName, ZipArchive::CREATE) !== TRUE) {
      throw new ExportException('Unable to compress the dataset.');
    }

    $files = scandir($target_path);
    foreach ($files as $file) {
      if (!preg_match('/^\\..*/', $file)) {
        $zip->addFile($target_path . DIRECTORY_SEPARATOR . $file, $file);
      }
    }
    $zip->close();
  }

  /**
   * Exports the evaluation.
   *
   * @author giuliano
   * @Route("/export/evaluation", name="app_project_export_evaluation")
   * @Method("GET")
   * @param Request $request
   * @return Response
   */
  public
  function evaluation(Request $request)
  {
    try {
      $this->get('logger')->info($request);

      if (!$this->isCsrfTokenValid('authenticate', $request->headers->get("CsrfToken"))) {
        throw new AuthenticationException('invalid or missing CSRF token.');
      }

      $params = $request->query->all();

      RequiredParametersChecker::check($params, ['format']);
      GenericValidator::validateExportDataFormat($params['format']);

      $authenticatedUser = $this->get('app.authenticated_user');
      $user = $authenticatedUser->get();

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->getDoctrine()->getRepository("AppBundle:Project");
      $project = $projectRepository->findActiveProjectById($params);

      $authorizedUser = $this->get('app.authorized_user');
      $authorizedUser->isOwnerOrAdmin($user, $project);

      return $this->exportEvaluation($project->getId(), $project->getType(), $user->getId(), $params['format']);

    } catch (GenericException $e) {
      //return new Response($this->renderView('exception/generic-exception.twig', array('e' => $e)));
      return new Response('Cannot export the evaluation data: ' . lcfirst($e->getMessage()) . ' ' . $e->getTip(), 404);
    } catch (Exception $e) {
      //return new Response($this->renderView('exception/exception.twig', array('e' => $e)));
      return new Response('Cannot export the evaluation data: ' . lcfirst($e->getMessage()), 404);
    }
  }

  /**
   * @param $projectId
   * @param $projectType
   * @param $userId
   * @param $format
   * @return Response
   */
  private
  function exportEvaluation($projectId, $projectType, $userId, $format)
  {
    $message = "An error occurred exporting data.";
    $decisionOptions = array(
      -1 => "dont_know",
      0 => "No",
      1 => 'Yes',
      2 => '--',
      3 => 'N/A');

    /** @var ProjectRepository $repository */
    $repository = $this->getDoctrine()->getRepository('AppBundle:Project');
    /** @var Project $project */
    $project = $repository->find($projectId);

    /** @var  AnnotationRepository $repository_annotation */
    $repository_annotation = $this->getDoctrine()->getRepository('AppBundle:Annotation');

    if ($project) {
      $evalRanges = json_decode($project->getRanges(), TRUE);
      $evalRanges[-1]["label"] = "dont_know";


      /** @var RoleRepository $repository_role */
      $repository_role = $this->getDoctrine()->getRepository("AppBundle:Role");
      $joinUsers = $repository_role->getRoles($projectId, $userId);
      $users = array();
      foreach ($joinUsers as $key => $jUser) {
        $users[$jUser["id"]] = $jUser["username"];
      }

      $anonymous = array();
      $listVal = array();

      $sources = $this->getDoctrine()->getRepository('AppBundle:Sentence')->getSentences($projectId, "source");


      $targets = $repository_annotation->sentenceAnnotation($projectId);

      if (count($targets) > 0) {
        $headerFields = ["id", "source", "target-language", "target-name", "target", "label", "user"];
        //todo remove reference to tree
        if ($projectType == "tree") {
          array_splice($headerFields, count($headerFields) - 2, 1, ["ev1", "ev2", "ev3", "ev4", "ev5"]);
        }

        //inizialize the output file
        $fileout = "/tmp/mtequal_prj-" . $projectId . "_evdata-" . time() . "." . $format;
        $fp = fopen($fileout, 'w');
        if ($format == "csv") {
          fputcsv($fp, $headerFields, ',');
        } else if ($format == "json") {
          $listVal = array();
        }

        foreach ($targets as $key => $val) {
          //skip the evaluation from out of own users list
          /*if (!array_key_exists($val["user"], $users)) {
            continue;
          }*/
          $eval = $val["eval"];
          $iuser = $val["user"];
          if ($val["revuser"] > 0) {
            $iuser = $val["revuser"];
          }
          unset($val["revuser"]);

          $val = array_values($val);
          //replace user ID with username
          if (array_key_exists($iuser, $users)) {
            array_splice($val, count($val) - 1, 1, $users[$iuser]);
          } else {
            if (!array_key_exists($iuser, $anonymous)) {
              $anonymous[$iuser] = count($anonymous) + 1;
            }
            $aUser = "anonymous" . $anonymous[$iuser];
            array_splice($val, count($val) - 1, 1, $aUser);
          }

          //add source
          if (array_key_exists($val[0], $sources)) {
            array_splice($val, 1, 0, $sources[$val[0]]);
          } else {
            array_splice($val, 1, 0, "");
          }
          //for decision tree format replace the one value with multiple values (one for each question)

          //todo remove reference to tree
          if ($projectType == "tree") {
            $answers = array();
            if ($eval == 0) {
              for ($i = 0; $i < 5; $i++) {
                array_push($answers, $decisionOptions[-1]);
              }
            } else {
              for ($i = 0; $i < strlen($eval); $i++) {
                $ev = substr($eval, $i, 1);
                array_push($answers, $decisionOptions[$ev]);
              }
            }
            array_splice($val, count($val) - 2, 1, $answers);
          } else if ($projectType != "ranking") {
            array_splice($val, count($val) - 2, 1, $evalRanges[$eval - 1]["label"]);
          }

          if ($format == "csv") {
            fputcsv($fp, $val, ',');
          } else if ($format == "json") {
            $listVal[] = array_combine($headerFields, $val);
          }
        }
        if ($format == "json") {
          $response['data'] = $listVal;
          fwrite($fp, json_encode($response, JSON_PRETTY_PRINT));
        }
        fclose($fp);

        if (file_exists($fileout)) {
          /** @var Response $response */
          $response = new Response();
          $response->headers->set('Pragma', 'public');
          $response->headers->set('Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0');
          //$response->headers->set('Cache-Control: private',false); // required for certain browsers
          $response->headers->set('Expires', '-1');
          $response->headers->set('Content-Type', 'application/excel');
          $response->headers->set('Content-Transfert-Encoding', 'binary');
          $response->headers->set('Content-Disposition', 'attachment; filename=' . basename($fileout));
          $response->headers->set('Content-Length', filesize($fileout));
          $response->setContent(file_get_contents($fileout));
          unlink($fileout);

          return $response;
        }
      } else {
        $message = "There is no evaluation data.";
      }
    } else {
      $message = "There is no evaluation data.";
    }

    return new Response($message, 404);

    //return $this->redirectToRoute('admin', array('view' => 'settings.export', "message" => $message));
  }
}