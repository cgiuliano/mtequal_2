<?php
/**
 * Created by PhpStorm.
 * User: giuliano
 * Date: 2019-02-01
 * Time: 17:03
 */

namespace AppBundle\Entity;

use AppBundle\Util\StringGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Report entity class
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRepository")
 * @ORM\Table(name="report")
 * @UniqueEntity(fields="token", message="report already exists")
 */
class Report
{
  /**
   * User constructor.
   * @param $project
   * @param $user
   * @throws \Exception
   */
  function __construct($project, $user)
  {
    $this->created = new \DateTime('now');
    $this->token = StringGenerator::getAlphaNumeric(32);
    $this->project = $project;
    $this->user = $user;
    $this->active = true;
  }

  /**
   * @ORM\Column(type="boolean")
   */
  private $active;

  /**
   * @ORM\Id
   * @ORM\Column(type="integer", nullable=false, unique=true)
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\Column(type="integer", nullable=false)
   */
  private $user;

  /**
   * @ORM\Column(type="integer", nullable=false)
   */
  private $project;

  /**
   * @ORM\Column(type="datetime")
   */
  private $created;

  /**
   * @ORM\Column(type="string")
   */
  private $token;

  /**
   * @return mixed
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * @return mixed
   */
  public function getProject()
  {
    return $this->project;
  }

  /**
   * @return mixed
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * @return mixed
   */
  public function getToken()
  {
    return $this->token;
  }

  /**
   * @param mixed $active
   */
  public function setActive($active)
  {
    $this->active = $active;
  }

}