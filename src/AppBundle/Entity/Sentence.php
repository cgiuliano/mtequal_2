<?php
namespace AppBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SentenceRepository")
 * @ORM\Table(name="sentence",
 *                      indexes={@ORM\Index(name="id", columns={"id"}),
 *                              @ORM\Index(name="type", columns={"type"}),
 *                              @ORM\Index(name="language", columns={"language"}),
 *                              @ORM\Index(name="corpus", columns={"corpus"}),
 *                              @ORM\Index(name="project", columns={"project"}),
 *                              @ORM\Index(name="simto", columns={"simto"}),
 *                              @ORM\Index(name="isintra", columns={"isintra"})})
 */
class Sentence
{
    public function __construct(){
        $this->setSimto(null);
        $this->setIsintra(0);
        $this->setUpdated(new \DateTime());
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $num;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $corpus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $project;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isurl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $linked;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $simto;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $isintra;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $tokenization;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $comment;



    /**
     * @return integer
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param integer $num
     */
    public function setNum($num)
    {
        $this->num = $num;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCorpus()
    {
        return $this->corpus;
    }

    /**
     * @param int $corpus
     */
    public function setCorpus($corpus)
    {
        $this->corpus = $corpus;
    }

    /**
     * @return int
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param int $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return boolean
     */
    public function getIsurl()
    {
        return $this->isurl;
    }

    /**
     * @param boolean $isurl
     */
    public function setIsurl($isurl)
    {
        $this->isurl = $isurl;
    }

    /**
     * @return int
     */
    public function getLinked()
    {
        return $this->linked;
    }

    /**
     * @param int $linked
     */
    public function setLinked($linked)
    {
        $this->linked = $linked;
    }

    /**
     * @return int
     */
    public function getSimto()
    {
        return $this->simto;
    }

    /**
     * @param int $simto
     */
    public function setSimto($simto)
    {
        $this->simto = $simto;
    }

    /**
     * @return integer
     */
    public function getIsintra()
    {
        return $this->isintra;
    }

    /**
     * @param integer $isintra
     */
    public function setIsintra($isintra)
    {
        $this->isintra = $isintra;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTimeInterface $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTimeInterface $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getTokenization()
    {
        return $this->tokenization;
    }

    /**
     * @param int $tokenization
     */
    public function setTokenization($tokenization)
    {
        $this->tokenization = $tokenization;
    }

    /**
     * @return comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

}
