<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 24/10/15
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleTypeRepository")
 * @ORM\Table(name="roletype")
 */
class RoleType
{

  /**
   * @ORM\Id
   * @ORM\Column(type="string", nullable=false)
   */
  private $name;

  /**
   * @ORM\Column(type="text")
   */
  private $description;

  /**
   * @ORM\Column(type="boolean")
   */
  private $can_edit;

  /**
   * @ORM\Column(type="boolean")
   */
  private $can_delete;

  /**
   * @ORM\Column(type="boolean")
   */
  private $can_invite;

  /**
   * @ORM\Column(type="boolean")
   */
  private $can_judge;

  /**
   * @ORM\Column(type="boolean")
   */
  private $can_evaluate;

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * @return boolean
   */
  public function canEdit()
  {
    return $this->can_edit;
  }

  /**
   * @return boolean
   */
  public function canDelete()
  {
    return $this->can_delete;
  }

  /**
   * @return boolean
   */
  public function canInvite()
  {
    return $this->can_invite;
  }

  /**
   * @return boolean
   */
  public function canJudge()
  {
    return $this->can_judge;
  }

  /**
   * @return boolean
   */
  public function canEvaluate()
  {
    return $this->can_evaluate;
  }

    /**
     * Set canEdit
     *
     * @param boolean $canEdit
     *
     * @return RoleType
     */
    public function setCanEdit($canEdit)
    {
        $this->can_edit = $canEdit;

        return $this;
    }

    /**
     * Get canEdit
     *
     * @return boolean
     */
    public function getCanEdit()
    {
        return $this->can_edit;
    }

    /**
     * Set canDelete
     *
     * @param boolean $canDelete
     *
     * @return RoleType
     */
    public function setCanDelete($canDelete)
    {
        $this->can_delete = $canDelete;

        return $this;
    }

    /**
     * Get canDelete
     *
     * @return boolean
     */
    public function getCanDelete()
    {
        return $this->can_delete;
    }

    /**
     * Set canInvite
     *
     * @param boolean $canInvite
     *
     * @return RoleType
     */
    public function setCanInvite($canInvite)
    {
        $this->can_invite = $canInvite;

        return $this;
    }

    /**
     * Get canInvite
     *
     * @return boolean
     */
    public function getCanInvite()
    {
        return $this->can_invite;
    }

    /**
     * Set canJudge
     *
     * @param boolean $canJudge
     *
     * @return RoleType
     */
    public function setCanJudge($canJudge)
    {
        $this->can_judge = $canJudge;

        return $this;
    }

    /**
     * Get canJudge
     *
     * @return boolean
     */
    public function getCanJudge()
    {
        return $this->can_judge;
    }

    /**
     * Set canEvaluate
     *
     * @param boolean $canEvaluate
     *
     * @return RoleType
     */
    public function setCanEvaluate($canEvaluate)
    {
        $this->can_evaluate = $canEvaluate;

        return $this;
    }

    /**
     * Get canEvaluate
     *
     * @return boolean
     */
    public function getCanEvaluate()
    {
        return $this->can_evaluate;
    }
}
