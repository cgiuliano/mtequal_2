<?php

namespace AppBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 *
 * @ORM\Table(name="viewed",
 *              indexes={@ORM\Index(name="project", columns={"project"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ViewedDataRepository")
 */
class ViewedData
{
  public function __construct($num, $project, $user)
  {
    $this->num = $num;
    $this->user = $user;
    $this->project = $project;
    $this->modified = new \DateTime();
  }

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @var \AppBundle\Entity\Sentence
   *
   * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sentence")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="index_num", referencedColumnName="num")
   * })
   */
  private $num;

  /**
   * @ORM\Column(type="integer")
   */
  private $project;

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $user;


  /**
   * @ORM\Column(type="datetime")
   */
  private $modified;


  /**
   * Set num
   *
   * @param integer $num
   */
  public function setNum($num)
  {
    $this->num = $num;
  }

  /**
   * @return Sentence
   */
  public function getNum()
  {
    return $this->num;
  }

  /**
   * Set project
   *
   * @param integer $project
   */
  public function setProject($project)
  {
    $this->project = $project;
  }

  /**
   * Get project id
   *
   * @return integer
   */
  public function getProject()
  {
    return $this->project;
  }

  /**
   * @return integer
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * @param integer $userid
   */
  public function setUser($userid)
  {
    $this->user = $userid;
  }

  /**
   * @return DateTimeInterface
   */
  public function getModified()
  {
    return $this->modified;
  }

  /**
   * @param DateTimeInterface $modified
   */
  public function setModified($modified)
  {
    $this->modified = $modified;
  }
}
