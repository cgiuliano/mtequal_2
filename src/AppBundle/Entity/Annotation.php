<?php
/**
 * Project: mtequal_ext
 * User: cgirardi
 * Date: 21/10/15
 */
namespace AppBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnnotationRepository")
 * @ORM\Table(name="annotation",
              indexes={
                    @ORM\Index(name="num", columns={"num"}),
                    @ORM\Index(name="output_id", columns={"output_id"}),
                    @ORM\Index(name="user", columns={"user_id"}),
                    @ORM\Index(name="project", columns={"project_id"}),
                    @ORM\Index(name="eval", columns={"eval"}),
                    @ORM\Index(name="active", columns={"active"})})
 */

class Annotation
{
  const DONTKNOW_VALUE = 0;

  public function __construct(){
    $this->setActive(true);
    $this->setRevuser(0);
    $this->setModified(new \DateTime());
  }

  //* @ORM\ManyToOne(targetEntity="Sentence", inversedBy="annotated_sentences")
  //* @ORM\JoinColumn(name="num", referencedColumnName="num")

  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   */
  private $num;

  //* @ORM\ManyToOne(targetEntity="Sentence", inversedBy="annotated_output")
  //* @ORM\JoinColumn(name="output_id", referencedColumnName="id")

  /**
   * @ORM\Column(type="string", length=200)
   */
  private $output_id;


  /**
   * @ORM\Id
   * @ORM\ManyToOne(targetEntity="User")
   */
  private $user;

  /**
   * @ORM\ManyToOne(targetEntity="Project")
   */
  private $project;

  /**
   * @ORM\Id
   * @ORM\Column(type="string")
   */
  private $eval;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $evalids;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $evaltext;

  /**
  * @ORM\Column(type="datetime")
  */
  private $modified;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $confirmed;

  /**
   * @ORM\Id
   * @ORM\Column(type="boolean", nullable=false, options={"default" = true})
   */
  private $active;

  /**
   * @ORM\Column(type="integer", nullable=false)
   */
  private $revuser;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $revtime;

  // SETTER AND GETTER

  /**
   * @return integer
   */
  public function getNum()
  {
    return $this->num;
  }

  /**
   * @param integer $num
   */
  public function setNum($num)
  {
    $this->num = $num;
  }

  /**
   * @return string
   */
  public function getOutputId()
  {
    return $this->output_id;
  }

  /**
   * @param string $id
   */
  public function setOutputId($id)
  {
    $this->output_id = $id;
  }

  /**
   * @return User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * @param User $user
   */
  public function setUser(User $user)
  {
    $this->user = $user;
  }

  /**
   * @return Project
   */
  public function getProject()
  {
    return $this->project;
  }

  /**
   * @param Project $project
   */
  public function setProject(Project $project)
  {
    $this->project = $project;
  }

  /**
   * @return string
   */
  public function getEval()
  {
    return $this->eval;
  }

  /**
   * @param string $eval
   */
  public function setEval($eval)
  {
    $this->eval = $eval;
  }

  /**
   * @return string
   */
  public function getEvalids()
  {
    return $this->evalids;
  }

  /**
   * @param string $evalids
   */
  public function setEvalids($evalids)
  {
    $this->evalids = $evalids;
  }

  /**
   * @return string
   */
  public function getEvaltext()
  {
    return $this->evaltext;
  }

  /**
   * @param string $evaltext
   */
  public function setEvaltext($evaltext)
  {
    $this->evaltext = $evaltext;
  }

  /**
   * @return DateTimeInterface
   */
  public function getModified()
  {
    return $this->modified;
  }

  /**
   * @param DateTimeInterface $datetime
   */
  public function setModified($datetime)
  {
    $this->modified = $datetime;
  }

  /**
   * Set confirmed time
   * @param \DateTime $confirmed
   */
  public function setConfirmed($datetime)
  {
      $this->confirmed = $datetime;
  }

  /**
   * Get confirmed time
   *
   * @return \DateTime
   */
  public function getConfirmed()
  {
      return $this->confirmed;
  }

  /**
   * @return boolean
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * @param boolean $active
   */
  public function setActive($active)
  {
    $this->active = $active;
  }

  /**
   * @return integer
   */
  public function getRevuser()
  {
    return $this->revuser;
  }

  /**
   * @param integer $userid
   */
  public function setRevuser($userid)
  {
    $this->revuser = $userid;
  }

  /**
   * Set revision time
   * @param \DateTime $datetime
   */
  public function setRevtime($datetime)
  {
    $this->revtime = $datetime;
  }

  /**
   * Get revision time
   *
   * @return \DateTime
   */
  public function getRevtime()
  {
    return $this->revtime;
  }
}
