#!/bin/sh


echo "updating db schema..."


\rm -rf app/cache/*
php app/console doctrine:schema:update --force --dump-sql

php app/console cache:clear --env=prod
php app/console assets:install

echo "done!"