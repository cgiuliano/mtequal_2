# MT-equal 2 #

## A web application for human evaluation of machine translation quality ##

Authors: Claudio Giuliano (giuliano@fbk.eu) based on mt-equal_ext created by Christian Girardi

License: Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0.txt)

Last update: January 2018, 10.

## Requirements

* PHP 7.0+
* Apache 2.4+
* MySQL 5.5+
* Git (optional) 2.5.4+

If you don't have already installed PHP, Apache, and MySQL, please, consider the installation of a Apache–MySQL–PHP package, for example, LAMPs (Linux), WAMPs (Windows), or MAMPs (Macintosh).


## Quick start

These installation and configuration instructions have been specifically built for Ubuntu (16.04). On other operative systems, they might need some changes or not work at all.

### Configuration of PHP ###

In order install the required software on Ubuntu run this command:

```
php -m
```

and check to have the following PHP Modules installed:

```
bcmath, bz2, calendar, Core, ctype, curl, date, dba, dom, ereg, exif, fileinfo, filter, ftp, gd, hash, iconv, json, ldap, libxml, mbstring, mysql,
 mysqli, mysqlnd, openssl, pcre, PDO, pdo_mysql, pdo_sqlite, Phar, posix, readline, Reflection, session, shmop, SimpleXML, snmp, soap, sockets, SPL,
 sqlite3, standard, sysvmsg, sysvsem, sysvshm, tidy, tokenizer, wddx, xml, xmlreader, xmlrpc, xmlwriter, xsl, zip, zlib
```
if not, install them, for example the following commands install the zip, dom, url, mysql modules, respectively
```
sudo apt-get install php-zip
sudo apt-get install php-xml
sudo apt-get install php-curl
sudo apt-get install php-mysql
```

then, you might have to restart your Apache:

```
sudo /etc/init.d/apache2 restart
```

or

```
sudo service apache2 restart 
```

In order to use Apache mod_rewrite you can type the following command in the terminal:
```
sudo a2enmod rewrite
```

Now restart services:
```
sudo /etc/init.d/apache2 restart
sudo /usr/bin/mysqld_safe > /dev/null 2>&1 &
```

or

```
sudo service apache2 restart
sudo service mysql start
```

### Installation and configuration of MT-equal 2 ###

Create the directory where you want to install the application, for example:

```
sudo mkdir /var/www/mtequal
```

Get the the last version of MT-EQuAl:

* decompress the downloaded file from https://bitbucket.org/cgiuliano/mtequal_2/downloads/
* or to use the following git command:

```
sudo git clone https://bitbucket.org/cgiuliano/mtequal_2.git /var/www/mtequal/html/
```

Run the installation script:

```
$ cd /var/www/mtequal/html/
$ sudo ./INSTALL.sh
```

If you are using LAMPs, link the directory /var/www/mtequal/html/web/ in the Apache htdocs (usually located in /etc/apache2/htdocs/):

```
cd /etc/apache2/htdocs/
sudo ln -s /var/www/mtequal/html/web/ mtequal
```

If you need to change some configuration parameters you can edit the file:

```
sudo nano app/config/parameters.yml
```

Run the following command to give Apache the ownership of the html folder:

```
sudo chown -R www-data: /var/www/mtequal/html
```

Alternatively, use the ACL to assign access permissions to the web server user:

```
setfacl -R -m "u:www-data:rwx" /var/www/mtequal/html
```

Finally you can open the web application on your browser at the address:

http://localhost/mtequal/


### Configuration of Apache ###

Install the package libapache2-mod-php

```
sudo apt-get install libapache2-mod-php
```

Set up the Apache virtual host on Ubuntu.

```
sudo nano /etc/apache2/sites-available/mtequal.conf
```

Add the following information.

```
<VirtualHost *:80>
    ServerName <HOSTNAME>
    ServerAdmin <ADMIN EMAIL>

    ErrorLog ${APACHE_LOG_DIR}/mtequal.http.error.log
    CustomLog ${APACHE_LOG_DIR}/mtequal.http.access.log combined

    DocumentRoot /var/www/mtequal/html/web
    <Directory /var/www/mtequal>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        
        #With Apache 2.4
        Require all granted
        #With Apache 2.2
        #Order allow,deny
        
        allow from all
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ /app.php [QSA,L]
    </Directory>
</VirtualHost>
```

Enable the new virtual host:

```
sudo a2ensite mtequal
```

Restart the Apache server:

```
sudo service apache2 restart
```

If the application does not receive custom headers, it could be needed:

```
RewriteCond %{HTTP:_csrf_token} ^(.*)
RewriteRule .* - [e=HTTP__csrf_token:%1]
```
If the installation and configuration steps are successful, MT-EQuAl is running at http://<HOSTNAME>/.

If https is required, the configuration can be changed as follow:

```
<VirtualHost mtequal.futuro.media:80>
    ServerName mtequal.futuro.media
    ServerAdmin giulano@fbk.eu
    ErrorLog ${APACHE_LOG_DIR}/mtequal.http.error.log
    CustomLog ${APACHE_LOG_DIR}/mtequal.http.access.log combined

    RedirectPermanent / https://<YOUR_DOMAIN>
</VirtualHost>

<VirtualHost mtequal.futuro.media:443>
	ServerAdmin giuliano@fbk.eu
        ServerName mtequal.futuro.media
	DocumentRoot /var/www/mtequal/html/web
	RewriteEngine on
	ErrorLog ${APACHE_LOG_DIR}/mtequal.https.error.log
	CustomLog ${APACHE_LOG_DIR}/mtequal.https.access.log combined
	SSLEngine on
	SSLProxyEngine on
	SSLProtocol all -SSLv2
	SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP
	SSLCertificateFile	<YOUR_CERTIFICATE_FILE>
	SSLCertificateKeyFile   <YOUR_CERTIFICATE_KEY_FILE>
	SSLCertificateChainFile <YOUR_CERTIFICATE_CHAIN_FILE>
	<IfModule mod_dir.c>
		 DirectoryIndex index.html index.dhtml index.php index.htm default.htm welcome.html index.php
	</IfModule>
	<Directory "/var/www/mtequal">
		Options Indexes FollowSymLinks MultiViews
		AllowOverride None
		Order allow,deny
		allow from all
			RewriteEngine On
			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteRule ^(.*)$ /app.php [QSA,L]
	</Directory>
</VirtualHost>

``` 

### Problem solving ###

If bootstrap.php.cache is missing, run:

```
./vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php 
```

If testing on locahost, comment in ```app/config/config.yml``` the following lines

```
cookie_httponly: true
cookie_secure: true
```

If in production, the following configuration could cause problems, in case set to false.
```cookie_httponly: false```