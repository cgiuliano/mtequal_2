#!/bin/sh

#INSTALL
if [ ! -f bin/composer ]; then
    mkdir bin
    echo "Downloading composer (https://getcomposer.org/) ..."
    curl -sS https://getcomposer.org/installer | php -- --install-dir=bin --filename=composer
fi

\rm -rf app/cache/*
php bin/composer install
php app/console assets:install
php bin/composer update


php app/console doctrine:database:create -q
php app/console doctrine:schema:update --force

php app/console cache:clear --env=prod --no-debug

echo "Production database initialization..."
while read line; do
    php app/console doctrine:query:sql "$line" -q
done < data/database_init.sql

echo "Application install done"